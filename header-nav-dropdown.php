<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>

	<meta http-equiv="Content-Type" content="<?php bloginfo( 'html_type' ); ?>; charset=<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<link rel="stylesheet" href="https://use.typekit.net/mql5urv.css">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/site.webmanifest">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">

	<?php wp_head(); ?>

</head>

<?php

extract($args);

?>

<body <?php body_class(); ?>>

	<div class="wrapper">

		<div class="wrapper__inner">
			<?php app_render_fragment( 'header/popup' ); ?>

			<section class="section-navigation js-custom-scroll">
				<div class="shell">
					<div class="section__inner">
						<div class="section__bar">
							<a href="<?php echo home_url( '/' ); ?>" class="logo">
								<?php if ( ! empty( $style ) && ( $args['style'] == 'hairlounge' ) ) : ?>
									<img src="<?php bloginfo('template_directory'); ?>/resources/images/logo-white.svg" alt="Skinmed Hairlounge">
								<?php elseif ( ! empty( $style ) && ( $args['style'] == 'dermatology' ) ) : ?>
									<img src="<?php bloginfo('template_directory'); ?>/resources/images/logo-white.svg" alt="Skinmed Dermatology">
								<?php elseif ( ! empty( $style ) && ( $args['style'] == 'cosmetology' ) ) : ?>
									<img src="<?php bloginfo('template_directory'); ?>/resources/images/logo-white.svg" alt="Skinmed Cosmetology">
								<?php else: ?>
									<img src="<?php bloginfo('template_directory'); ?>/resources/images/logo-white.svg" alt="Skinmed">
								<?php endif ?>
							</a>

							<a href="#" class="btn-close-menu">
								<img src="<?php bloginfo('template_directory'); ?>/resources/images/ico-cross-lines.svg" alt="">
							</a>
						</div><!-- /.section__bar -->

						<div class="section__content">
							<div class="section__aside">
								<form action="<?php echo home_url( '/' ); ?>" class="search-form" method="get" role="search">
									<label>
										<span class="screen-reader-text">
											<?php _e( 'Suche', 'app' ); ?>
										</span>

										<input type="search" title="Search for:" name="s" value="" placeholder="Suche" class="search__field">
									</label>

									<button type="submit" class="search__btn">
										<?php _e( 'Search', 'app' ); ?>
									</button>

								</form>



								<?php



								if ( has_nav_menu( 'popup-tabs-menu' ) ) :
									wp_nav_menu( array(
										'theme_location'  => 'top-bar-menu-dropdown',
										'container'       => 'nav',
										'container_class' => 'nav-tabs nav-tabs--dropdown js-nav-tabs',
										'depth'           => 3,
									) );
								endif;



								if ( has_nav_menu( 'main-menu' ) ) :

									wp_nav_menu( array(

										'theme_location'  => 'main-menu',

										'container'       => 'nav',

										'container_class' => 'nav',

										'depth'           => 2,

									) );

								endif;



								app_render_fragment( 'header/socials' );



								app_render_fragment( 'header/languages' );



								app_render_fragment( 'header/copyright' );

								?>

							</div><!-- /.section__aside -->



							<?php

							$online_button = get_field( 'app_header_booking_button', 'option' );



							if ( ! empty( $online_button ) ) : ?>

								<div class="section__actions">

									<?php echo app_render_button_span( $online_button, 'btn-circle' ); ?>

								</div><!-- /.section__actions -->

							<?php endif ?>

						</div><!-- /.section__content -->

					</div><!-- /.section__inner -->

				</div><!-- /.shell -->

			</section><!-- /.section-navigation -->





			<section class="section-search-overlay js-custom-scroll">

				<div class="section__overlay"></div><!-- /.section__overlay -->

				<div class="shell">

					<div class="section__inner">

						<div class="section__bar">

							<a href="#" class="btn-close js-btn-close">

								<img src="<?php bloginfo('template_directory'); ?>/resources/images/ico-cross-lines.svg" alt="">

							</a>

						</div><!-- /.section__bar -->



						<?php app_render_fragment( 'header/search' ); ?>

					</div><!-- /.section__inner -->

				</div><!-- /.shell -->

			</section><!-- /.section-search-overlay -->

<?php
$white_background = get_field( 'app_header_white_background' );

if ( is_singular( 'app_member' ) || is_singular( 'job_listing' ) ) {
	$white_background = true;
}

if ( is_search() ) {
	$white_background = '';
}
?>

			<header class="header <?php echo ( ! empty( $white_background ) ) ? 'header--white-bg' : ''; ?>">

				<div class="shell">

					<?php app_render_fragment( 'header/top-bar-nav-dropdown', array(
						'style' => $args,
					) ); ?>

					<?php if ( ! empty( $style ) && ( $style == 'white' ) ) : ?>

						<div class="header__content">

							<a href="<?php echo home_url( '/' ); ?>" class="logo">

								<img src="<?php bloginfo('template_directory'); ?>/resources/images/logo-dark.svg" alt="Skinmed">

							</a>



							<a href="#" class="btn-menu">

								<img src="<?php bloginfo('template_directory'); ?>/resources/images/ico-btn-menu-dark.svg" alt="">

							</a>

						</div><!-- /.header__content -->

					<?php elseif ( ! empty( $style ) && ( $style == 'subtitle' ) ) : ?>

						<div class="header__content">

							<a href="<?php echo home_url( '/' ); ?>" class="logo">

								<img src="<?php bloginfo('template_directory'); ?>/resources/images/logo-dark-with-subtitle.svg" alt="Skinmed">

							</a>



							<a href="#" class="btn-menu">

								<img src="<?php bloginfo('template_directory'); ?>/resources/images/ico-btn-menu-dark.svg" alt="">

							</a>

						</div><!-- /.header__content -->

					<?php elseif ( ! empty( $style ) && ( $style == 'dermatology' ) ) : ?>
						<div class="header__content">

							<a href="<?php echo home_url( '/' ); ?>" class="logo">

								<img src="<?php bloginfo('template_directory'); ?>/resources/images/skinmed_dermatology.svg" alt="Skinmed Dermatology">

							</a>



							<a href="#" class="btn-menu">

								<img src="<?php bloginfo('template_directory'); ?>/resources/images/ico-btn-menu-dark.svg" alt="">

							</a>

						</div><!-- /.header__content -->
					<?php elseif ( ! empty( $style ) && ( $style == 'cosmetology' ) ) : ?>
						<div class="header__content">

							<a href="<?php echo home_url( '/' ); ?>" class="logo">

								<img src="<?php bloginfo('template_directory'); ?>/resources/images/skinmed_cosmetology.svg" alt="Skinmed Cosmetology">

							</a>



							<a href="#" class="btn-menu">

								<img src="<?php bloginfo('template_directory'); ?>/resources/images/ico-btn-menu-dark.svg" alt="">

							</a>

						</div><!-- /.header__content -->
					<?php elseif ( ! empty( $style ) && ( $style == 'hairlounge' ) ) : ?>
						<div class="header__content">

							<a href="<?php echo home_url( '/' ); ?>" class="logo">

								<img src="<?php bloginfo('template_directory'); ?>/resources/images/skinmed_hairlounge.svg" alt="Skinmed Hairlounge">

							</a>



							<a href="#" class="btn-menu">

								<img src="<?php bloginfo('template_directory'); ?>/resources/images/ico-btn-menu-dark.svg" alt="">

							</a>

						</div><!-- /.header__content -->
					<?php else: ?>

						<div class="header__content">

							<a href="<?php echo home_url( '/' ); ?>" class="logo">

								<img src="<?php bloginfo('template_directory'); ?>/resources/images/logo-white.svg" alt="Skinmed">

							</a>



							<a href="#" class="btn-menu">

								<img src="<?php bloginfo('template_directory'); ?>/resources/images/ico-btn-menu-white.svg" alt="">

							</a>

						</div><!-- /.header__content -->

					<?php endif ?>

				</div><!-- /.shell -->

			</header><!-- /.header -->



			<?php if ( is_front_page() ): ?>

				<div class="main main--fullpage">

			<?php else: ?>

				<div class="main">

			<?php endif ?>
