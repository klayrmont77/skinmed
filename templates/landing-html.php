<?php
/**
 * Template Name: Landing Html
 * Static: Yes # Remove this line once the template is integrated with the CMS
 */
?>

<?php get_header(); ?>

<section class="section-intro">
	<div class="section__image">
		<span class="image-fit">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/resources/images/temp/jpg/section-intro-1.jpg" alt="" />
		</span>
	</div><!-- /.section__image -->

	<div class="section__body">
		<div class="shell">
			<h1>Kompetenzzentrum</h1>

			<h6>Für Hautkrebs und Hautchirurgie</h6>

			<i class="ico-circle-text">
				<span>Online Buchen</span>
			</i>
		</div><!-- /.shell -->
	</div><!-- /.section__body -->
</section><!-- /.section-intro -->

<section class="section-slider-articles">
	<div class="section__head">
		<div class="shell">
			<blockquote>
				<h3>Höchste Präzision für die Haut</h3>

				<p>Skinmed bietet ein in der Schweiz einzigartiges Angebot zur Diagnose, Behandlung und Nachsorge bei Hautkrebserkrankungen unter einem Dach. </p>
			</blockquote>
		</div><!-- /.shell -->
	</div><!-- /.section__head -->

	<div class="section__body">
		<div class="shell">
			<div class="slider-articles js-slider-articles">
				<div class="slides js__slides">
					<div class="slide">
						<div class="slide__group">
							<div class="slide__caption" style="background-color: #2e2e2d;">
								<p>Operationen</p>
							</div><!-- /.slide__caption -->

							<div class="slide__image image-fit">
								<a href="#"></a>

								<img src="<?php bloginfo('stylesheet_directory'); ?>/resources/images/temp/jpg/slider-articles-1.jpg" alt="" />
							</div><!-- /.slide__image -->

							<div class="slide__content">
								<h5>
									<a href="#">Chirurgische Eingriffe und Hautchirurgie</a>
								</h5>

								<p>Von der Muttermalentfernung über die Spindel bis hin zur Lappenplastik und Vollhauttransplantation beherrschen die Fachärztinnen und Fachärzte bei Skinmed alle Methoden der modernen Hautchirurgie und setzen diese gezielt ein….</p>
							</div><!-- /.slide__content -->

							<div class="slide__actions">
								<a href="#">Mehr erfahren</a>
							</div><!-- /.slide__actions -->
						</div><!-- /.slide__group -->
					</div><!-- /.slide -->

					<div class="slide">
						<div class="slide__group">
							<div class="slide__caption" style="background-color: #7e7e7e;">
								<p>VORSORGE</p>
							</div><!-- /.slide__caption -->

							<div class="slide__image image-fit">
								<a href="#"></a>

								<img src="<?php bloginfo('stylesheet_directory'); ?>/resources/images/temp/jpg/slider-articles-2.jpg" alt="" />
							</div><!-- /.slide__image -->

							<div class="slide__content">
								<h5>
									<a href="#">Der Fotofinder zur Früherkennung von Hautkrebs</a>
								</h5>

								<p>Das Skinmed Kompetenzzentrum für Hautkrebs und Hautchirurgie verfügt über die modernste Infrastruktur zur Früherkennung von Hautkrebs. Der Fotofinder ist ein hochpräzises System, das die Haut in unvergleichlicher Qualität abbildet. Muttermale, die zu Hautkrebs führen…</p>
							</div><!-- /.slide__content -->

							<div class="slide__actions">
								<a href="#">Mehr erfahren</a>
							</div><!-- /.slide__actions -->
						</div><!-- /.slide__group -->
					</div><!-- /.slide -->

					<div class="slide">
						<div class="slide__group">
							<div class="slide__caption" style="background-color: #7e7e7e;">
								<p>VORSORGE</p>
							</div><!-- /.slide__caption -->

							<div class="slide__image image-fit">
								<a href="#"></a>

								<img src="<?php bloginfo('stylesheet_directory'); ?>/resources/images/temp/jpg/slider-articles-3.jpg" alt="" />
							</div><!-- /.slide__image -->

							<div class="slide__content">
								<h5>
									<a href="#">Digitale Auflichtmikroskopie</a>
								</h5>

								<p>Die Ganzkörperuntersuchung mittels Auflichtmikroskopie bedeutet, dass der Dermatologe oder die Dermatologin die gesamte Haut des Körpers untersucht und auffällige Muttermale mit einer speziellen beleuchteten Lupe genau prüft.. Die Lupe wird auf die betroffene Hautstelle gelegt...</p>
							</div><!-- /.slide__content -->

							<div class="slide__actions">
								<a href="#">Mehr erfahren</a>
							</div><!-- /.slide__actions -->
						</div><!-- /.slide__group -->
					</div><!-- /.slide -->

					<div class="slide">
						<div class="slide__group">
							<div class="slide__caption" style="background-color: #003672;">
								<p>VORSORGE</p>
							</div><!-- /.slide__caption -->

							<div class="slide__image image-fit">
								<a href="#"></a>

								<img src="<?php bloginfo('stylesheet_directory'); ?>/resources/images/temp/jpg/slider-articles-4.jpg" alt="" />
							</div><!-- /.slide__image -->

							<div class="slide__content">
								<h5>
									<a href="#">PDT (photodynamische Therapie) </a>
								</h5>

								<p>PDT steht für photodynamische Therapie: Das bedeutet, der Patientin oder dem Patienten mit Hautkrebsvorstufen wird eine spezielle Creme aufgetragen, welche die „Vor-Krebs-Zellen“/Hautkrebsvorstufen für ein spezielles Licht sensibilisiert. Anschliessend erfolgt die Bestrahlung mit dem speziellen Licht…</p>
							</div><!-- /.slide__content -->

							<div class="slide__actions">
								<a href="#">Mehr erfahren</a>
							</div><!-- /.slide__actions -->
						</div><!-- /.slide__group -->
					</div><!-- /.slide -->

					<div class="slide">
						<div class="slide__group">
							<div class="slide__caption" style="background-color: #003672;">
								<p>VORSORGE</p>
							</div><!-- /.slide__caption -->

							<div class="slide__image image-fit">
								<a href="#"></a>

								<img src="<?php bloginfo('stylesheet_directory'); ?>/resources/images/temp/jpg/slider-articles-5.jpg" alt="" />
							</div><!-- /.slide__image -->

							<div class="slide__content">
								<h5>
									<a href="#">Lasertherapie</a>
								</h5>

								<p>Hier gibt es zahlreiche Indikationen und Anwendungsmöglichkeiten in der Dermatologie. In der Hautchirurgie kommen vor allem ablative (abtragende) Laser zum Einsatz, die die obersten Hautschichten abtragen und somit Hautkrebsvorstufen beseitigen können. Allen voran ist ...</p>
							</div><!-- /.slide__content -->

							<div class="slide__actions">
								<a href="#">Mehr erfahren</a>
							</div><!-- /.slide__actions -->
						</div><!-- /.slide__group -->
					</div><!-- /.slide -->

					<div class="slide">
						<div class="slide__group">
							<div class="slide__caption" style="background-color: #003672;">
								<p>VORSORGE</p>
							</div><!-- /.slide__caption -->

							<div class="slide__image image-fit">
								<a href="#"></a>

								<img src="<?php bloginfo('stylesheet_directory'); ?>/resources/images/temp/jpg/slider-articles-6.jpg" alt="" />
							</div><!-- /.slide__image -->

							<div class="slide__content">
								<h5>
									<a href="#">Kryotherapie</a>
								</h5>

								<p>Die Kryotherapie ist eine Vereisungstherapie. Hier erfolgt fein dosiert die Vereisung der oberen Hautschichten durch flüssigen Stickstoff. Dabei kommt es zum Absterben der Hautkrebsvorstufen und gesunde Haut kann nachwachsen.</p>
							</div><!-- /.slide__content -->

							<div class="slide__actions">
								<a href="#">Mehr erfahren</a>
							</div><!-- /.slide__actions -->
						</div><!-- /.slide__group -->
					</div><!-- /.slide -->
				</div><!-- /.slides -->

				<div class="slider__actions">
					<a href="#" class="btn-slide btn-slide--prev js__prev">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 6.921 12.429">
							<path d="M321.745,173.936l-5.861-5.861,5.861-5.861" transform="translate(-315.177 -161.861)" />
						</svg>
					</a>

					<a href="#" class="btn-slide btn-slide--next js__next">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 6.921 12.429">
							<path d="M5.861,11.721,0,5.861,5.861,0" transform="translate(0.707 0.354)" />
						</svg>
					</a>
				</div><!-- /.slider__actions -->
			</div><!-- /.slider-articles -->
		</div><!-- /.shell -->
	</div><!-- /.section__body -->
</section><!-- /.section-slider-articles -->

<section class="section-content">
	<div class="shell">
		<div class="section__body">
			<h3>Der Fotofinder</h3>

			<h5>zur Früherkennung von Hautkrebs</h5>

			<div class="block-video alignleft">
				<a href="#">
					<i class="ico-play-video"></i>
				</a>

				<div class="block__image image-fit">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/resources/images/temp/jpg/section-image-and-content-1.jpg" alt="" />
				</div><!-- /.block__image -->
			</div><!-- /.block-video -->

			<p>Das Skinmed Kompetenzzentrum für Hautkrebs und Hautchirurgie verfügt über die modernste Infrastruktur zur Früherkennung von Hautkrebs. Der Fotofinder ist ein hochpräzises System, das die Haut in unvergleichlicher Qualität abbildet. Muttermale, die zu Hautkrebs führen könnten, werden durch das System und das Knowhow der behandelnden Ärztinnen und Ärzte rasch erkannt und können frühzeitig behandelt werden.</p>
		</div><!-- /.section__body -->

		<i class="ico-circle-text ico-circle-text--right">
			<span>Online Buchen</span>
		</i>
	</div><!-- /.shell -->
</section><!-- /.section-content -->

<section class="section-entry">
	<div class="shell">
		<div class="post">
			<div class="post__entry">
				<h3>Ihre Vorteile bei Skinmed:</h3>

				<ul>
					<li>Über 20 Jahre Erfahrung</li>

					<li>Über 20 spezialisierte Ärztinnen und Ärzte</li>

					<li>Komplettes Spektrum der Dermatologie, Phlebologie und Allergologie</li>

					<li>Über 4000 dermatochirurgische Eingriffe pro Jahr</li>

					<li>Modernste Infrastruktur mit OP der Klasse 1</li>

					<li>Weiterbildungsklinik Kategorie C, 2 Jahre</li>

					<li>Ambulante Operationen in örtlicher Betäubung </li>

					<li>Eigenes Histopathologie-Labor</li>

					<li>5 Standorte in der Schweiz: Aarau, Lenzburg, Olten, Wohlen und Zürich </li>
				</ul>

				<p>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/resources/images/temp/jpg/post-entry-1.jpg" alt="" class="alignfull" />
				</p>

				<p>Unsere Dermatochirurginnen und -chirurgen beherrschen zahlreiche Methoden der Hautchirurgie, die individuell abgestimmt auf die Ausgangslage der Patientinnen und Patienten zum Einsatz kommen. Bei Skinmed arbeiten wir mit höchster Präzision und modernster Infrastruktur, so dass bei einer Hautkrebsoperation ein möglichst natürliches und Narbenfreies und dadurch quasi «unsichtbares» Ergebnis entsteht.</p>
			</div><!-- /.post__entry -->
		</div><!-- /.post -->
	</div><!-- /.shell -->
</section><!-- /.section-entry -->

<section class="section-about">
	<div class="shell">
		<div class="section__head">
			<h3>ABCDE-Regel:</h3>

			<h5>Das sind die Frühwarnzeichen für Hautkrebs</h5>

			<p>Hautkrebs ist die weltweit häufigste Krebsart. Während der weisse Hautkrebs – frühzeitig erkannt – gut behandelbar und fast zu 100% heilbar ist, ist der schwarze Hautkrebs weitaus gefährlicher. Dass sich die Haut verändert, ist normal. Doch wie erkennt man, ob diese Veränderungen harmlos sind oder sich in eine gefährliche Richtung entwickeln? Die ABCDE-Regel hilft bei einer ersten Selbstbeurteilung, ob ein Muttermal oder Pigmentfleck Potenzial für Hautkrebs in sich birgt.</p>

			<p><strong>Aber Achtung:</strong> Anzeichen für Hautkrebs können leicht mit harmlosen Veränderungen verwechselt werden. Deshalb ersetzt auch eine aufmerksame Beobachtung an sich selbst die regelmässige Kontrolle beim Dermatologen nicht.</p>
		</div><!-- /.section__head -->

		<div class="section__list">
			<div class="list-about">
				<div class="list__body">
					<ul>
						<li>
							<div class="list__image image-fit image-fit--contain">
								<img src="<?php bloginfo('stylesheet_directory'); ?>/resources/images/temp/png/list-about-1.png" alt="" />
							</div><!-- /.list__image -->

							<div class="list__content">
								<h3>A</h3>

								<h4>Asymmetrie</h4>

								<p>Muttermale sollten eine gleichmässige, runde, ovale oder längliche Form haben; keine Asymmetrien oder Ungleichmässigkeiten.</p>
							</div><!-- /.list__content -->
						</li>

						<li>
							<div class="list__image image-fit image-fit--contain">
								<img src="<?php bloginfo('stylesheet_directory'); ?>/resources/images/temp/png/list-about-2.png" alt="" />
							</div><!-- /.list__image -->

							<div class="list__content">
								<h3>B</h3>

								<h4>Begrenzung</h4>

								<p>Die Ränder eines Hautflecks sollen scharf und klar sein – im Gegensatz zu verwaschenen, gezackten oder unebenen und rauen Rändern sowie verwischten oder ausgefransten Konturen.</p>
							</div><!-- /.list__content -->
						</li>

						<li>
							<div class="list__image image-fit image-fit--contain">
								<img src="<?php bloginfo('stylesheet_directory'); ?>/resources/images/temp/png/list-about-3.png" alt="" />
							</div><!-- /.list__image -->

							<div class="list__content">
								<h3>C</h3>

								<h4>Colour</h4>

								<p>Unterschiedliche Färbungen, hellere und dunklere Bereiche, eventuell rötliche Anteile in einem Pigmentmal sind ein Warnzeichen. Dasselbe gilt für krustige Auflagen.</p>
							</div><!-- /.list__content -->
						</li>

						<li>
							<div class="list__image image-fit image-fit--contain">
								<img src="<?php bloginfo('stylesheet_directory'); ?>/resources/images/temp/png/list-about-4.png" alt="" />
							</div><!-- /.list__image -->

							<div class="list__content">
								<h3>D</h3>

								<h4>Durchmesser</h4>

								<p>Muttermale mit einem Durchmesser von über 5 mm an der breitesten Stelle, Pigmentmale, die größer als 5 mm im Durchmesser sind oder eine Halbkugelform haben, sollten kontrolliert werden. Achtung: Es gibt auch Melanome, die kleiner als 5 mm sind.</p>
							</div><!-- /.list__content -->
						</li>

						<li>
							<div class="list__image image-fit image-fit--contain">
								<img src="<?php bloginfo('stylesheet_directory'); ?>/resources/images/temp/png/list-about-5.png" alt="" />
							</div><!-- /.list__image -->

							<div class="list__content">
								<h3>E</h3>

								<h4>Entwicklung</h4>

								<p>Pigmentflecken, die sich in einem der vorgängig genannten Merkmale verändern, sollte besondere Aufmerksamkeit geschenkt werden. Auch ein Muttermal, das man aufgekratzt hat, das juckt, schmerzt, blutet, nässt oder entzündet ist, gehört in die Behandlung eines Dermatologen.</p>
							</div><!-- /.list__content -->
						</li>
					</ul>
				</div><!-- /.list__body -->

				<div class="list__foot">
					<p>In der Regel sollten mehrere der oben genannten Kriterien zusammenkommen, bevor ein Muttermal als kritisch zu erachten ist. Gelegentlich genügt aber auch die Erfüllung eines der genannten Kriterien. Im Zweifel daher lieber einmal mehr zum Dermatologen als einmal zu wenig.</p>
				</div><!-- /.list__foot -->
			</div><!-- /.list-about -->
		</div><!-- /.section__list -->

		<div class="section__video">
			<div class="block-iframe">
				<iframe type="text/html" width="" height="" src="http://www.youtube.com/embed/55L-KDkcQ5A" allowfullscreen frameborder="0"></iframe>
			</div><!-- /.block-iframe -->
		</div><!-- /.section__video -->
	</div><!-- /.shell -->
</section><!-- /.section-about -->

<section class="section-image-and-content">
	<div class="shell">
		<div class="section__head">
			<h3>Ihre Ansprechpartner für eine fundierte Hautkrebsbehandlung</h3>
		</div><!-- /.section__head -->

		<div class="section__body">
			<div class="section__body-image">
				<span>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/resources/images/temp/jpg/section-content-and-image-secondary-1.jpg" alt="" />

					<i class="ico-circle-text">
						<span>Online Buchen</span>
					</i>
				</span>
			</div><!-- /.section__body-image -->

			<div class="section__body-content">
				<p><strong>Der weisse Hautkrebs ist die häufigste Krebsart beim Menschen. Und die Zahl der Erkrankten steigt. Neben dem weissen Hautkrebs, der in den meisten Fällen heilbar ist, gibt es auch den bösartigen schwarzen Hautkrebs. Hier ist eine frühzeitige Entdeckung entscheidend für die Heilungschancen. Beide Hautkrebsarten deuten sich in der Regel frühzeitig an, sodass eine regelmässige Vorsorgeuntersuchung bei Risikopatienten wichtig ist.</strong></p>

				<p>Skinmed hat sich mit seinem Ärzteteam auf das Thema Hautkrebs-Früherkennung und Hautkrebstherapie spezialisiert. Viele Hautkrebsvorstufen lassen sich ohne eine Operation heilen. Dafür gibt es die sogenannte Kryotherapie, Laser aber auch spezielle Cremes.</p>

				<p>Als das Zentrum für Hautchirurgie in der Schweiz hat sich Skinmed aber insbesondere auch auf Operationen von Hautkrebs spezialisiert. Unser operatives Kompetenzteam besteht aus speziell chirurgisch ausgebildeten Dermatologinnen und Dermatologen sowie Plastischen Chirurginnen und Plastischen Chirurgen. Die Ärzte und Ärztinnen haben über 20 Jahre Erfahrung und führen jährlich 4’000 hautchirurgische Eingriffe in bester Infrastruktur mit modernsten OP-Sälen durch. Ebenso zählt ein eigenes histopathologisches Labor zur Klinik. Dies bringt den grossen Vorteil einer schnellen Befundung der Hautexzidate, sowie direkte Rücksprache zwischen den operierenden Ärzten und den Pathologen mit sich. Die meisten Operationen werden in örtlicher Betäubung durchgeführt, sodass Sie nach der Operation direkt nach Hause können.</p>
			</div><!-- /.section__body-content -->
		</div><!-- /.section__body -->
	</div><!-- /.shell -->
</section><!-- /.section-image-and-content -->

<section class="section-list-content">
	<div class="shell">
		<div class="list-content">
			<ul>
				<li>
					<blockquote>
						<h6>Wer sollte zur Vorsorge kommen?</h6>

						<ul>
							<li>Menschen mit Hauttyp I und II</li>

							<li>Menschen mit besonders vielen Muttermalen (mehr als 50 Stück) </li>

							<li>Hautkrebs in der Familie oder eigenen Anamnese</li>

							<li>Viele Sonnenbrände in der Kindheit</li>

							<li>Wenn Muttermale Form und/oder Grösse verändern</li>

							<li>Ab einem Alter von spätestens 30 Jahren sollte ein einmaliger Ganzkörpercheck erfolgen</li>

							<li>Personen ab spätestens 50 Jahre sollten regelmässig zur Vorsorge kommen (alle ein bis zwei Jahre)</li>
						</ul>
					</blockquote>
				</li>

				<li>
					<blockquote>
						<h6>Wie schützt man sich vor Hautkrebs</h6>

						<ul>
							<li>Sonnenschutz mit Lichtschutzfaktor 50 anwenden</li>

							<li>Eher im Schatten aufhalten, Mittagssonne meiden</li>

							<li>Bedeckende Kleidung</li>

							<li>Nach dem Schwimmen erneut eincremen</li>

							<li>Exzessives Sonnenbaden vermeiden</li>

							<li>Mittagssonne meiden</li>

							<li>Exzessives Sonnenbaden vermeiden</li>

							<li>Mittagssonne meiden</li>
						</ul>
					</blockquote>
				</li>

				<li>
					<blockquote>
						<h6>Wie erkennt man weissen Hautkrebs?</h6>

						<p>Weissen Hautkrebs als Patient zu erkennen ist nicht einfach. Anzeichen für weissen Hautkrebs oder die Vorstufen von weissem Hautkrebs können sein:</p>

						<ul>
							<li>Chronische Wunden, die über mehr als 3 Monate nicht heilen</li>

							<li>Wiederkehrend auftretende rote raueStellen der Haut, oder bräunlich-gelbe</li>

							<li>Stellen mit Krusten die z.T. blutenkönnen und gefühlstechnisch leicht irritiert sind</li>

							<li>Schnell wachsende Knoten, häufig rötlich mit vermehrten Gefässneubildungen(Adern)</li>
						</ul>
					</blockquote>
				</li>
			</ul>
		</div><!-- /.list-content -->
	</div><!-- /.shell -->
</section><!-- /.section-list-content -->

<section class="section-slider-before-after">
	<div class="shell">
		<div class="section__head">
			<h3>Behandlungsergebnisse</h3>

			<p>Unsere Dermatochirurginnen und -chirurgen arbeiten mit höchster Präzision, so dass bei einer Hautkrebsoperation ein möglichst natürliches und Narbenfreies, also quasi «unsichtbares» Ergebnis entsteht. Überzeugen Sie sich selbst bei unseren Fotos.</p>
		</div><!-- /.section__head -->

		<div class="section__body">
			<div class="slider-before-after js-slider-before-after">
				<div class="slides js__slides">
					<div class="slide">
						<div class="slide__images">
							<div class="slide__image">
								<span class="image-fit">
									<img src="<?php bloginfo('stylesheet_directory'); ?>/resources/images/temp/jpg/slider-before-anfter-1.jpg" alt="" />
								</span>
							</div><!-- /.slide__image -->

							<div class="slide__image">
								<span class="image-fit">
									<img src="<?php bloginfo('stylesheet_directory'); ?>/resources/images/temp/jpg/slider-before-anfter-1-1.jpg" alt="" />
								</span>
							</div><!-- /.slide__image -->
						</div><!-- /.slide__images -->
					</div><!-- /.slide -->

					<div class="slide">
						<div class="slide__images">
							<div class="slide__image">
								<span class="image-fit">
									<img src="<?php bloginfo('stylesheet_directory'); ?>/resources/images/temp/jpg/slider-before-anfter-1.jpg" alt="" />
								</span>
							</div><!-- /.slide__image -->

							<div class="slide__image">
								<span class="image-fit">
									<img src="<?php bloginfo('stylesheet_directory'); ?>/resources/images/temp/jpg/slider-before-anfter-1-1.jpg" alt="" />
								</span>
							</div><!-- /.slide__image -->
						</div><!-- /.slide__images -->
					</div><!-- /.slide -->

					<div class="slide">
						<div class="slide__images">
							<div class="slide__image">
								<span class="image-fit">
									<img src="<?php bloginfo('stylesheet_directory'); ?>/resources/images/temp/jpg/slider-before-anfter-1.jpg" alt="" />
								</span>
							</div><!-- /.slide__image -->

							<div class="slide__image">
								<span class="image-fit">
									<img src="<?php bloginfo('stylesheet_directory'); ?>/resources/images/temp/jpg/slider-before-anfter-1-1.jpg" alt="" />
								</span>
							</div><!-- /.slide__image -->
						</div><!-- /.slide__images -->
					</div><!-- /.slide -->

					<div class="slide">
						<div class="slide__images">
							<div class="slide__image">
								<span class="image-fit">
									<img src="<?php bloginfo('stylesheet_directory'); ?>/resources/images/temp/jpg/slider-before-anfter-1.jpg" alt="" />
								</span>
							</div><!-- /.slide__image -->

							<div class="slide__image">
								<span class="image-fit">
									<img src="<?php bloginfo('stylesheet_directory'); ?>/resources/images/temp/jpg/slider-before-anfter-1-1.jpg" alt="" />
								</span>
							</div><!-- /.slide__image -->
						</div><!-- /.slide__images -->
					</div><!-- /.slide -->
				</div><!-- /.slides -->

				<div class="slider__actions">
					<a href="#" class="btn-slide btn-slide--prev js__prev">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 6.921 12.429">
							<path d="M321.745,173.936l-5.861-5.861,5.861-5.861" transform="translate(-315.177 -161.861)" />
						</svg>
					</a>

					<a href="#" class="btn-slide btn-slide--next js__next">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 6.921 12.429">
							<path d="M5.861,11.721,0,5.861,5.861,0" transform="translate(0.707 0.354)" />
						</svg>
					</a>
				</div><!-- /.slider__actions -->
			</div><!-- /.slider-before-after -->
		</div><!-- /.section__body -->
	</div><!-- /.shell -->
</section><!-- /.section-slider-before-after -->

<section class="section-content-about">
	<div class="shell">
		<blockquote>
			<p><strong>Buchen Sie jetzt</strong> Ihren Termin zur Vorsorgeuntersuchung oder Diagnose und Behandlung bei Verdacht auf Hautkrebs.</p>

			<i class="ico-circle-text ico-circle-text--right">
				<span>Online Buchen</span>
			</i>
		</blockquote>
	</div><!-- /.shell -->
</section><!-- /.section-content-about -->

<?php get_footer(); ?>
