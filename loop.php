<?php
if ( ! is_search() && ! is_archive() ) :

	echo apply_filters( 'the_content', get_the_content( null, false, get_option( 'page_for_posts' ) ) );

elseif ( have_posts() ) : ?>
	<section class="app-block-articles">
		<div class="shell">
			<div class="app__block-inner">
				<ol class="articles js-articles-isotope">
					<?php while ( have_posts() ) : the_post();

						$category 		 = get_the_category();
						if ( ! empty( $category ) ) {
							$category_name   = $category[0]->name;
							$category_url 	 = get_term_link( $category[0] );
						}

						$author_id 		 = get_post_field( 'post_author', get_the_ID() );
						$author_nickname = get_the_author_meta( 'nickname', $author_id );
						$author_url 	 = get_author_posts_url( $author_id );
						?>
						<li class="article js-article-isotope">
							<div class="article__inner" data-aos="fade-up">
								<?php if ( has_post_thumbnail() ) : ?>
									<div class="article__image">
										<a href="<?php the_permalink(); ?>">
											<?php the_post_thumbnail(); ?>
										</a>
									</div><!-- /.article__image -->
								<?php endif ?>

								<div class="article__content">
									<?php if ( ! empty( $category ) ) : ?>
										<div class="article__categories">
											<a href="<?php echo esc_url( $category_url ); ?>">
												<?php echo esc_html( $category_name ); ?>
											</a>
										</div><!-- /.article__categories -->
									<?php endif ?>

									<h4 class="article__title">
										<a href="<?php the_permalink(); ?>">
											<?php the_title(); ?>
										</a>
									</h4><!-- /.article__title -->

									<div class="article__meta">
										<ul>
											<li>
												<?php echo get_the_date( 'j.m.Y' ); ?>
											</li>

											<li>
												<a href="<?php echo esc_url( $author_url ); ?>">
													<?php echo esc_html( $author_nickname ); ?>
												</a>
											</li>
										</ul>
									</div><!-- /.article__meta -->

									<div class="article__entry">
										<p>
											<?php the_excerpt(); ?>
										</p>
									</div><!-- /.article__entry -->
								</div><!-- /.article__content -->
							</div><!-- /.article__inner -->
						</li><!-- /.article -->
					<?php endwhile; ?>
				</ol><!-- /.articles -->
				<?php
				carbon_pagination('posts', array(
					'wrapper_before' => '<div class="paging">',
					'wrapper_after' => '</div>',
					'enable_prev' => true,
					'enable_next' => true,
					'enable_first' => true,
					'enable_last' => true,
					'enable_numbers' => true,
					'enable_current_page_text' => false,
					'number_limit' => 3,
					'large_page_number_limit' => 1,
					'large_page_number_interval' => -1,
					'numbers_wrapper_before' => '<ul>',
					'numbers_wrapper_after' => '</ul>',
					'prev_html' => '<a href="{URL}" class="paging__prev">' . esc_html__( '', 'crb' ) . '</a>',
					'next_html' => '<a href="{URL}" class="paging__next">' . esc_html__( '', 'crb' ) . '</a>',
					'first_html' => '<a href="{URL}" class="paging__first"></a>',
					'last_html' => '<a href="{URL}" class="paging__last"></a>',
					'number_html' => '<li><a href="{URL}">{PAGE_NUMBER}</a></li>',
					'current_number_html' => '<li class="current"><a href="{URL}">{PAGE_NUMBER}</a></li>',
					'limiter_html' => '<li class="paging__spacer">...</li>',
				));
				?>
			</div><!-- /.app__block-inner -->
		</div><!-- /.shell -->
	</section><!-- /.app-block-articles -->
<?php else : ?>
	<section class="section-default">
		<div class="shell">
			<div class="section__content">
				<?php app_the_title( '<h1 class="pagetitle">', '</h1>' ); ?>
				<div class="articles">
					<ol>
						<li>
							<div class="article article--error404 article--not-found">
								<div class="article__body">
									<div class="article__entry richtext-entry">
										<p>
											<?php
								if ( is_category() ) { // If this is a category archive
									printf( __( "Sorry, but there aren't any posts in the %s category yet.", 'crb' ), single_cat_title( '', false ) );
								} else if ( is_date() ) { // If this is a date archive
									_e( "Sorry, but there aren't any posts with this date.", 'crb' );
								} else if ( is_author() ) { // If this is a category archive
									$userdata = get_user_by( 'id', get_queried_object_id() );
									printf( __( "Sorry, but there aren't any posts by %s yet.", 'crb' ), $userdata->display_name );
								} else if ( is_search() ) { // If this is a search
									_e( 'No posts found. Try a different search?', 'crb' );
								} else {
									_e( 'No posts found.', 'crb' );
								}
								?>
							</p>

							<?php get_search_form(); ?>
						</div><!-- /.article__entry -->
					</div><!-- /.article__body -->
				</div><!-- /.article -->
			</li>
		</ol>
	</div><!-- /.articles -->
</div><!-- /.section__content -->
</div><!-- /.shell -->
</section><!-- /.section-default -->
<?php endif; ?>
