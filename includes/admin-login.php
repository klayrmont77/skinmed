<?php
/**
 * Change the login header logo href attribute to open the homepage
 * instead of the default https://wordpress.org/.
 *
 * @link https://developer.wordpress.org/reference/hooks/login_headerurl/
 *
 * @param  string $login_header_url Login header logo URL.
 * @return string Homepage URL.
 */
function app_change_login_header_url( $login_header_url ) {
	return home_url( '/' );
}
add_filter( 'login_headerurl', 'app_change_login_header_url' );

/**
 * Change the login header logo title attribute to display the Site Title
 * instead of the default "Powered by WordPress".
 *
 * @link https://developer.wordpress.org/reference/hooks/login_headertext/
 *
 * @param  string $login_header_title Login header logo URL.
 * @return string Site Title.
 */
function app_change_login_header_title( $login_header_title ) {
	return get_bloginfo( 'name' );
}
add_filter( 'login_headertext', 'app_change_login_header_title' );

function crb_change_login_styles() { ?>
	<style type="text/css">
		@import url('https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap');

		.login { font-family: 'Montserrat', sans-serif; background-color: #3C3C3C; color: #3f3f3f; line-height: 1.59; }
		#login h1 a { width: 250px; height: 75px; background: url(<?php echo get_template_directory_uri(); ?>/resources/images/logo-white.svg) center no-repeat; background-size: contain; }

		#lostpasswordform,
		.login form#loginform { background-color: #323232; padding: 32px 24px; box-shadow: none; border-radius: 0; border: none; }
		.login #login #login_error,
		.login #login .message,
		.login #login .success { color: #323232; border-radius: 0 3px 3px 0; box-shadow: 0 1px 3px rgba( 0, 0, 0, .3 ); }

		#lostpasswordform input:not([type="checkbox"]),
		#loginform input:not([type="checkbox"]) { font-size: 16px; border-radius: 0; padding: 14px 15px; margin: 0 0 15px 0; box-shadow: none; outline: none; border: none; }
		#loginform input:not([type="checkbox"]):focus { border: none; box-shadow: none; outline: none; }
		#loginform input[type="checkbox"]:focus { border: 1px solid #0094d6; box-shadow: unset; }
		#loginform { background-color: #fff; border-radius: 0; }

		#lostpasswordform label,
		#loginform label { font-size: 11px; color: #fff; letter-spacing: 0.2em; text-transform: uppercase; font-weight: 500; display: block; }

		#lostpasswordform #wp-submit,
		.login #loginform #wp-submit {  font-family: 'Montserrat', sans-serif; display: block; min-width: 170px; padding: 2px 12px 0; margin: 15px 0 0 0; font-weight: 700; background-color: #fff; font-size: 14px; color: #000; height: auto; text-transform: uppercase; letter-spacing: 0.03em; line-height: 35px; border: none; box-shadow: unset; border-radius: 0; text-shadow: unset; transition: all .4s; border: 2px solid #fff;}
		#lostpasswordform #wp-submit:hover,
		.login #loginform #wp-submit:hover { background: transparent; color: #fff; }
		.login input[type="checkbox"]:checked:before { color: #0094d6; }
		.login form .input,
		.login input[type=text],
		.login #loginform input[type=text],
		.login #loginform input[type=password] { font-size: 16px; border-radius: 0; padding: 14px 15px; margin: 0 0 15px 0; }

		.login form#loginform .forgetmenot { width: 100%; display: flex; align-items: center; }

		.login #loginform .button.wp-hide-pw { height: 49px; }

		.login #login #backtoblog,
		.login #login #nav { padding: 0; margin-top: 10px; }

		.login #login .privacy-policy-link,
		.login #login #backtoblog a,
		.login #login #nav a { color: #000; background-color: #fff; padding: 7px 10px; display: inline-block; text-decoration: none; opacity: 1; transition: all .4s; }

		.login #login .privacy-policy-link:hover,
		.login #login #backtoblog a:hover,
		.login #login #nav a:hover { color: #fff; background-color: #323232;  }

		.login #login #backtoblog a:focus,
		.login #login #nav a:focus { outline: none; box-shadow: none; }

		#login .message { border-left: 4px solid #323232;  }

		#login #wfls-prompt-overlay {
			background-color: #323232;
		}
		#login #wfls-prompt-overlay input[type="submit"] {
			font-family: 'Montserrat', sans-serif;
			display: block;
			min-width: 170px;
			padding: 2px 12px 0;
			margin: 15px 0 0 0;
			font-weight: 700;
			background-color: #fff;
			font-size: 14px;
			color: #000;
			height: auto;
			text-transform: uppercase;
			letter-spacing: 0.03em;
			line-height: 35px;
			border: none;
			box-shadow: unset;
			border-radius: 0;
			text-shadow: unset;
			transition: all .4s;
			border: 2px solid #fff;
		}
	</style>
<?php }
add_action( 'login_enqueue_scripts', 'crb_change_login_styles', 10 );
