<?php

add_action( 'acf/init', 'app_acf_init_block_types' );
function app_acf_init_block_types() {
	// Check whether the function exists.
	if ( function_exists( 'acf_register_block_type' ) ) {
		acf_register_block_type( array(
			'name'            => 'intro',
			'title'           => __( 'Intro', 'app' ),
			'description'     => __( 'An intro block.', 'app' ),
			'render_template' => 'fragments/blocks/intro.php',
			'category'        => 'custom-blocks',
			'icon'            => 'slides',
			'keywords'        => array( 'intro' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'hero-home',
			'title'           => __( 'Hero (Home)', 'app' ),
			'description'     => __( 'A hero (Home) block.', 'app' ),
			'render_template' => 'fragments/blocks/hero-home.php',
			'category'        => 'custom-blocks',
			'post_types'	  => array( 'page', 'post', 'app_member', 'app_media' ),
			'icon'            => 'slides',
			'keywords'        => array( 'hero', 'intro' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'hero',
			'title'           => __( 'Hero', 'app' ),
			'description'     => __( 'A hero block.', 'app' ),
			'render_template' => 'fragments/blocks/hero.php',
			'category'        => 'custom-blocks',
			'icon'            => 'slides',
			'keywords'        => array( 'hero', 'intro' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'hero-alt',
			'title'           => __( 'Hero Alt', 'app' ),
			'description'     => __( 'A hero alt block.', 'app' ),
			'render_template' => 'fragments/blocks/hero-alt.php',
			'category'        => 'custom-blocks',
			'icon'            => 'slides',
			'keywords'        => array( 'hero', 'intro', 'alt' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'hero-actions',
			'title'           => __( 'Hero with Actions', 'app' ),
			'description'     => __( 'A hero with actions block.', 'app' ),
			'render_template' => 'fragments/blocks/hero-actions.php',
			'category'        => 'custom-blocks',
			'icon'            => 'slides',
			'keywords'        => array( 'hero', 'intro', 'actions' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'hero-magazine',
			'title'           => __( 'Hero (Magazine)', 'app' ),
			'description'     => __( 'A hero (magazine) block.', 'app' ),
			'render_template' => 'fragments/blocks/hero-magazine.php',
			'category'        => 'custom-blocks',
			'icon'            => 'slides',
			'keywords'        => array( 'hero', 'intro', 'magazine' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'hero-service',
			'title'           => __( 'Hero (Service)', 'app' ),
			'description'     => __( 'A hero (service) block.', 'app' ),
			'render_template' => 'fragments/blocks/hero-service.php',
			'category'        => 'custom-blocks',
			'icon'            => 'slides',
			'keywords'        => array( 'hero', 'intro', 'service' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );


		acf_register_block_type( array(
			'name'            => 'content-with-image-plastic',
			'title'           => __( 'Content with Image (Plastic Surgery)', 'app' ),
			'description'     => __( 'A content with image (plastic surgery) block.', 'app' ),
			'render_template' => 'fragments/blocks/content-with-image-plastic.php',
			'category'        => 'custom-blocks',
			'post_types'	  => array( 'page', 'post', 'app_member', 'app_media' ),
			'icon'            => 'images-alt',
			'keywords'        => array( 'plastic', 'surgery', 'content', 'image' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'content-with-image-derma',
			'title'           => __( 'Content with Image (Dermatology)', 'app' ),
			'description'     => __( 'A content with image (Dermatology) block.', 'app' ),
			'render_template' => 'fragments/blocks/content-with-image-derma.php',
			'category'        => 'custom-blocks',
			'post_types'	  => array( 'page', 'post', 'app_member', 'app_media' ),
			'icon'            => 'images-alt2',
			'keywords'        => array( 'dermatology', 'surgery', 'content', 'image' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'content-with-image-landing',
			'title'           => __( 'Content with Image (Landing)', 'app' ),
			'description'     => __( 'A content with image (landing) block.', 'app' ),
			'render_template' => 'fragments/blocks/content-with-image-landing.php',
			'category'        => 'custom-blocks',
			'post_types'	  => array( 'page', 'post', 'app_member', 'app_media' ),
			'icon'            => 'images-alt2',
			'keywords'        => array( 'landing', 'content', 'image' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'team',
			'title'           => __( 'Team', 'app' ),
			'description'     => __( 'A team block.', 'app' ),
			'render_template' => 'fragments/blocks/team.php',
			'category'        => 'custom-blocks',
			'icon'            => 'groups',
			'keywords'        => array( 'team', 'members' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'institute',
			'title'           => __( 'Aestethic Institute', 'app' ),
			'description'     => __( 'A aestethic institute block.', 'app' ),
			'render_template' => 'fragments/blocks/institute.php',
			'category'        => 'custom-blocks',
			'post_types'	  => array( 'page', 'post', 'app_member', 'app_media' ),
			'icon'            => 'welcome-learn-more',
			'keywords'        => array( 'swiss', 'aestethic', 'institute' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'product',
			'title'           => __( 'Product', 'app' ),
			'description'     => __( 'A product block.', 'app' ),
			'render_template' => 'fragments/blocks/product.php',
			'category'        => 'custom-blocks',
			'post_types'	  => array( 'page', 'post', 'app_member', 'app_media' ),
			'icon'            => 'products',
			'keywords'        => array( 'content', 'product' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'restaurant',
			'title'           => __( 'Skin\'s Restaurant', 'app' ),
			'description'     => __( 'A restaurant block.', 'app' ),
			'render_template' => 'fragments/blocks/restaurant.php',
			'post_types'      => array( 'page', 'post', 'app_member', 'app_media' ),
			'category'        => 'custom-blocks',
			'icon'            => 'food',
			'keywords'        => array( 'skin', 'restaurant' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'image-with-accordion',
			'title'           => __( 'Image with Accordion', 'app' ),
			'description'     => __( 'A image with accordion block.', 'app' ),
			'render_template' => 'fragments/blocks/image-with-accordion.php',
			'category'        => 'custom-blocks',
			'icon'            => 'menu',
			'keywords'        => array( 'image', 'accordion' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'image-with-accordion-reversed',
			'title'           => __( 'Image with Accordion Reversed', 'app' ),
			'description'     => __( 'A image with accordion reversed block.', 'app' ),
			'render_template' => 'fragments/blocks/image-with-accordion-reversed.php',
			'category'        => 'custom-blocks',
			'icon'            => 'menu',
			'keywords'        => array( 'image', 'accordion', 'reversed' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'image-with-accordion-alt',
			'title'           => __( 'Image with Accordion Alt', 'app' ),
			'description'     => __( 'A image with accordion alt block.', 'app' ),
			'render_template' => 'fragments/blocks/image-with-accordion-alt.php',
			'category'        => 'custom-blocks',
			'icon'            => 'menu',
			'keywords'        => array( 'image', 'accordion', 'alt' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'single-service',
			'title'           => __( 'Single Service', 'app' ),
			'description'     => __( 'A single service block.', 'app' ),
			'render_template' => 'fragments/blocks/single-service.php',
			'category'        => 'custom-blocks',
			'icon'            => 'admin-generic',
			'keywords'        => array( 'service', 'single' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'image-full',
			'title'           => __( 'Image Full Width', 'app' ),
			'description'     => __( 'A image full width block.', 'app' ),
			'render_template' => 'fragments/blocks/image-full.php',
			'category'        => 'custom-blocks',
			'icon'            => 'cover-image',
			'keywords'        => array( 'width', 'full', 'image' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'columns',
			'title'           => __( 'Columns Custom', 'app' ),
			'description'     => __( 'A columns custom block.', 'app' ),
			'render_template' => 'fragments/blocks/columns.php',
			'category'        => 'custom-blocks',
			'icon'            => 'columns',
			'keywords'        => array( 'columns' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'numbers',
			'title'           => __( 'Numbers', 'app' ),
			'description'     => __( 'A numbers block.', 'app' ),
			'render_template' => 'fragments/blocks/numbers.php',
			'category'        => 'custom-blocks',
			'icon'            => 'chart-line',
			'keywords'        => array( 'numbers' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'history-slider',
			'title'           => __( 'History Slider', 'app' ),
			'description'     => __( 'A history slider block.', 'app' ),
			'render_template' => 'fragments/blocks/history-slider.php',
			'category'        => 'custom-blocks',
			'icon'            => 'slides',
			'keywords'        => array( 'history', 'slider' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'members-listing',
			'title'           => __( 'Members Listing', 'app' ),
			'description'     => __( 'A members listing block.', 'app' ),
			'render_template' => 'fragments/blocks/members-listing.php',
			'category'        => 'custom-blocks',
			'icon'            => 'groups',
			'keywords'        => array( 'team', 'listing', 'members' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'team-members-listing',
			'title'           => __( 'Team Members Listing', 'app' ),
			'description'     => __( 'A team members listing block.', 'app' ),
			'render_template' => 'fragments/blocks/team-members-listing.php',
			'category'        => 'custom-blocks',
			'icon'            => 'groups',
			'keywords'        => array( 'team', 'listing', 'members' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'plain-text',
			'title'           => __( 'Plain Text', 'app' ),
			'description'     => __( 'A plain text block.', 'app' ),
			'render_template' => 'fragments/blocks/plain-text.php',
			'category'        => 'custom-blocks',
			'icon'            => 'editor-paste-text',
			'keywords'        => array( 'plain', 'text' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'image-with-content',
			'title'           => __( 'Image with Content', 'app' ),
			'description'     => __( 'A image with content block.', 'app' ),
			'render_template' => 'fragments/blocks/image-with-content.php',
			'category'        => 'custom-blocks',
			'icon'            => 'images-alt',
			'keywords'        => array( 'content', 'image' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'team-related',
			'title'           => __( 'Team (Related)', 'app' ),
			'description'     => __( 'A team related block.', 'app' ),
			'render_template' => 'fragments/blocks/team-related.php',
			'category'        => 'custom-blocks',
			'icon'            => 'groups',
			'keywords'        => array( 'related', 'team' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'map',
			'title'           => __( 'Map', 'app' ),
			'description'     => __( 'A map block.', 'app' ),
			'render_template' => 'fragments/blocks/map.php',
			'category'        => 'custom-blocks',
			'icon'            => 'location-alt',
			'keywords'        => array( 'map', 'contact', 'location' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'articles',
			'title'           => __( 'Articles', 'app' ),
			'description'     => __( 'A articles block.', 'app' ),
			'render_template' => 'fragments/blocks/articles.php',
			'category'        => 'custom-blocks',
			'icon'            => 'admin-post',
			'keywords'        => array( 'articles', 'blog', 'listing' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'contact',
			'title'           => __( 'Contact', 'app' ),
			'description'     => __( 'A contact block.', 'app' ),
			'render_template' => 'fragments/blocks/contact.php',
			'category'        => 'custom-blocks',
			'icon'            => 'forms',
			'keywords'        => array( 'contact', 'form' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'magazine-content',
			'title'           => __( 'Magazine Content', 'app' ),
			'description'     => __( 'A magazine content block.', 'app' ),
			'render_template' => 'fragments/blocks/magazine-content.php',
			'category'        => 'custom-blocks',
			'icon'            => 'admin-page',
			'keywords'        => array( 'magazine', 'content' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'careers',
			'title'           => __( 'Careers', 'app' ),
			'description'     => __( 'A careers block.', 'app' ),
			'render_template' => 'fragments/blocks/careers.php',
			'category'        => 'custom-blocks',
			'icon'            => 'welcome-learn-more',
			'keywords'        => array( 'careers' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'media-articles',
			'title'           => __( 'Media Articles', 'app' ),
			'description'     => __( 'A media articles block.', 'app' ),
			'render_template' => 'fragments/blocks/media.php',
			'category'        => 'custom-blocks',
			'icon'            => 'media-archive',
			'keywords'        => array( 'media', 'articles', 'magazines' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'booking',
			'title'           => __( 'Booking', 'app' ),
			'description'     => __( 'A booking block.', 'app' ),
			'render_template' => 'fragments/blocks/booking.php',
			'category'        => 'custom-blocks',
			'icon'            => 'book',
			'keywords'        => array( 'booking' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'booking-form',
			'title'           => __( 'Booking with Form', 'app' ),
			'description'     => __( 'A booking with form block.', 'app' ),
			'render_template' => 'fragments/blocks/booking-form.php',
			'category'        => 'custom-blocks',
			'icon'            => 'book-alt',
			'keywords'        => array( 'booking', 'form' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'slider',
			'title'           => __( 'Slider', 'app' ),
			'description'     => __( 'An image slide block.', 'app' ),
			'render_template' => 'fragments/blocks/slider.php',
			'category'        => 'custom-blocks',
			'icon'            => 'slides',
			'keywords'        => array( 'slider', 'slide', 'image' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'article-menu',
			'title'           => __( 'Article Menu', 'app' ),
			'description'     => __( 'Article Menu.', 'app' ),
			'render_template' => 'fragments/blocks/article-menu.php',
			'category'        => 'custom-blocks',
			'icon'            => 'book',
			'keywords'        => array( 'menu' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'locations',
			'title'           => __( 'Locations', 'app' ),
			'description'     => __( 'A locations block.', 'app' ),
			'render_template' => 'fragments/blocks/locations.php',
			'category'        => 'custom-blocks',
			'icon'            => 'location',
			'keywords'        => array( 'locations' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'center-title',
			'title'           => __( 'Central Title', 'app' ),
			'description'     => __( 'A central title block.', 'app' ),
			'render_template' => 'fragments/blocks/center-title.php',
			'category'        => 'custom-blocks',
			'icon'            => 'editor-textcolor',
			'keywords'        => array( 'center', 'title', 'content' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'center-text',
			'title'           => __( 'Central Text', 'app' ),
			'description'     => __( 'A central text block.', 'app' ),
			'render_template' => 'fragments/blocks/center-text.php',
			'category'        => 'custom-blocks',
			'icon'            => 'editor-textcolor',
			'keywords'        => array( 'center', 'text', 'content' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'video-slider',
			'title'           => __( 'Video Slider', 'app' ),
			'description'     => __( 'A video slider block.', 'app' ),
			'render_template' => 'fragments/blocks/video-slider.php',
			'category'        => 'custom-blocks',
			'icon'            => 'playlist-video',
			'keywords'        => array( 'slider', 'video' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'image-slider',
			'title'           => __( 'Image Slider', 'app' ),
			'description'     => __( 'An image slider block.', 'app' ),
			'render_template' => 'fragments/blocks/image-slider.php',
			'category'        => 'custom-blocks',
			'icon'            => 'images-alt',
			'keywords'        => array( 'slider', 'image' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'reviews',
			'title'           => __( 'Reviews', 'app' ),
			'description'     => __( 'A reviews block.', 'app' ),
			'render_template' => 'fragments/blocks/reviews.php',
			'category'        => 'custom-blocks',
			'icon'            => 'google',
			'keywords'        => array( 'reviews', 'google' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'service-image',
			'title'           => __( 'Service with Image', 'app' ),
			'description'     => __( 'A service with image block.', 'app' ),
			'render_template' => 'fragments/blocks/service-image.php',
			'category'        => 'custom-blocks',
			'icon'            => 'admin-appearance',
			'keywords'        => array( 'service', 'image' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'slider-logos',
			'title'           => __( 'Slider with Logos', 'app' ),
			'description'     => __( 'A slider with logos block.', 'app' ),
			'render_template' => 'fragments/blocks/slider-logos.php',
			'category'        => 'custom-blocks',
			'icon'            => 'images-alt2',
			'keywords'        => array( 'slider', 'logos' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'articles-slider',
			'title'           => __( 'Articles Slider', 'app' ),
			'description'     => __( 'An aticles slider block.', 'app' ),
			'render_template' => 'fragments/blocks/articles-slider.php',
			'category'        => 'custom-blocks',
			'icon'            => 'admin-page',
			'keywords'        => array( 'slider', 'articles' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'content-landing',
			'title'           => __( 'Content (Landing)', 'app' ),
			'description'     => __( 'A content block.', 'app' ),
			'render_template' => 'fragments/blocks/content-landing.php',
			'category'        => 'custom-blocks',
			'icon'            => 'welcome-write-blog',
			'keywords'        => array( 'content', 'landing' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'entry',
			'title'           => __( 'Entry', 'app' ),
			'description'     => __( 'An entry block.', 'app' ),
			'render_template' => 'fragments/blocks/entry.php',
			'category'        => 'custom-blocks',
			'icon'            => 'welcome-write-blog',
			'keywords'        => array( 'entry' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'about',
			'title'           => __( 'About', 'app' ),
			'description'     => __( 'An about block.', 'app' ),
			'render_template' => 'fragments/blocks/about.php',
			'category'        => 'custom-blocks',
			'icon'            => 'welcome-learn-more',
			'keywords'        => array( 'about' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'list',
			'title'           => __( 'Custom List', 'app' ),
			'description'     => __( 'A custom list block.', 'app' ),
			'render_template' => 'fragments/blocks/list.php',
			'category'        => 'custom-blocks',
			'icon'            => 'editor-ul',
			'keywords'        => array( 'list' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'slider-before-after',
			'title'           => __( 'Slider Before and After', 'app' ),
			'description'     => __( 'A before and after slider block.', 'app' ),
			'render_template' => 'fragments/blocks/slider-before-after.php',
			'category'        => 'custom-blocks',
			'icon'            => 'slides',
			'keywords'        => array( 'slider', 'before', 'after' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

		acf_register_block_type( array(
			'name'            => 'callout',
			'title'           => __( 'Callout', 'app' ),
			'description'     => __( 'A callout block.', 'app' ),
			'render_template' => 'fragments/blocks/callout.php',
			'category'        => 'custom-blocks',
			'icon'            => 'controls-volumeon',
			'keywords'        => array( 'callout' ),
			'align'           => 'full',
			'supports'        => [
				'align' => array( 'full' )
			],
			'mode' => 'edit',
		) );

	}
}
