<?php

if( function_exists('acf_add_options_page') ) {



	acf_add_options_page(array(

		'page_title' 	=> 'Theme General Settings',

		'menu_title'	=> 'Theme Settings',

		'menu_slug' 	=> 'theme-options',

		'capability'	=> 'edit_posts',

		'redirect'		=> false

	) );



	acf_add_options_sub_page(array(

		'page_title' 	=> 'Header Settings',

		'menu_title'	=> 'Header Settings',

		'parent_slug'	=> 'theme-options',

	));



	acf_add_options_sub_page(array(

		'page_title' 	=> 'Footer Settings',

		'menu_title'	=> 'Footer Settings',

		'parent_slug'	=> 'theme-options',

	));

	acf_add_options_sub_page(array(

		'page_title' 	=> 'Job Settings',

		'menu_title'	=> 'Job Settings',

		'parent_slug'	=> 'theme-options',

	));



}



function app_render_button( $button, $btn_class = 'btn' ) {

	if ( empty( $button['title'] ) || empty( $button['url'] ) ) {

		return;

	}



	if ( empty( $button['target'] ) ) {

		$button['target'] = '_self';

	}



	extract( $button );



	$button_url = esc_url( $url );



	return "<a href='{$button_url}' class='{$btn_class}' target='{$target}'>{$title}</a>";

}



function app_render_button_span( $button, $btn_class = 'btn' ) {

	if ( empty( $button['title'] ) || empty( $button['url'] ) ) {

		return;

	}



	if ( empty( $button['target'] ) ) {

		$button['target'] = '_self';

	}



	extract( $button );



	$button_url = esc_url( $url );



	return "<a href='{$button_url}' class='{$btn_class}' target='{$target}'><span>{$title}</span></a>";

}



function app_load_acf_menus_field_choices( $field ) {

	$field['choices']         = array();

	$menus                    = wp_get_nav_menus();

	$field['choices']['none'] = __( 'None', 'crb' );



	foreach ( $menus as $index => $menu ) {

		$value = $menu->slug;

		$label = $menu->name;



		$field['choices'][ $value ] = $label;

	}





	return $field;

}



add_action( 'init', 'app_populate_acf_fields', 10, 0 );

function app_populate_acf_fields() {

	$field_keys = array(

		'submenu',

	);



	foreach( $field_keys as $field_key ) {

		add_filter( 'acf/load_field/name=' . $field_key, 'app_load_acf_menus_field_choices' );

	}

}



function app_strip_phone_digits( $phone ) {

	return preg_replace('~\D+~', '', $phone);

}



add_filter( 'upload_mimes', 'app_mime_types' );

function app_mime_types( $mimes ) {

	$mimes['svg'] = 'image/svg+xml';



	return $mimes;

}



add_filter( 'wp_check_filetype_and_ext', 'app_check_filetype', 10, 4 );

function app_check_filetype( $data, $file, $filename, $mimes ) {

	$filetype = wp_check_filetype( $filename, $mimes );



	return [

		'ext'             => $filetype['ext'],

		'type'            => $filetype['type'],

		'proper_filename' => $data['proper_filename']

	];

}



add_filter( 'image_downsize', 'app_fix_svg_size_attributes', 10, 2 );

function app_fix_svg_size_attributes( $out, $id ) {

	$image_url  = wp_get_attachment_url( $id );

	$file_ext   = pathinfo( $image_url, PATHINFO_EXTENSION );



	if ( is_admin() || 'svg' !== $file_ext ) {

		return false;

	}



	return array( $image_url, null, null, false );

}

function app_remove_cpt_slug( $post_link, $post ) {

    if ( 'app_member' === $post->post_type && 'publish' === $post->post_status ) {
        $post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );
    }

    return $post_link;
}
add_filter( 'post_type_link', 'app_remove_cpt_slug', 10, 2 );

function app_add_cpt_post_names_to_main_query( $query ) {

    if ( ! $query->is_main_query() ) {
        return;
    }

    if ( ! isset( $query->query['page'] ) || 2 !== count( $query->query ) ) {
        return;
    }

    if ( empty( $query->query['name'] ) ) {
        return;
    }

    $query->set( 'post_type', array( 'post', 'page', 'app_member' ) );
}
add_action( 'pre_get_posts', 'app_add_cpt_post_names_to_main_query' );

add_filter( 'pre_get_posts', 'app_edit_blog_query' );
function app_edit_blog_query( $query ) {
	$sort_by = app_request_param( 'app_sort_query' );

	if( empty( $sort_by ) ) {
		return $query;
	}

	$categories       = get_categories();
	$categories_slugs = wp_list_pluck( $categories, 'slug' );

	foreach ( $categories_slugs as $slug ) {
		if ( $sort_by == $slug ) {
			$tax_args = array(
				'taxonomy' => 'category',
				'field'    => 'slug',
				'terms'    => $slug,
			);

			$query->set( 'tax_query', array( $tax_args ) );
		}
	}

	return $query;
}

function app_display_locations( $locations ) {
	foreach ($locations as $location ) {

		echo esc_html( strtoupper( $location['location'] ) );

		if ( next($locations) ) {
			echo ' | ';
		}
	}
}

function app_remove_wp_jobs_form_styles() {

	if ( is_singular( 'job_listing' ) ) {
	    wp_dequeue_style( 'wp-job-manager-frontend' );
	}
}
add_action( 'wp_enqueue_scripts', 'app_remove_wp_jobs_form_styles', 100 );

function app_render_youtube_video( $url ) {
	return str_replace( 'watch?v=', 'embed/', $url );
}
