<?php get_header(); ?>

<section class="section-default section-default--404">
	<div class="shell">
		<div class="section__content">
			<?php
			app_the_title( '<h2 class="pagetitle">', '</h2>' );
?>
			<p><?php _e( 'Please check the URL for proper spelling and capitalization.', 'app' ); ?></p>

		</div><!-- /.section__content -->
	</div><!-- /.shell -->
</section><!-- /.section-default -->

<?php get_footer(); ?>
