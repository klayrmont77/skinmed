<?php
$new_dropdown = get_field( 'app_header_use_new_dropdown_styles', 'option' );

if ( ! $new_dropdown ) {
	get_header( '', array(
		'style' => '',
	) );
} else {
	get_header( 'nav-dropdown', array(
		'style' => '',
	) );
}

if ( is_single() ) {
	get_template_part( 'loop', 'single' );
} else {
	get_template_part( 'loop' );
}

get_footer();

