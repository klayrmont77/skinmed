<?php
$search_title = get_field( 'app_header_search_terms_title', 'option' );
$search_items = get_field( 'app_header_popular_search_terms', 'option' );
?>
<div class="section__form">
	<form action="<?php echo home_url( '/' ); ?>" class="search-form" method="get" role="search">
		<label>
			<span class="screen-reader-text">
				<?php _e( 'Suche', 'app' ); ?>
			</span>
			<input type="search" title="Search for:" name="s" value="" placeholder="Suche" class="search__field">
		</label>
		<button type="submit" class="search__btn">
			<?php _e( 'Search', 'app' ); ?>
		</button>
	</form>
</div><!-- /.section__form -->

<?php if ( ! empty( $search_items ) ) : ?>
	<div class="section__popular-terms">
		<?php if ( ! empty( $search_title ) ) : ?>
			<h5>
				<?php echo esc_html( $search_title ); ?>
			</h5>
		<?php endif ?>

		<ul>
			<?php foreach ( $search_items as $search_item ) : ?>
				<li>
					<a href="<?php echo esc_url( home_url() . '/?s=' . $search_item['term'] ); ?>">
						<?php echo esc_html( $search_item['term'] ); ?>
					</a>
				</li>
			<?php endforeach ?>
		</ul>
	</div><!-- /.section__popular-terms -->
<?php endif; ?>
