<div class="header__bar">

	<?php 

	$location_link = get_field( 'app_header_location_link', 'option' );

	$phone 		   = get_field( 'app_header_phone', 'option' );



	if ( ! empty( $location_link ) ) : ?>

		<a href="<?php echo esc_url( $location_link ); ?>" class="btn-icon">

			<?php if ( ! empty( $style['style'] ) ) : ?>

				<img src="<?php bloginfo('template_directory'); ?>/resources/images/ico-pin-dark.svg" alt="">

			<?php else: ?>

				<img src="<?php bloginfo('template_directory'); ?>/resources/images/ico-pin-white.svg" alt="">

			<?php endif ?>

		</a>

	<?php endif;

	if ( has_nav_menu( 'top-bar-menu-dropdown' ) ) :
		wp_nav_menu( array(
			'theme_location'  => 'top-bar-menu-dropdown',
			'container'       => 'nav',
			'container_class' => 'nav-utilities nav-utilities--dropdown',
			'depth'           => 3,
		) );
	endif;

	if ( ! empty( $style['style'] ) ) : ?>

		<a href="#" class="btn-icon js-btn-search-overlay btn--icon-search">

			<img src="<?php bloginfo('template_directory'); ?>/resources/images/ico-search-dark.svg" alt="">

		</a>



		<?php if ( ! empty( $phone ) ) : ?>

			<a href="tel:+<?php echo app_strip_phone_digits( $phone ); ?>" class="btn-icon">

				<img src="<?php bloginfo('template_directory'); ?>/resources/images/ico-phone-dark.svg" alt="">

			</a>

		<?php endif ?>

	<?php else: ?>

		<a href="#" class="btn-icon js-btn-search-overlay btn--icon-search">

			<img src="<?php bloginfo('template_directory'); ?>/resources/images/ico-search-white.svg" alt="">

		</a>



		<?php if ( ! empty( $phone ) ) : ?>

			<a href="tel:+<?php echo app_strip_phone_digits( $phone ); ?>" class="btn-icon">

				<img src="<?php bloginfo('template_directory'); ?>/resources/images/ico-phone-new-white.svg" alt="">

			</a>

		<?php endif ?>

	<?php endif ?>

</div><!-- /.header__bar -->

