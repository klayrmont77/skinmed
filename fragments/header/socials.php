<?php
$button  = get_field( 'app_header_menu_bottom_button', 'option' );
$socials = get_field( 'app_header_socials', 'option' );

echo app_render_button( $button, 'btn-with-border' );

if ( ! empty( $socials ) ) : ?>
	<div class="socials">
		<ul>
			<?php foreach ( $socials as $social ) : ?>
				<li>
					<a href="<?php echo esc_url( $social['url'] ); ?>">
						<i class="<?php echo esc_attr( $social['icon'] ); ?>"></i>
					</a>
				</li>
			<?php endforeach; ?>
		</ul>
	</div><!-- /.socials -->
<?php endif;
