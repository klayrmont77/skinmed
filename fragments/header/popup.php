<?php

$popups = get_field( 'app_header_popups', 'option' );



if ( empty( $popups ) ) {

	return;

}


foreach ( $popups as $popup ) : ?>

	<section class="section-category js-custom-scroll" id="<?php echo esc_attr( $popup['popup_id'] ); ?>">

		<div class="shell">

			<div class="section__inner">

				<div class="section__bar">

					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="logo">

						<img src="<?php bloginfo('template_directory'); ?>/resources/images/logo-white.svg" alt="Skinmed">

					</a>



					<a href="#" class="btn-close-menu">

						<img src="<?php bloginfo('template_directory'); ?>/resources/images/ico-cross-lines.svg" alt="">

					</a>

				</div><!-- /.section__bar -->



				<div class="section__body">

					<div class="section__content">

						<h2>

							<?php echo esc_html( $popup['title'] ); ?>

						</h2>



						<?php

						echo wpautop( esc_html( $popup['text'] ) );



						echo app_render_button( $popup['button'], 'btn-with-border' );

						?>

					</div><!-- /.section__content -->



					<div class="section__actions">

						<?php echo app_render_button_span( $popup['booking_button'], 'btn-circle' ); ?>

					</div><!-- /.section__actions -->

				</div><!-- /.section__body -->



				<div class="section__nav">

					<?php if ( has_nav_menu( $popup['submenu'] ) ) :

						wp_nav_menu( array(

							'theme_location'  => $popup['submenu'],

							'container'       => 'nav',

							'container_class' => 'nav-categories',

							'depth'           => 3,

						) );

					endif; ?>



				</div><!-- /.section__nav -->

			</div><!-- /.section__inner -->

		</div><!-- /.shell -->



	</section><!-- /.section-category -->

<?php endforeach ?>



