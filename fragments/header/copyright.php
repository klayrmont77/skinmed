<?php
$copyright = get_field( 'app_header_copyright', 'option' );
$links 	   = get_field( 'app_header_links', 'option' );
?>
<div class="section__foot">
	<div class="copyright">
		<?php echo apply_filters( 'the_content', $copyright ) ?>
	</div><!-- /.copyright -->

	<?php if ( ! empty( $links ) ) : ?>
		<ul>
			<?php foreach ( $links as $link ) : ?>
				<li>
					<a href="<?php echo esc_url( $link['url'] ); ?>">
						<?php echo esc_html( $link['label'] ); ?>
					</a>
				</li>
			<?php endforeach; ?>
		</ul>
	<?php endif ?>
</div><!-- /.section__foot -->
