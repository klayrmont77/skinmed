<?php
$title 	 = get_field( 'title' );
$submenu = get_field( 'submenu' );

$button_text_main = get_field( 'button_text_main' );
$button_text_sub = get_field( 'button_text_sub' );
$button_link = get_field( 'button_link' );

if ( empty( $submenu ) ) {
	return;
}
?>
<section class="app-block-menu" data-aos="fade-up">
	<div class="shell">
		<div class="section__nav">

			<?php if ( ! empty( $title ) ) : ?>
				<h2>
					<?php echo esc_html( $title ); ?>
				</h2>
			<?php endif;

			if ( ! empty( $button_text_main ) && ! empty( $button_text_sub ) && ! empty( $button_link ) ) : ?>

				<a class="button-bordered"
			   href="<?php echo $button_link;?>">
				<span class="text-main"><?php echo $button_text_main;?></span>
				<span class="text-sub"><?php echo $button_text_sub;?></span>
			</a>

			<?php endif;

			if ( has_nav_menu( $submenu ) ) :
				wp_nav_menu( array(
					'theme_location'  => $submenu,
					'container'       => 'nav',
					'container_class' => 'nav-categories',
					'depth'           => 3,
				) );
			endif; ?>

		</div>
	</div>
</section>
