<?php
$title 		   = get_field( 'title' );
$location 	   = get_field( 'location' );
$phone 		   = get_field( 'phone' );
$schedule 	   = get_field( 'schedule' );
$map_latitude  = get_field( 'map_latitude' );
$map_longitude = get_field( 'map_longitude' );
?>
<section class="app-block-map">
	<div class="app__block-inner">
		<div class="app__block-content">
			<div class="app__block-content-inner">
				<?php if ( ! empty( $title ) ) : ?>
					<div class="app__block-heading">
						<h3>
							<?php echo esc_html( $title ); ?>
						</h3>
					</div><!-- /.app__block-heading -->
				<?php endif; ?>

				<div class="app__block-entry richtext-entry">
					<?php
					echo app_content( $location );

					if ( ! empty( $phone ) ) : ?>
						<h6>
							<a href="tel:<?php echo app_strip_phone_digits( $phone ); ?>">
								<?php echo esc_html( $phone ); ?>
							</a>
						</h6>
					<?php endif; ?>
				</div><!-- /.app__block-entry -->

				<?php if ( ! empty( $schedule ) ) : ?>
					<div class="app__block-work-time">
						<?php echo app_content( $schedule ); ?>
					</div><!-- /.app__block-work-time -->
				<?php endif ?>
			</div><!-- /.app__block-content-inner -->
		</div><!-- /.app__block-content -->

		<div class="app__block-map">
			<div class="map" id="map" data-lat="<?php echo esc_attr( $map_latitude ); ?>" data-lng="<?php echo esc_attr( $map_longitude ); ?>"></div><!-- /.map -->
		</div><!-- /.app__block-map -->
	</div><!-- /.app__block-inner -->
</section><!-- /.app-block-map -->
