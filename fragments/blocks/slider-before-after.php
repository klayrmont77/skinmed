<?php
$heading_text = get_field( 'heading_text' );
$sliders 	  = get_field( 'sliders' );

if ( empty( $sliders ) ) {
	return;
}
?>
<section class="section-slider-before-after">
	<div class="shell">
		<?php if ( ! empty( $heading_text ) ) : ?>
			<div class="section__head">
				<?php echo app_content( $heading_text ); ?>
			</div><!-- /.section__head -->
		<?php endif ?>

		<div class="section__body">
			<div class="slider-before-after js-slider-before-after">
				<div class="slides js__slides">
					<?php foreach ( $sliders as $slider ) : ?>
						<div class="slide">
							<div class="slide__images">
								<div class="slide__image">
									<span class="image-fit">
										<?php echo wp_get_attachment_image( $slider['before_image'], 'app_full_width' ); ?>
									</span>
								</div><!-- /.slide__image -->

								<div class="slide__image">
									<span class="image-fit">
										<?php echo wp_get_attachment_image( $slider['after_image'], 'app_full_width' ); ?>
									</span>
								</div><!-- /.slide__image -->
							</div><!-- /.slide__images -->
						</div><!-- /.slide -->
					<?php endforeach ?>
				</div><!-- /.slides -->

				<div class="slider__actions">
					<a href="#" class="btn-slide btn-slide--prev js__prev">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 6.921 12.429">
							<path d="M321.745,173.936l-5.861-5.861,5.861-5.861" transform="translate(-315.177 -161.861)" />
						</svg>
					</a>

					<a href="#" class="btn-slide btn-slide--next js__next">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 6.921 12.429">
							<path d="M5.861,11.721,0,5.861,5.861,0" transform="translate(0.707 0.354)" />
						</svg>
					</a>
				</div><!-- /.slider__actions -->
			</div><!-- /.slider-before-after -->
		</div><!-- /.section__body -->
	</div><!-- /.shell -->
</section><!-- /.section-slider-before-after -->
