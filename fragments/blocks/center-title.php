<?php
$text = get_field( 'text' );

if ( empty( $text ) ) {
	return;
}
?>
<section class="app-block-center-title" data-aos="fade-up">
	<div class="shell">
		<div class="app__block-content">
			<h4>
				<?php echo nl2br( esc_html( $text ) ); ?>
			</h4>
		</div><!-- /.app__block-content -->
	</div><!-- /.shell -->
</section><!-- /.app-block-center-title -->
