<?php
$image  = get_field( 'image' );
$button = get_field( 'button' );

if ( empty( $image ) && empty( $button ) ) {
	return;
}
?>
<section class="app-block-fullwidth-image" data-aos="zoom-in">
	<?php
	echo wp_get_attachment_image( $image, 'app_full_width' );

	if ( ! empty( $button ) ) : ?>
		<div class="shell">
			<?php echo app_render_button_span( $button, 'btn-circle btn-circle--dark' ); ?>
		</div><!-- /.shell -->
	<?php endif ?>
</section><!-- /.app-block-fullwidth-image -->
