<?php
$heading_text 	   = get_field( 'heading_text' );
$items 			   = get_field( 'items' );
$bottom_text 	   = get_field( 'bottom_text' );
$youtube_video_url = get_field( 'youtube_video_url' );

if ( ! empty( $youtube_video_url ) ) {
	$video_url = Carbon_Video::create( $youtube_video_url );
}
?>
<section class="section-about">
	<div class="shell">
		<?php if ( ! empty( $heading_text ) ) : ?>
			<div class="section__head">
				<?php echo app_content( $heading_text ); ?>
			</div><!-- /.section__head -->
		<?php endif ?>

		<div class="section__list">
			<div class="list-about">
				<div class="list__body">
					<ul>
						<?php foreach ( $items as $item ) : ?>
							<li>
								<div class="list__image image-fit image-fit--contain">
									<?php echo wp_get_attachment_image( $item['image'], 'app_full_width' ); ?>
								</div><!-- /.list__image -->

								<div class="list__content">
									<?php echo app_content( $item['text'] ); ?>
								</div><!-- /.list__content -->
							</li>
						<?php endforeach ?>
					</ul>
				</div><!-- /.list__body -->

				<?php if ( ! empty( $bottom_text ) ) : ?>
					<div class="list__foot">
						<?php echo app_content( $bottom_text ); ?>
					</div><!-- /.list__foot -->
				<?php endif ?>
			</div><!-- /.list-about -->
		</div><!-- /.section__list -->

		<?php if ( ! empty( $video_url ) ) : ?>
			<div class="section__video">
				<div class="block-iframe">
					<iframe type="text/html" width="" height="" src="<?php echo $video_url->get_embed_url(); ?>" allowfullscreen frameborder="0"></iframe>
				</div><!-- /.block-iframe -->
			</div><!-- /.section__video -->
		<?php endif ?>
	</div><!-- /.shell -->
</section><!-- /.section-about -->
