<?php
$text 		 = get_field( 'text' );
$alt_section = get_field( 'alt_section' );

if ( empty( $text ) ) {
	return;
}
?>
<section class="app-block-center-text" data-aos="fade-up">
	<div class="shell">
		<div class="<?php echo ( ! empty( $alt_section ) ) ? 'entry' : 'app__block-content'; ?>">
			<?php echo app_content( $text ); ?>
		</div><!-- /.app__block-content -->
	</div><!-- /.shell -->
</section><!-- /.app-block-center-text -->
