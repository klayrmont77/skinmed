<?php
$title 			= get_field( 'title' );
$subtitle 		= get_field( 'subtitle' );
$column_images  = get_field( 'column_images' );
$column_text 	= get_field( 'column_text' );
$footer_image 	= get_field( 'footer_image' );
?>
<section class="app-block-grid-content">
	<div class="app__block-head" data-aos="fade-up">
		<div class="shell">
			<?php if ( ! empty( $title ) ) : ?>
				<h2>
					<?php echo esc_html( $title ); ?>
				</h2>
			<?php endif;

			if ( ! empty( $subtitle ) ) : ?>
				<p>
					<?php echo esc_html( $subtitle ); ?>
				</p>
			<?php endif; ?>
		</div><!-- /.shell -->
	</div><!-- /.app__block-head -->

	<div class="app__block-inner">
		<?php if ( ! empty( $column_images ) ) : ?>
			<div class="app__block-images" data-aos="fade-left">
				<?php
				foreach ( $column_images as $image ) {
					echo wp_get_attachment_image( $image, 'app_full_width' );
				}
				?>
			</div><!-- /.app__block-images -->
		<?php endif;

		if ( ! empty( $column_text ) ) : ?>
			<div class="app__block-content" data-aos="fade-right">
				<div class="app__block-content-inner richtext-entry">
					<?php echo app_content( $column_text ); ?>
				</div><!-- /.app__block-content-inner -->
			</div><!-- /.app__block-content -->
		<?php endif ?>
	</div><!-- /.app__block-inner -->

	<?php if ( ! empty( $footer_image ) ) : ?>
		<div class="app__block-image" data-aos="fade-up">
			<?php echo wp_get_attachment_image( $footer_image, 'app_full_width' ); ?>
		</div><!-- /.app__block-image -->
	<?php endif; ?>
</section><!-- /.app-block-grid-content -->
