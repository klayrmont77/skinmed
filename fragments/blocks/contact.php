<?php
$title 				= get_field( 'title' );
$subtitle 			= get_field( 'subtitle' );
$text 				= get_field( 'text' );
$form 				= get_field( 'form' );
$contacts_title 	= get_field( 'contacts_title' );
$external_url 		= get_field( 'external_url' );
$external_url_label = get_field( 'external_url_label' );
$phone 				= get_field( 'app_header_phone', 'option' );
?>
<section class="app-block-contact-form">
	<div class="shell">
		<div class="app__block-inner">
			<div class="app__block-head richtext-entry" data-aos="fade-up">
				<?php if ( ! empty( $title ) ) : ?>
					<h1>
						<?php echo esc_html( $title ); ?>
					</h1>
				<?php endif;

				if ( ! empty( $subtitle ) ) : ?>
					<h4>
						<?php echo esc_html( $subtitle ); ?>
					</h4>
				<?php endif; ?>
			</div><!-- /.app__block-head -->

			<?php if ( ! empty( $text ) ) : ?>
				<div class="app__block-entry richtext-entry" data-aos="fade-up">
					<?php echo wpautop( esc_html( $text ) ); ?>
				</div><!-- /.app__block-entry -->
			<?php endif;

			if ( ! empty( $form ) ) : ?>
				<div class="app__block-form" data-aos="fade-up">
					<?php app_render_gform( $form, true ); ?>
				</div><!-- /.app__block-form -->
			<?php endif; ?>

			<div class="app__block-foot richtext-entry" data-aos="fade-up">
				<p>
					<?php echo esc_html( $contacts_title ); ?>
					<br>
					<?php if ( ! empty( $phone ) ) :
						_e( 'Telefon: ', 'app' ); ?>
						<strong>
							<a href="tel:<?php echo app_strip_phone_digits( $phone ); ?>">
								<?php echo esc_html( $phone ); ?>
							</a>
						</strong><br>
					<?php endif;

					if ( ! empty( $external_url ) && ! empty( $external_url_label ) ) : ?>
						<a href="<?php echo esc_url( $external_url ); ?>" target="_blank">
							<?php echo esc_html( $external_url_label ); ?>
						</a>
					<?php endif ?>
				</p>
			</div><!-- /.app__block-foot -->
		</div><!-- /.app__block-inner -->
	</div><!-- /.shell -->
</section><!-- /.app-block-contact-form -->
