<?php
$title = get_field( 'title' );
$text  = get_field( 'text' );
$image = get_field( 'image' );
?>
<section class="app-block-booking-tool">
	<div class="shell">
		<div class="app__block-inner">
			<div class="app__block-head" data-aos="fade-up">
				<?php if ( ! empty( $title ) ) : ?>
					<h1>
						<?php echo esc_html( $title ); ?>
					</h1>
				<?php endif;

				echo wpautop( esc_html( $text ) );
				?>
			</div><!-- /.app__block-head -->

			<?php if ( ! empty( $image ) ) : ?>
				<div class="app__block-media" data-aos="fade-up">
					<?php echo wp_get_attachment_image( $image, 'app_full_width' ); ?>
				</div><!-- /.app__block-media -->
			<?php endif ?>
		</div><!-- /.app__block-inner -->
	</div><!-- /.shell -->
</section><!-- /.app-block-booking-tool -->
