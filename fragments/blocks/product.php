<?php
$slides = get_field( 'slides' );

if ( empty( $slides ) ) {
	return;
}
?>
<section class="app-block-slider-fullheight js-fullheight" >
	<div class="slider-fullheight slider-fullheight--type-5 js-slider-fullheight">
		<div class="slider__clip swiper">
			<div class="slider__slides swiper-wrapper">
				<?php foreach ( $slides as $slide ) : ?>
					<div class="slider__slide swiper-slide image-fit">
						<?php echo wp_get_attachment_image( $slide['background_image'], 'app_full_width', false, array( 'class' => 'slider__slide-bg' ) ); ?>

						<div class="shell">
							<div class="slider__slide-inner">
								<div class="slider__slide-content animation-parent">
									<?php echo wp_get_attachment_image( $slide['logo'], 'app_full_width' );

									if ( ! empty( $slide['title'] ) ) : ?>
										<h2>
											<?php echo esc_html( $slide['title'] ); ?>
										</h2>
									<?php endif;

									echo wpautop( esc_html( $slide['text'] ) );

									if ( ! empty( $slide['button'] ) ) : ?>
										<div class="slider__slide-actions">
											<?php echo app_render_button( $slide['button'] ); ?>
										</div><!-- /.slider__slide-actions -->
									<?php endif ?>
								</div><!-- /.slider__slide-content -->

								<?php if ( ! empty( $slide['product_image'] ) ) : ?>
									<div class="slider__slide-image animation-parent">
										<?php echo wp_get_attachment_image( $slide['product_image'], 'app_full_width' ); ?>
									</div><!-- /.slider__slide-image -->
								<?php endif ?>
							</div><!-- /.slider__slide-inner -->
						</div><!-- /.shell -->
					</div><!-- /.slider__slide swiper-slide -->
				<?php endforeach ?>
			</div><!-- /.slider__slides swiper-wrapper -->

			<div class="slider__actions">
				<div class="swiper-pagination"></div>

				<div class="swiper-button-next">
					<svg xmlns="http://www.w3.org/2000/svg" width="48.047" height="48.047" viewBox="0 0 48.047 48.047"><g id="Group_174" data-name="Group 174" transform="translate(335.297 183.951) rotate(180)"><circle id="Ellipse_7" data-name="Ellipse 7" cx="23.524" cy="23.524" r="23.524" transform="translate(287.75 136.404)" fill="transparent" stroke="#fff" stroke-miterlimit="10" stroke-width="1"/><path id="Path_267" data-name="Path 267" d="M321.745,173.936l-5.861-5.861,5.861-5.861" transform="translate(-8.881 -8.148)" fill="transparent" stroke="#fff" stroke-miterlimit="10" stroke-width="1"/></g></svg>
				</div>

				<div class="swiper-button-prev">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="48.048" height="48.047" viewBox="0 0 48.048 48.047"><defs><clipPath id="clip-path"><rect width="48.048" height="48.048" fill="transparent"/></clipPath></defs><g id="Repeat_Grid_1" data-name="Repeat Grid 1" clip-path="url(#clip-path)"><g id="Group_219" data-name="Group 219" transform="translate(-287.25 -135.904)"><circle id="Ellipse_7" data-name="Ellipse 7" cx="23.524" cy="23.524" r="23.524" transform="translate(287.75 136.404)" fill="transparent" stroke="#fff" stroke-miterlimit="10" stroke-width="1"/><path id="Path_267" data-name="Path 267" d="M321.745,173.936l-5.861-5.861,5.861-5.861" transform="translate(-8.881 -8.148)" fill="transparent" stroke="#fff" stroke-miterlimit="10" stroke-width="1"/></g></g></svg>
				</div>
			</div><!-- /.slider__actions -->
		</div><!-- /.slider__clip swiper-container -->
	</div><!-- /.slider-fullheight js-slider-fullheight -->
</section><!-- /.app-block-slider-fullheight -->
