<?php
$title 	   = get_field( 'title' );
$subtitle  = get_field( 'subtitle' );
$image 	   = get_field( 'image' );
$form 	   = get_field( 'form' );
$text 	   = get_field( 'text' );
$note_text = get_field( 'note_text' );
?>
<section class="app-block-book">
	<div class="shell">
		<div class="app__block-head" data-aos="fade-up">
			<div class="app__block-head-inner">
				<?php if ( ! empty( $title ) ) : ?>
					<h2>
						<?php echo esc_html( $title ); ?>
					</h2>
				<?php endif;

				if ( ! empty( $subtitle ) ) : ?>
					<h4>
						<?php echo esc_html( $subtitle ); ?>
					</h4>
				<?php endif; ?>
			</div><!-- /.app__block-head-inner -->
		</div><!-- /.app__block-head -->

		<div class="app__block-inner">
			<?php if ( ! empty( $image ) ) : ?>
				<div class="app__block-image" data-aos="fade-left">
					<div class="app__block-image-inner">
						<?php echo wp_get_attachment_image( $image, 'app_full_width' ); ?>
					</div><!-- /.app__block-image-inner -->
				</div><!-- /.app__block-image -->
			<?php endif; ?>

			<div class="app__block-content" data-aos="fade-right">
				<div class="app__block-content-inner">
					<?php echo app_content( $text ); ?>

					<?php app_render_gform( $form, true ); ?>
				</div><!-- /.app__block-content-inner -->
			</div><!-- /.app__block-content -->
		</div><!-- /.app__block-inner -->

		<?php if ( ! empty( $note_text ) ) : ?>
			<div class="app__block-notice" data-aos="fade-up">
				<?php echo app_content( $note_text ); ?>
			</div><!-- /.app__block-notice -->
		<?php endif ?>
	</div><!-- /.shell -->
</section><!-- /.app-block-book -->
