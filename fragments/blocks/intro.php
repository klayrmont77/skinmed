<?php
$background_image = get_field( 'background_image' );
$title 			  = get_field( 'title' );
$subtitle 		  = get_field( 'subtitle' );
$button 		  = get_field( 'button' );
?>
<section class="section-intro">
	<div class="section__image">
		<span class="image-fit">
			<?php echo wp_get_attachment_image( $background_image, 'app_full_width' ); ?>
		</span>
	</div><!-- /.section__image -->

	<div class="section__body">
		<div class="shell">
			<h1>
				<?php echo esc_html( $title ); ?>
			</h1>

			<?php if ( ! empty( $subtitle ) ) : ?>
				<h6>
					<?php echo esc_html( $subtitle ); ?>
				</h6>
			<?php endif;

			if ( ! empty( $button ) ) : ?>
				<i class="ico-circle-text">
					<a href="<?php echo esc_url( $button['url'] ); ?>" target="<?php echo esc_attr( $button['target'] ); ?>"></a>

					<span>
						<?php echo esc_html( $button['title'] ); ?>
					</span>
				</i>
			<?php endif ?>
		</div><!-- /.shell -->
	</div><!-- /.section__body -->
</section><!-- /.section-intro -->
