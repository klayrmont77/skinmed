<?php
$title 			= get_field( 'title' );
$subtitle 		= get_field( 'subtitle' );
$text 			= get_field( 'text' );
$image 			= get_field( 'image' );
$booking_button = get_field( 'booking_button' );
$button 		= get_field( 'button' );
$accordions 	= get_field( 'accordions' );
?>
<section class="app-block-with-accordion-type-3">
	<div class="shell">
		<div class="app__block-inner">
			<div class="app__block-image image-fit" data-aos="fade-left">
				<?php
				echo wp_get_attachment_image( $image, 'app_full_width' );

				echo app_render_button_span( $booking_button, 'btn-circle btn-circle--dark' );
				?>
			</div><!-- /.app__block-image -->

			<div class="app__block-content" data-aos="fade-right">
				<div class="app__block-content-inner richtext-entry">
					<?php if ( ! empty( $title ) ) : ?>
						<h3>
							<?php echo esc_html( $title ); ?>
						</h3>
					<?php endif;

					if ( ! empty( $subtitle ) ) : ?>
						<h5>
							<?php echo esc_html( $subtitle ); ?>
						</h5>
					<?php endif;

					echo wpautop( esc_html( $text ) );
					
					if ( ! empty( $accordions ) ) : ?>
						<div class="accordion js-accordion">
							<?php foreach ( $accordions as $entry ) : ?>
								<div class="accordion__section">
									<div class="accordion__head">
										<div class="accordion__icon">
											<i class="fas fa-plus"></i>
											<i class="fas fa-minus"></i>
										</div><!-- /.accordion__icon -->

										<h6>
											<?php echo esc_html( $entry['title'] ); ?>
										</h6>
									</div><!-- /.accordion__head -->

									<?php if ( ! empty( $entry['text'] ) ) : ?>
										<div class="accordion__body richtext-entry">
											<?php echo wpautop( esc_html( $entry['text'] ) ); ?>
										</div><!-- /.accordion__body -->
									<?php endif; ?>
								</div><!-- /.accordion__section -->
							<?php endforeach; ?>
						</div><!-- /.accordion js-accordion -->
					<?php endif;

					if ( ! empty( $button ) ) : ?>
						<div class="app__block-actions">
							<?php echo app_render_button( $button, 'btn btn--dark-border' ); ?>
						</div><!-- /.app__block-actions -->
					<?php endif ?>
				</div><!-- /.app__block-content-inner -->
			</div><!-- /.app__block-content -->
		</div><!-- /.app__block-inner -->
	</div><!-- /.shell -->
</section><!-- /.app-block-with-accordion -->
