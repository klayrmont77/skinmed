<?php
$logo_image 	   = get_field( 'logo_image' );
$section_title 	   = get_field( 'section_title' );
$reviews_shortcode = get_field( 'reviews_shortcode' );
?>
<section class="app-block-reference-slider">
	<div class="shell">
		<div class="app__block-inner">
			<?php if ( ! empty( $logo_image ) || ! empty( $section_title ) ) : ?>
				<div class="app__block-heading">
					<?php echo wp_get_attachment_image( $logo_image, 'app_full_width' );

					if ( ! empty( $section_title ) ) : ?>
						<h6>
							<?php echo esc_html( $section_title ); ?>
						</h6>
					<?php endif ?>
				</div><!-- /.app__block-heading -->
			<?php endif; ?>

			<div class="app__block-slider">
				<div class="slider-reference">
					<div class="slider__clip">
						<?php echo do_shortcode( $reviews_shortcode ); ?>
					</div><!-- /.slider__clip swiper-container -->
				</div><!-- /.slider-reference js-slider-reference -->
			</div><!-- /.app__block-slider -->
		</div><!-- /.app__block-inner -->
	</div><!-- /.shell -->
</section><!-- /.app-block-reference-slider -->
