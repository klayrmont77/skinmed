<?php
$section_title = get_field( 'section_title' );
$slides 	   = get_field( 'slides' );

if ( empty( $slides ) ) {
	return;
}
?>
<section class="app-block-timeline-slider">
	<?php if ( ! empty( $section_title ) ) : ?>
		<div class="app__block-head" data-aos="fade-up">
			<div class="shell">
				<h3>
					<?php echo esc_html( $section_title ); ?>
				</h3>
			</div><!-- /.shell -->
		</div><!-- /.app__block-head -->
	<?php endif; ?>

	<div class="app__block-body" data-aos="fade-up">
		<div class="app__block-slider">
			<div class="slider-timeline-actions">
				<div class="swiper-button-prev">
					<svg xmlns="http://www.w3.org/2000/svg" width="48.048" height="48.047" viewBox="0 0 48.048 48.047"><g id="Group_243" data-name="Group 243" transform="translate(-287.25 -135.904)"><circle id="Ellipse_7" data-name="Ellipse 7" cx="23.524" cy="23.524" r="23.524" transform="translate(287.75 136.404)" fill="transparent" stroke="#2e2e2d" stroke-miterlimit="10" stroke-width="1"/><path id="Path_267" data-name="Path 267" d="M321.745,173.936l-5.861-5.861,5.861-5.861" transform="translate(-8.881 -8.148)" fill="transparent" stroke="#2e2e2d" stroke-miterlimit="10" stroke-width="1"/></g></svg>
				</div>

				<div class="swiper-button-next">
					<svg xmlns="http://www.w3.org/2000/svg" width="48.048" height="48.047" viewBox="0 0 48.048 48.047"><g id="Group_242" data-name="Group 242" transform="translate(335.298 183.951) rotate(180)"><circle id="Ellipse_7" data-name="Ellipse 7" cx="23.524" cy="23.524" r="23.524" transform="translate(287.75 136.404)" fill="transparent" stroke="#2e2e2d" stroke-miterlimit="10" stroke-width="1"/><path id="Path_267" data-name="Path 267" d="M321.745,173.936l-5.861-5.861,5.861-5.861" transform="translate(-8.881 -8.148)" fill="transparent" stroke="#2e2e2d" stroke-miterlimit="10" stroke-width="1"/></g></svg>
				</div>
			</div><!-- /.slider-timeline-actions -->

			<div class="slider-timeline js-slider-timeline">
				<div class="slider__clip swiper">
					<div class="slider__slides swiper-wrapper">
						<?php foreach ( $slides as $slide ) : ?>
							<div class="slider__slide swiper-slide">
								<div class="slider__slide-year">
									<h4>
										<?php echo esc_html( $slide['title'] ); ?>
									</h4>
								</div><!-- /.slider__slide-year -->

								<div class="slider__slide-entry">
									<?php echo wpautop( esc_html( $slide['text'] ) ); ?>
								</div><!-- /.slider__slide-entry -->
							</div><!-- /.slider__slide swiper-slide -->
						<?php endforeach ?>
					</div><!-- /.slider__slides swiper-wrapper -->
				</div><!-- /.slider__clip swiper-container -->
			</div><!-- /.slider-timeline js-slider-timeline -->
		</div><!-- /.app__block-slider -->
	</div><!-- /.app__block-body -->
</section><!-- /.app-block-timeline-slider -->
