<?php
$section_title = get_field( 'section_title' );
$slides 	   = get_field( 'slides' );

if ( empty( $slides ) ) {
	return;
}
?>
<section class="app-block-videos-slider">
	<div class="shell">
		<div class="app__block-inner">
			<?php if ( ! empty( $section_title ) ) : ?>
				<div class="app__block-heading" data-aos="fade-up">
					<h6>
						<?php echo esc_html( $section_title ); ?>
					</h6>
				</div><!-- /.app__block-heading -->
			<?php endif; ?>

			<div class="app__block-slider">
				<div class="slider-videos js-slider-videos">
					<div class="slider__actions">
						<div class="swiper-button-prev swiper-button-prev-video">
							<svg xmlns="http://www.w3.org/2000/svg" width="48.048" height="48.047" viewBox="0 0 48.048 48.047"><g id="Group_243" data-name="Group 243" transform="translate(-287.25 -135.904)"><circle id="Ellipse_7" data-name="Ellipse 7" cx="23.524" cy="23.524" r="23.524" transform="translate(287.75 136.404)" fill="transparent" stroke="#2e2e2d" stroke-miterlimit="10" stroke-width="1"/><path id="Path_267" data-name="Path 267" d="M321.745,173.936l-5.861-5.861,5.861-5.861" transform="translate(-8.881 -8.148)" fill="transparent" stroke="#2e2e2d" stroke-miterlimit="10" stroke-width="1"/></g></svg>
						</div>

						<div class="swiper-button-next swiper-button-next-video">
							<svg xmlns="http://www.w3.org/2000/svg" width="48.048" height="48.047" viewBox="0 0 48.048 48.047"><g id="Group_242" data-name="Group 242" transform="translate(335.298 183.951) rotate(180)"><circle id="Ellipse_7" data-name="Ellipse 7" cx="23.524" cy="23.524" r="23.524" transform="translate(287.75 136.404)" fill="transparent" stroke="#2e2e2d" stroke-miterlimit="10" stroke-width="1"/><path id="Path_267" data-name="Path 267" d="M321.745,173.936l-5.861-5.861,5.861-5.861" transform="translate(-8.881 -8.148)" fill="transparent" stroke="#2e2e2d" stroke-miterlimit="10" stroke-width="1"/></g></svg>
						</div>
					</div><!-- /.slider__actions -->

					<div class="slider__clip swiper">
						<div class="slider__slides swiper-wrapper">
							<?php foreach ( $slides as $slide ) : ?>
								<div class="slider__slide swiper-slide">
									<div class="video" data-aos="fade-up">
										<div class="video__image">
											<img src="<?php bloginfo( 'stylesheet_directory'); ?>/resources/images/temp/video-image.jpg" alt="" />

											<div class="video__iframe">
												<iframe width="560" height="315" src="<?php echo app_render_youtube_video( $slide['youtube_link'] ); ?>?enablejsapi=1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
											</div><!-- /.video__iframe -->
										</div><!-- /.video__image -->

										<?php if ( ! empty( $slide['title'] ) || ! empty( $slide['text'] ) ) : ?>
											<div class="video__content">
												<?php if ( ! empty( $slide['title'] ) ) : ?>
													<h6>
														<?php echo esc_html( $slide['title'] ); ?>
													</h6>
												<?php endif;

												echo wpautop( esc_html( $slide['text'] ) ); ?>
											</div><!-- /.video__content -->
										<?php endif ?>
									</div><!-- /.video -->
								</div><!-- /.slider__slide swiper-slide -->
							<?php endforeach; ?>
						</div><!-- /.slider__slides swiper-wrapper -->
					</div><!-- /.slider__clip swiper-container -->
				</div><!-- /.slider-videos js-slider-videos -->
			</div><!-- /.app__block-slider -->
		</div><!-- /.app__block-inner -->
	</div><!-- /.shell -->
</section><!-- /.app-block-videos-slider -->
