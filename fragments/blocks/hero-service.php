<?php
$pretitle = get_field( 'pretitle' );
$title 	  = get_field( 'title' );
$image 	  = get_field( 'image' );
?>
<section class="app-block-heading app-block-heading--smaller">
	<?php if ( ! empty( $pretitle ) || ! empty( $title ) ) : ?>
		<div class="shell">
			<div class="app__block-inner">
				<div class="app__block-content aos-init aos-animate" data-aos="fade-up">
					<?php if ( ! empty( $pretitle ) ) : ?>
						<h5>
							<?php echo esc_html( $pretitle ); ?>
						</h5>
					<?php endif;

					if ( ! empty( $title ) ) : ?>
						<h2>
							<?php echo nl2br( esc_html( $title ) ); ?>
						</h2>
					<?php endif ?>
				</div><!-- /.app__block-content -->
			</div><!-- /.app__block-inner -->
		</div><!-- /.shell -->
	<?php endif;

	echo wp_get_attachment_image( $image, 'app_full_width' ); ?>
</section>
