<?php
$position 	 = get_field( 'position' );
$image 	  	 = get_field( 'image' );
$add_caption = get_field( 'add_caption' );

if ( ! empty( $add_caption ) )  {
	$title 	  = get_field( 'title' );
	$subtitle = get_field( 'subtitle' );
}
?>
<section class="app-block-image-<?php echo esc_attr( $position ); ?>">
	<div class="app__block-inner">
		<div class="app__block-image" data-aos="fade-<?php echo esc_attr( $position ); ?>">
			<?php echo wp_get_attachment_image( $image, 'app_full_width' ); ?>
		</div><!-- /.app__block-image -->
		
		<?php if ( ! empty( $add_caption ) ) : ?>
			<div class="app__block-caption" data-aos="fade-<?php echo ( $position == 'left' ) ? 'right' : 'left'; ?>">
				<?php if ( ! empty( $title ) ) : ?>
					<h6>
						<?php echo esc_html( $title ); ?>
					</h6>
				<?php endif;

				if ( ! empty( $subtitle ) ) : ?>
					<p>
						<?php echo esc_html( $subtitle ); ?>
					</p>
				<?php endif ?>
			</div><!-- /.app__block-caption -->
		<?php endif ?>
	</div><!-- /.app__block-inner -->
</section><!-- /.app-block-image-right -->
