<?php
$image 			  = get_field( 'image' );
$text  			  = get_field( 'text' );
$reverse_section  = get_field( 'reverse_section' );
?>
<section class="app-block-image-content-type-<?php echo ( empty( $reverse_section ) ) ? '1' : '2'; ?>">
	<div class="app__block-inner">
		<?php if ( empty( $reverse_section ) && ! empty( $image ) ) : ?>
			<div class="app__block-image">
				<?php echo wp_get_attachment_image( $image, 'app_full_width' ); ?>
			</div><!-- /.app__block-image -->
		<?php endif;

		if ( ! empty( $text ) ) : ?>
			<div class="app__block-content">
				<div class="app__block-content-inner richtext-entry">
					<?php echo app_content( $text ); ?>
				</div><!-- /.app__block-content-inner -->
			</div><!-- /.app__block-content -->
		<?php endif;

		if ( ! empty( $reverse_section ) && ! empty( $image ) ) : ?>
			<div class="app__block-image">
				<?php echo wp_get_attachment_image( $image, 'app_full_width' ); ?>
			</div><!-- /.app__block-image -->
		<?php endif ?>
	</div><!-- /.app-block-inner -->
</section><!-- /.app-block-image-content-type-1 -->
