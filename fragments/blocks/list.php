<?php
$list_items = get_field( 'list_items' );

if ( empty( $list_items ) ) {
	return;
}
?>
<section class="section-list-content">
	<div class="shell">
		<div class="list-content">
			<ul>
				<?php foreach ( $list_items as $item ) : ?>
					<li>
						<blockquote>
							<?php echo app_content( $item['text'] ); ?>
						</blockquote>
					</li>
				<?php endforeach ?>
			</ul>
		</div><!-- /.list-content -->
	</div><!-- /.shell -->
</section><!-- /.section-list-content -->
