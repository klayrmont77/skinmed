<?php
$heading_text = get_field( 'heading_text' );
$custom_items = get_field( 'custom_items' );

if ( ! $custom_items ) {
	$featured_articles = get_field( 'featured_articles' );

	$args = array(
		'post_type' 	 => 'post',
		'posts_per_page' => 6
	);

	if ( ! empty( $featured_articles ) ) {
		$args['post__in'] 		= $featured_articles;
		$args['orderby']  		= 'post__in';
		$args['posts_per_page'] = -1;
	}

	$articles_query = new WP_Query( $args );
} else {
	$items = get_field( 'items' );
}
?>
<section class="section-slider-articles">
	<?php if ( ! empty( $heading_text ) ) : ?>
		<div class="section__head">
			<div class="shell">
				<blockquote>
					<?php echo app_content( $heading_text ); ?>
				</blockquote>
			</div><!-- /.shell -->
		</div><!-- /.section__head -->
	<?php endif ?>

	<div class="section__body">
		<div class="shell">
			<div class="slider-articles js-slider-articles">
				<div class="slides js__slides">
					<?php if ( ! $custom_items ) : ?>
						<?php while ( $articles_query->have_posts() ) : $articles_query->the_post();
							$category = get_the_category();
							$color 	  = get_field( 'app_post_featured_color', get_the_ID() );
							?>
							<div class="slide">
								<div class="slide__group">
									<?php if ( ! empty( $category ) ): ?>
										<div class="slide__caption" style="background-color: <?php echo ( $color ) ? esc_attr( $color ) : '#7e7e7e' ?>;">
											<p>
												<?php echo esc_html( $category[0]->name ); ?>
											</p>
										</div><!-- /.slide__caption -->
									<?php endif ?>

									<?php if ( has_post_thumbnail() ) : ?>
										<div class="slide__image image-fit">
											<a href="<?php the_permalink(); ?>"></a>

											<?php the_post_thumbnail(); ?>
										</div><!-- /.slide__image -->
									<?php endif ?>

									<div class="slide__content">
										<h5>
											<a href="<?php the_permalink(); ?>">
												<?php the_title(); ?>
											</a>
										</h5>

										<?php the_excerpt(); ?>
									</div><!-- /.slide__content -->

									<div class="slide__actions">
										<a href="<?php the_permalink(); ?>">
											<?php _e( 'Mehr erfahren', 'app' ); ?>
										</a>
									</div><!-- /.slide__actions -->
								</div><!-- /.slide__group -->
							</div><!-- /.slide -->
						<?php
						endwhile;
						wp_reset_postdata();
						?>
					<?php else: ?>
						<?php foreach ( $items as $item ) : ?>
							<div class="slide">
								<div class="slide__group">
									<?php if ( ! empty( $item['heading_title'] ) ): ?>
										<div class="slide__caption" style="background-color: <?php echo ( $item['heading_color'] ) ? esc_attr( $item['heading_color'] ) : '#7e7e7e' ?>;">
											<p>
												<?php echo esc_html( $item['heading_title'] ); ?>
											</p>
										</div><!-- /.slide__caption -->
									<?php endif ?>

									<?php if ( ! empty( $item['image'] ) ) : ?>
										<div class="slide__image image-fit">
											<a href="<?php echo esc_url( $item['button']['url'] ); ?>" target="<?php echo esc_attr( $item['button']['target'] ); ?>"></a>

											<?php echo wp_get_attachment_image( $item['image'], 'app_full_width' ); ?>
										</div><!-- /.slide__image -->
									<?php endif ?>

									<div class="slide__content">
										<h5>
											<a href="<?php echo esc_url( $item['button']['url'] ); ?>" target="<?php echo esc_attr( $item['button']['target'] ); ?>">
												<?php echo esc_html( $item['title'] ); ?>
											</a>
										</h5>

										<?php echo app_content( $item['text'] ); ?>
									</div><!-- /.slide__content -->

									<div class="slide__actions">
										<a href="<?php echo esc_url( $item['button']['url'] ); ?>" target="<?php echo esc_attr( $item['button']['target'] ); ?>">
											<?php echo esc_html( $item['button']['title'] ); ?>
										</a>
									</div><!-- /.slide__actions -->
								</div><!-- /.slide__group -->
							</div><!-- /.slide -->
						<?php endforeach ?>
					<?php endif ?>
				</div><!-- /.slides -->

				<div class="slider__actions">
					<a href="#" class="btn-slide btn-slide--prev js__prev">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 6.921 12.429">
							<path d="M321.745,173.936l-5.861-5.861,5.861-5.861" transform="translate(-315.177 -161.861)" />
						</svg>
					</a>

					<a href="#" class="btn-slide btn-slide--next js__next">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 6.921 12.429">
							<path d="M5.861,11.721,0,5.861,5.861,0" transform="translate(0.707 0.354)" />
						</svg>
					</a>
				</div><!-- /.slider__actions -->
			</div><!-- /.slider-articles -->
		</div><!-- /.shell -->
	</div><!-- /.section__body -->
</section><!-- /.section-slider-articles -->
