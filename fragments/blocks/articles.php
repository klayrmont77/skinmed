<?php
$categories = get_categories();
?>
<section class="app-block-articles">
	<div class="shell">
		<nav class="nav__categories">			
			<ul>
				<li class="current">
					<a href="<?php echo the_permalink( get_option( 'page_for_posts' ) ); ?>" data-category="all">
						<?php _e( 'ALLE', 'app' ); ?>
					</a>
				</li>

				<?php foreach ( $categories as $category ) : ?>
					<li>
						<a href="#" data-category="<?php echo esc_attr( $category->slug ); ?>">
							<?php echo esc_html( $category->name ); ?>
						</a>
					</li>
				<?php endforeach; ?>
			</ul>
		</nav><!-- /.nav -->

		<img src="<?php bloginfo('template_directory'); ?>/resources/images/spinner.gif" alt="" class="js-spinner">
		<div class="app__block-inner">
			<ol class="articles js-articles-isotope">
				<?php while ( have_posts() ) : the_post();
					$author = get_field( 'app_post_author', get_the_ID() );

					if ( empty( $author ) ) {
						$author = get_field( 'app_blog_standard_author', 'options' );
					}

					$category 		 = get_the_category();
					$category_name   = $category[0]->name;
					$category_url 	 = get_term_link( $category[0] );

					if ( empty( $author ) ) {
						$author_id 		 = get_post_field( 'post_author', get_the_ID() );
						$author_nickname = get_the_author_meta( 'nickname', $author_id );
						$author_url 	 = get_author_posts_url( $author_id );
					}
					?>

					<li class="article js-article-isotope">

						<div class="article__inner" data-aos="fade-up">
							<?php if ( has_post_thumbnail() ) : ?>
								<div class="article__image">
									<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
								</div>
							<?php endif ?>

							<div class="article__content">
								<div class="article__categories">
									<a href="<?php echo esc_url( $category_url ); ?>"><?php echo esc_html( $category_name ); ?></a>
								</div>

								<h4 class="article__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>

								<div class="article__meta">
									<ul>
										<li><?php echo get_the_date( 'j.m.Y' ); ?></li>

										<?php if ( ! empty( $author ) ) : ?>
											<li>
												<a href="<?php the_permalink( $author[0] ); ?>"><?php echo get_the_title( $author[0] ); ?></a>
											</li>
										<?php else: ?>
											<li>
												<a href="<?php echo esc_url( $author_url ); ?>"><?php echo esc_html( $author_nickname ); ?></a>
											</li>
										<?php endif; ?>
									</ul>
								</div>

								<div class="article__entry">
									<p><?php the_excerpt(); ?></p>
								</div>
							</div>
						</div>
					</li>
				<?php endwhile; ?>
			</ol>
		</div>
		<?php
		carbon_pagination('posts', array(
			'enable_prev' => true,
			'enable_next' => true,
			'enable_first' => true,
			'enable_last' => true,
			'enable_numbers' => true,
			'enable_current_page_text' => false,
			'number_limit' => 3,
			'large_page_number_limit' => 1,
			'large_page_number_interval' => -1,
			'numbers_wrapper_before' => '<ul>',
			'numbers_wrapper_after' => '</ul>',
			'prev_html' => '<a href="{URL}" class="paging__prev">' . esc_html__( '', 'crb' ) . '</a>',
			'next_html' => '<a href="{URL}" class="paging__next">' . esc_html__( '', 'crb' ) . '</a>',
			'first_html' => '<a href="{URL}" class="paging__first"></a>',
			'last_html' => '<a href="{URL}" class="paging__last"></a>',
			'number_html' => '<li><a href="{URL}">{PAGE_NUMBER}</a></li>',
			'current_number_html' => '<li class="current"><a href="{URL}">{PAGE_NUMBER}</a></li>',
			'limiter_html' => '<li class="paging__spacer">...</li>',
		));
		?>
	</div>
</section>
