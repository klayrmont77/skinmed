<?php
$members = get_field( 'members' );

if ( empty( $members ) ) {
	return;
}
?>
<section class="app-block-members">
	<div class="shell">
		<div class="members">
			<?php foreach ( $members as $member ) : ?>
				<div class="member" data-aos="fade-up">
					<div class="member__inner">
						<div class="member__image image-fit">
							<?php echo wp_get_attachment_image( $member['image'], 'app_full_width' ); ?>
						</div><!-- /.member__image -->

						<div class="member__content">
							<div class="member__name">
								<p>
									<?php echo esc_html( $member['name'] ); ?>
								</p>
							</div><!-- /.member__name -->
						</div><!-- /.member__content -->
					</div><!-- /.member__inner -->
				</div><!-- /.member -->
			<?php endforeach ?>
		</div><!-- /.members -->
	</div><!-- /.shell -->
</section><!-- /.app-block-members -->
