<?php
$section_type = get_field( 'section_type' );
$image 		  = get_field( 'image' );
$text 		  = get_field( 'text' );
$logo 		  = get_field( 'logo' );

$section_class = '';

if ( $section_type == 'reverse' ) {
	$section_class = 'revert';
} elseif ( $section_type == 'reverse-alt' ) {
	$section_class = 'revert-alt';
} elseif ( $section_type == 'full' ) {
	$section_class = 'full__image';
}

?>
<section class="app-block-service-block <?php echo esc_attr( $section_class ); ?>">
	<div class="shell">
		<div class="app__block-inner">
			<?php if ( ! empty( $image ) ) : ?>
				<div class="app__block-image" data-aos="fade-right">
					<?php echo wp_get_attachment_image( $image, 'app_full_width' ); ?>
				</div><!-- /.app__block-image -->
			<?php endif;

			if ( ! empty( $text ) || ! empty( $logo ) ) : ?>
				<div class="app__block-caption" data-aos="fade-left">
					<?php
					echo wp_get_attachment_image( $logo, 'app_full_width' );
	
					echo app_content( $text );
					?>
				</div><!-- /.app__block-caption -->
			<?php endif ?>
		</div><!-- /.app__block-inner -->
	</div><!-- /.shell -->
</section><!-- /.app-block-service-block -->
