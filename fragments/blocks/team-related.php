<?php
$section_title = get_field( 'section_title' );
$members 	   = get_field( 'members' );

if ( empty( $members ) ) {
	return;
}

$args = array(
	'post_type' 	 => 'app_member',
	'posts_per_page' => -1,
	'post__in' 		 => $members,
	'orderby' 		 => 'post__in'
);

$members_query = new WP_Query( $args );
?>
<section class="app-block-members-slider-related">
	<div class="shell">
		<div class="app__block-inner">
			<?php if ( ! empty( $section_title ) ) : ?>
				<div class="app__block-heading">
					<h3>
						<?php echo esc_html( $section_title ); ?>
					</h3>
				</div><!-- /.app__block-heading -->
			<?php endif; ?>

			<div class="app__block-slider">
				<div class="slider-members-related js-slider-members-related">
					<div class="slider__actions">
						<div class="swiper-button-prev">
							<svg xmlns="http://www.w3.org/2000/svg" width="48.048" height="48.047" viewBox="0 0 48.048 48.047"><g id="Group_243" data-name="Group 243" transform="translate(-287.25 -135.904)"><circle id="Ellipse_7" data-name="Ellipse 7" cx="23.524" cy="23.524" r="23.524" transform="translate(287.75 136.404)" fill="transparent" stroke="#2e2e2d" stroke-miterlimit="10" stroke-width="1"/><path id="Path_267" data-name="Path 267" d="M321.745,173.936l-5.861-5.861,5.861-5.861" transform="translate(-8.881 -8.148)" fill="transparent" stroke="#2e2e2d" stroke-miterlimit="10" stroke-width="1"/></g></svg>
						</div>

						<div class="swiper-button-next">
							<svg xmlns="http://www.w3.org/2000/svg" width="48.048" height="48.047" viewBox="0 0 48.048 48.047"><g id="Group_242" data-name="Group 242" transform="translate(335.298 183.951) rotate(180)"><circle id="Ellipse_7" data-name="Ellipse 7" cx="23.524" cy="23.524" r="23.524" transform="translate(287.75 136.404)" fill="transparent" stroke="#2e2e2d" stroke-miterlimit="10" stroke-width="1"/><path id="Path_267" data-name="Path 267" d="M321.745,173.936l-5.861-5.861,5.861-5.861" transform="translate(-8.881 -8.148)" fill="transparent" stroke="#2e2e2d" stroke-miterlimit="10" stroke-width="1"/></g></svg>
						</div>
					</div><!-- /.slider__actions -->

					<div class="slider__clip swiper">
						<div class="slider__slides swiper-wrapper members-type-2">
							<?php while ( $members_query->have_posts() ) : $members_query->the_post();
								$member_id = get_the_ID();
								$location  = get_field( 'app_member_location', $member_id );
								$position  = get_field( 'app_member_position', $member_id );
								?>
								<div class="slider__slide swiper-slide">
									<div class="member" data-aos="fade-up">
										<a href="<?php the_permalink(); ?>">
											<div class="member__inner">
												<?php if ( has_post_thumbnail() ) : ?>

														<div class="member__image js-equalize-height">
															<?php the_post_thumbnail(); ?>
														</div><!-- /.member__image -->

												<?php endif; ?>

												<div class="member__content">
													<?php if ( ! empty( $location ) ) : ?>
														<div class="member__city">
															<p>
																<?php echo esc_html( $location ); ?>
															</p>
														</div><!-- /.member__city -->
													<?php endif; ?>

													<div class="member__name">
														<p>
															<?php the_title(); ?>
														</p>
													</div><!-- /.member__name -->

													<?php if ( ! empty( $position ) ) : ?>
														<div class="member__pos">
															<p>
																<?php echo esc_html( $position ); ?>
															</p>
														</div><!-- /.member__pos -->
													<?php endif; ?>
												</div><!-- /.member__content -->
											</div><!-- /.member__inner -->
										</a>
									</div><!-- /.member -->
								</div><!-- /.slider__slide swiper-slide -->
							<?php endwhile;
							wp_reset_postdata();
							?>
						</div><!-- /.slider__slides swiper-wrapper -->
					</div><!-- /.slider__clip swiper-container -->
				</div><!-- /.slider-members-related js-slider-members-related -->
			</div><!-- /.app__block-slider -->
		</div><!-- /.app__block-inner -->
	</div><!-- /.shell -->
</section><!-- /.app-block-members-slider-related -->
