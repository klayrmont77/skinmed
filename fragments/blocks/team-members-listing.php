<?php
$members = get_field( 'members' );

if ( empty( $members ) ) {
	return;
}
$args = array(
	'post_type' 	 => 'app_member',
	'posts_per_page' => -1,
	'post__in' 		 => $members,
	'orderby' 		 => 'post__in'
);

$members_query = new WP_Query( $args );
?>
<section class="app-block-members-type-2">
	<div class="shell">
		<div class="app__block-inner">
			<div class="members-type-2">
				<?php while ( $members_query->have_posts() ) : $members_query->the_post();
					$member_id = get_the_ID();
					$position  = get_field( 'app_member_position', $member_id );
					$location  = get_field( 'app_member_location', $member_id );
					?>
					<div class="member" data-aos="fade-up">
						<a href="<?php the_permalink(); ?>">
							<div class="member__inner">
								<?php if ( has_post_thumbnail() ) : ?>

										<div class="member__image js-equalize-height">
											<?php the_post_thumbnail(); ?>
										</div><!-- /.member__image -->

								<?php endif ?>

								<div class="member__content">
									<?php if ( ! empty( $location ) ) : ?>
										<div class="member__city">
											<p>
												<?php echo esc_html( $location ); ?>
											</p>
										</div><!-- /.member__city -->
									<?php endif; ?>

									<div class="member__name">
										<p>
											<?php the_title(); ?>
										</p>
									</div><!-- /.member__name -->

									<?php if ( ! empty( $position ) ) : ?>
										<div class="member__pos">
											<p>
												<?php echo esc_html( $position ); ?>
											</p>
										</div><!-- /.member__pos -->
									<?php endif; ?>
								</div><!-- /.member__content -->
							</div><!-- /.member__inner -->
						</a>
					</div><!-- /.member -->
				<?php endwhile;
				wp_reset_postdata();
				?>
			</div><!-- /.members -->
		</div><!-- /.app__block-inner -->
	</div><!-- /.shell -->
</section><!-- /.app-block-members -->
