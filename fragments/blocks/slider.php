<?php
$slides = get_field( 'slides' );

if ( empty( $slides ) ) {
	return;
}
?>
<div class="app-block-slider-images">
	<div class="app-block-inner" data-aos="fade-up">
		<div class="slider-images js-slider-images">
			<div class="slider__clip swiper">
				<div class="slider__slides swiper-wrapper">
					<?php foreach ( $slides as $slide ) : ?>
						<div class="slider__slide swiper-slide">
							<div class="slider__slide-inner">
								<div class="slider__slide-content">
									<?php if ( ! empty( $slide['title'] ) ) : ?>
										<h5>
											<?php echo esc_html( $slide['title'] ); ?>
										</h5>
									<?php endif;

									if ( ! empty( $slide['subtitle'] ) ) : ?>
										<p>
											<?php echo esc_html( $slide['subtitle'] ); ?>
										</p>
									<?php endif; ?>
								</div><!-- /.slider__slide-content -->

								<div class="slider__slide-image">
									<div class="slider__slide-image-inner">
										<?php echo wp_get_attachment_image( $slide['image'], 'app_full_width' ); ?>
									</div><!-- /.slider__slide-image-inner -->
								</div><!-- /.slider__slide-image -->
							</div><!-- /.slider__slide-inner -->
						</div><!-- /.slider__slide swiper-slide -->
					<?php endforeach ?>
				</div><!-- /.slider__slides swiper-wrapper -->
			</div><!-- /.slider__clip swiper-container -->

			<div class="slider__actions">
				<div class="swiper-button-prev">
					<svg xmlns="http://www.w3.org/2000/svg" width="48.048" height="48.047" viewBox="0 0 48.048 48.047"><g id="Group_243" data-name="Group 243" transform="translate(-287.25 -135.904)"><circle id="Ellipse_7" data-name="Ellipse 7" cx="23.524" cy="23.524" r="23.524" transform="translate(287.75 136.404)" fill="transparent" stroke="#2e2e2d" stroke-miterlimit="10" stroke-width="1"/><path id="Path_267" data-name="Path 267" d="M321.745,173.936l-5.861-5.861,5.861-5.861" transform="translate(-8.881 -8.148)" fill="transparent" stroke="#2e2e2d" stroke-miterlimit="10" stroke-width="1"/></g></svg>
				</div>

				<div class="swiper-button-next">
					<svg xmlns="http://www.w3.org/2000/svg" width="48.048" height="48.047" viewBox="0 0 48.048 48.047"><g id="Group_242" data-name="Group 242" transform="translate(335.298 183.951) rotate(180)"><circle id="Ellipse_7" data-name="Ellipse 7" cx="23.524" cy="23.524" r="23.524" transform="translate(287.75 136.404)" fill="transparent" stroke="#2e2e2d" stroke-miterlimit="10" stroke-width="1"/><path id="Path_267" data-name="Path 267" d="M321.745,173.936l-5.861-5.861,5.861-5.861" transform="translate(-8.881 -8.148)" fill="transparent" stroke="#2e2e2d" stroke-miterlimit="10" stroke-width="1"/></g></svg>
				</div>
			</div><!-- /.slider__actions -->
		</div><!-- /.slider-images js-slider-images -->
	</div><!-- /.app__block-inner -->
</div><!-- /.app-block-slider-images -->
