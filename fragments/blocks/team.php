<?php
$background_image = get_field( 'background_image' );
$pretitle 		  = get_field( 'pretitle' );
$title 			  = get_field( 'title' );
$text 			  = get_field( 'text' );
$team_members 	  = get_field( 'team_members' );

if ( empty( $team_members ) ) {
	return;
}

$args = array(
	'post_type' 	 => 'app_member',
	'posts_per_page' => -1,
	'post__in' 		 => $team_members,
	//'orderby' 		 => 'post__in',
	'orderby' 		 => 'rand',
);

$members_query = new WP_Query( $args );
?>
<section class="app-block-members-slider js-fullheight">
	<?php if ( ! empty( $background_image ) ) : ?>
		<div class="app__block-bg image-fit">
			<?php echo wp_get_attachment_image( $background_image, 'app_full_width' ); ?>
		</div><!-- /.app__block-bg -->
	<?php endif ?>

	<div class="app__block-inner">
		<div class="app__block-head animation-parent">
			<?php if ( ! empty( $pretitle ) ) : ?>
				<h5>
					<?php echo esc_html( $pretitle ); ?>
				</h5>
			<?php endif;

			if ( ! empty( $title ) ) : ?>
				<h2>
					<?php echo esc_html( $title ); ?>
				</h2>
			<?php endif;

			echo wpautop( esc_html( $text ) ); ?>
		</div><!-- /.app__block-head -->

		<div class="app__block-slider animation-parent">
			<div class="slider-members js-slider-members">
				<div class="slider__clip swiper">
					<div class="slider__slides swiper-wrapper">
						<?php
						$counter = 1;
						while ( $members_query->have_posts() ) : $members_query->the_post();
							$position = get_field( 'app_member_position', get_the_ID() );
							$image    = get_field( 'app_alpha_transparent', get_the_ID() );
							?>
							<div class="slider__slide slider__slide--size-<?php echo esc_attr( $counter ); ?> swiper-slide">
								<div class="slider__slide-member">
									<div class="slider__slide-member-image">
											<div class="slider__slide-member-content">
											<h5>
												<?php the_title(); ?>
											</h5>

											<?php if ( ! empty( $position ) ) : ?>
												<p>
													<?php echo esc_html( $position ); ?>
												</p>
											<?php endif; ?>

											<div class="slider__slide-actions">
												<a href="<?php the_permalink(); ?>" class="btn">
													<?php _e( 'Mehr erfahren', 'crb' ); ?>
												</a>
											</div><!-- /.slider__slide-actions -->
										</div><!-- /.slider__slide-member-content -->

										<?php
										if ( ! empty( $image ) ) {
											echo wp_get_attachment_image( $image, '' );
										} else {
											the_post_thumbnail();
										}
										?>

									</div><!-- /.slider__slide-member-image -->
								</div><!-- /.slider__slide-member -->
							</div><!-- /.slider__slide swiper-slide -->
						<?php
						$counter++;
						endwhile;
						wp_reset_postdata();
						?>
					</div><!-- /.slider__slides swiper-wrapper -->

					<div class="slider__actions">
						<div class="swiper-button-next enabled">
							<svg xmlns="http://www.w3.org/2000/svg" width="48.047" height="48.047" viewBox="0 0 48.047 48.047"><g id="Group_174" data-name="Group 174" transform="translate(335.297 183.951) rotate(180)"><circle id="Ellipse_7" data-name="Ellipse 7" cx="23.524" cy="23.524" r="23.524" transform="translate(287.75 136.404)" fill="transparent" stroke="#fff" stroke-miterlimit="10" stroke-width="1"/><path id="Path_267" data-name="Path 267" d="M321.745,173.936l-5.861-5.861,5.861-5.861" transform="translate(-8.881 -8.148)" fill="transparent" stroke="#fff" stroke-miterlimit="10" stroke-width="1"/></g></svg>
						</div>

						<div class="swiper-button-prev enabled">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="48.048" height="48.047" viewBox="0 0 48.048 48.047"><defs><clipPath id="clip-path"><rect width="48.048" height="48.048" fill="transparent"/></clipPath></defs><g id="Repeat_Grid_1" data-name="Repeat Grid 1" clip-path="url(#clip-path)"><g id="Group_219" data-name="Group 219" transform="translate(-287.25 -135.904)"><circle id="Ellipse_7" data-name="Ellipse 7" cx="23.524" cy="23.524" r="23.524" transform="translate(287.75 136.404)" fill="transparent" stroke="#fff" stroke-miterlimit="10" stroke-width="1"/><path id="Path_267" data-name="Path 267" d="M321.745,173.936l-5.861-5.861,5.861-5.861" transform="translate(-8.881 -8.148)" fill="transparent" stroke="#fff" stroke-miterlimit="10" stroke-width="1"/></g></g></svg>
						</div>
					</div><!-- /.slider__actions -->
				</div><!-- /.slider__clip swiper-container -->
			</div><!-- /.slider-members js-slider-members -->
		</div><!-- /.app__block-slider -->
	</div><!-- /.app__block-inner -->
</section><!-- /.app-block-members-slider -->
