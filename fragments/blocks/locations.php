<?php
$title 	   = get_field( 'title' );
$text  	   = get_field( 'text' );
$locations = get_field( 'locations' );

if ( empty( $locations ) ) {
	return;
}
?>
<section class="app-block-locations" data-aos="fade-up">
	<div class="shell">
		<div class="location__head">
			<?php if ( ! empty( $title ) ) : ?>
				<h2>
					<?php echo esc_html( $title ); ?>
				</h2>
			<?php endif;

			if ( ! empty( $text ) ) {
				echo wpautop( esc_html( $text ) );
			}
			?>		
		</div><!-- /.location__head -->

		<div class="locations__lists">
			<?php
			$counter = 1;
			foreach ( $locations as $location ) : ?>
				<div class="single__location" data-aos="<?php echo ( $counter % 2 == 0 ) ? 'fade-left' : 'fade-right'; ?>">
					<div class="location__image">
						<a href="<?php echo esc_url( $location['url'] ); ?>">
							<?php echo wp_get_attachment_image( $location['image'], 'app_full_width' ); ?>
						</a>
					</div><!-- /.location__image -->

					<div class="entry">
						<h4>
							<a href="<?php echo esc_url( $location['url'] ); ?>">
								<?php echo esc_html( $location['name'] ); ?>
							</a>
						</h4>

						<?php if ( ! empty( $location['company'] ) ) : ?>
							<h6>
								<?php echo esc_html( $location['company'] ); ?>
							</h6>
						<?php endif;

						if ( ! empty( $location['address'] ) && ! empty( $location['email'] ) ) : ?>
							<p>
								<?php
								if ( ! empty( $location['address'] ) ) {
									echo nl2br( esc_html( $location['address'] ) );
								}

								if ( ! empty( $location['email'] ) ) : ?>
									<br>
									<a href="mailto:<?php echo antispambot( $location['email'] ); ?>">
										<?php echo esc_html( $location['email'] ); ?>
									</a>
								<?php endif; ?>
							</p>
						<?php endif;

						if ( ! empty( $location['phone'] ) ) : ?>
							<p>
								<a href="tel:<?php echo app_strip_phone_digits( $location['phone'] ); ?>">
									<?php echo esc_html( $location['phone'] ); ?>
								</a>
							</p>
						<?php endif;

						if ( ! empty( $location['booking_url'] ) ) : ?>
							<a href="<?php echo esc_url( $location['booking_url'] ); ?>" class="buchen-link">
								<img src="<?php bloginfo('template_directory'); ?>/resources/images/temp/buchen-image.png" alt="Buchen Icon" class="ico-buchen">

								<span>Online Buchen</span>
							</a>
						<?php endif ?>
					</div><!-- /.entry -->
				</div><!-- /.single__location -->
			<?php
			$counter++;
		endforeach; ?>
		</div><!-- /.locations__lists -->
	</div><!-- /.shell -->
</section><!-- /.app-block-locations -->
