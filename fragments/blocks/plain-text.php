<?php
$title = get_field( 'title' );
$text  = get_field( 'text' );
?>
<section class="app-block-plain-text">
	<div class="shell">
		<div class="app__block-content richtext-entry">
			<?php if ( ! empty( $title ) ) : ?>
				<h4>
					<?php echo esc_html( $title ); ?>
				</h4>
			<?php endif;

			if ( ! empty( $text ) ) : ?>
				<p>
					<strong>
						<?php echo esc_html( $text ); ?>
					</strong>
				</p>
			<?php endif; ?>
		</div><!-- /.app__block-content -->
	</div><!-- /.shell -->
</section><!-- /.app-block-plain-text -->
