<?php
$number_of_articles = get_field( 'number_of_articles' );
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

if ( empty( $number_of_articles ) ) {
	$number_of_articles = 5;
}

$args 				= array(
	'post_type' 	 => 'app_media',
	'posts_per_page' => $number_of_articles,
	'paged'			 => $paged,
);

$media_query = new WP_Query( $args );
?>
<section class="app-block-articles app-block-articles--media">
	<div class="shell">
		<div class="app__block-inner">
			<ol class="articles articles--media js-articles-isotope">
				<?php while ( $media_query->have_posts() ) : $media_query->the_post();
					$terms 		   = get_the_terms( get_the_ID(), 'media_category' );
					$taxonomy_name = $terms[0]->name;
					?>
					<li class="article js-article-isotope">
						<div class="article__inner" data-aos="fade-up">
							<?php if ( has_post_thumbnail() ) : ?>
								<div class="article__image">
									<a href="<?php the_permalink(); ?>">
										<?php the_post_thumbnail(); ?>
									</a>
								</div><!-- /.article__image -->
							<?php endif ?>

							<div class="article__content">
								<?php if ( ! empty( $taxonomy_name ) ) : ?>
									<div class="article__magazine">
										<p>
											<?php echo esc_html( $taxonomy_name ); ?>
										</p>
									</div><!-- /.article__magazine -->
								<?php endif; ?>

								<h4 class="article__title">
									<a href="<?php the_permalink(); ?>">
										<?php the_title() ?>
									</a>
								</h4><!-- /.article__title -->

								<div class="article__meta">
									<ul>
										<li>
											<?php echo get_the_date( 'j.m.Y' ); ?>
										</li>
									</ul>
								</div><!-- /.article__meta -->

								<div class="article__entry">
									<p>
										<?php the_excerpt(); ?>
									</p>
								</div><!-- /.article__entry -->
							</div><!-- /.article__content -->
						</div><!-- /.article__inner -->
					</li><!-- /.article -->
				<?php endwhile;
				wp_reset_postdata();
				?>
			</ol><!-- /.articles -->
			<?php 
			carbon_pagination('posts', array(
				'wrapper_before' => '<div class="paging">',
				'wrapper_after' => '</div>',
				'total_pages' => $media_query->max_num_pages,
				'enable_prev' => true,
				'enable_next' => true,
				'enable_first' => true,
				'enable_last' => true,
				'enable_numbers' => true,
				'enable_current_page_text' => false,
				'number_limit' => 3,
				'large_page_number_limit' => 1,
				'large_page_number_interval' => 5,
				'numbers_wrapper_before' => '<ul>',
				'numbers_wrapper_after' => '</ul>',
				'prev_html' => '<a href="{URL}" class="paging__prev">' . esc_html__( '', 'crb' ) . '</a>',
				'next_html' => '<a href="{URL}" class="paging__next">' . esc_html__( '', 'crb' ) . '</a>',
				'first_html' => '<a href="{URL}" class="paging__first"></a>',
				'last_html' => '<a href="{URL}" class="paging__last"></a>',
				'number_html' => '<li><a href="{URL}">{PAGE_NUMBER}</a></li>',
				'current_number_html' => '<li class="current"><a href="{URL}">{PAGE_NUMBER}</a></li>',
				'limiter_html' => '<li class="paging__spacer">...</li>',
			)); 
			?>
		</div><!-- /.app__block-inner -->
	</div><!-- /.shell -->
</section><!-- /.app-block-articles -->
