<?php
$text 	= get_field( 'text' );
$button = get_field( 'button' );
?>
<section class="section-content-about">
	<div class="shell">
		<blockquote>
			<?php echo app_content( $text );

			if ( ! empty( $button ) ) : ?>
				<i class="ico-circle-text ico-circle-text--right">
					<a href="<?php echo esc_url( $button['url'] ); ?>" target="<?php echo esc_attr( $button['target'] ); ?>"></a>

					<span>
						<?php echo esc_html( $button['title'] ); ?>
					</span>
				</i>
			<?php endif ?>
		</blockquote>
	</div><!-- /.shell -->
</section><!-- /.section-content-about -->
