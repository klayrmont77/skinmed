<?php
$title 	 = get_field( 'title' );
$numbers = get_field( 'numbers' );

if ( empty( $numbers ) ) {
	return;
}
?>
<section class="app-block-numbers">
	<div class="shell">
		<?php if ( ! empty( $title ) ) : ?>
			<div class="app__block-head" data-aos="fade-up">
				<h4>
					<?php echo esc_html( $title ); ?>
				</h4>
			</div><!-- /.app__block-head -->
		<?php endif; ?>

		<div class="app__block-body">
			<ul>
				<?php foreach ( $numbers as $number ) : ?>
					<li data-aos="fade-up">
						<p>
							<?php echo esc_html( $number['title'] ); ?>
						</p>

						<h3>
							<?php echo esc_html( $number['number'] ); ?>
						</h3>
					</li>
				<?php endforeach; ?>
			</ul>
		</div><!-- /.app__block-body -->
	</div><!-- /.shell -->
</section><!-- /.app-block-numbers -->
