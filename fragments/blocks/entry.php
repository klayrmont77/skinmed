<?php
$text 		 = get_field( 'text' );
$image 		 = get_field( 'image' );
$bottom_text = get_field( 'bottom_text' );

if ( empty( $text ) && empty( $bottom_text ) ) {
	return;
}
?>
<section class="section-entry">
	<div class="shell">
		<div class="post">
			<div class="post__entry">
				<?php echo app_content( $text ); ?>

				<?php if ( ! empty( $image ) ) : ?>
					<p>
						<?php echo wp_get_attachment_image( $image, 'app_full_width', false, array( 'class' => 'alignfull' ) ); ?>
					</p>
				<?php endif ?>

				<?php echo app_content( $bottom_text ); ?>
			</div><!-- /.post__entry -->
		</div><!-- /.post -->
	</div><!-- /.shell -->
</section><!-- /.section-entry -->
