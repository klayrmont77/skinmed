<?php
$pretitle = get_field( 'pretitle' );
$title 	  = get_field( 'title' );
$text 	  = get_field( 'text' );
$button   = get_field( 'button' );
?>
<section class="app-block-heading-with-actions">
	<div class="shell">
		<div class="app__block-inner">
			<div class="app__block-content" data-aos="fade-up">
				<?php if ( ! empty( $pretitle ) ) : ?>
					<h5>
						<?php echo esc_html( $pretitle ); ?>
					</h5>
				<?php endif;

				if ( ! empty( $title ) ) : ?>
					<h2>
						<?php echo esc_html( $title ); ?>
					</h2>
				<?php endif;

				echo wpautop( esc_html( $text ) ); ?>
			</div><!-- /.app__block-content -->

			<?php if ( ! empty( $button ) ) : ?>
				<div class="app__block-actions">
					<?php echo app_render_button_span( $button, 'btn-circle btn-circle--blue' ); ?>
				</div><!-- /.app__block-actions -->
			<?php endif; ?>
		</div><!-- /.app__block-inner -->
	</div><!-- /.shell -->
</section><!-- /.app-block-heading-with-actions -->
