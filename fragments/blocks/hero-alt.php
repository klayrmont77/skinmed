<?php
$larger_title = get_field( 'larger_title' );
$title 		  = get_field( 'title' );
$text  		  = get_field( 'text' );
$less_spacing = get_field( 'less_spacing' );
?>
<section class="app-block-heading <?php echo ( ! empty( $less_spacing ) ) ? 'app-block-heading--smaller' : ''; ?>">
	<div class="shell">
		<div class="app__block-inner">
			<div class="app__block-content" data-aos="fade-up">
				<?php if ( ! empty( $title ) ) :
					if ( ! empty( $larger_title ) ) : ?>
						<h1>
							<?php echo esc_html( $title ); ?>
						</h1>
					<?php else: ?>
						<h3>
							<?php echo esc_html( $title ); ?>
						</h3>
					<?php endif;
				endif ?>

				<?php echo app_content( $text ); ?>
			</div><!-- /.app__block-content -->
		</div><!-- /.app__block-inner -->
	</div><!-- /.shell -->
</section><!-- /.app-block-heading -->
