<?php
$title = get_field( 'title' );
$text  = get_field( 'text' );
?>
<section class="app-block-heading-center">
	<div class="shell">
		<div class="app__block-inner js-fullheight">
			<div class="app__block-content"  data-aos="zoom-in">
				<?php if ( ! empty( $title ) ) : ?>
					<h2>
						<?php echo esc_html( $title ); ?>
					</h2>
				<?php endif;

				echo wpautop( esc_html( $text ) ); ?>
			</div><!-- /.app__block-content -->
		</div><!-- /.app__block-inner -->
	</div><!-- /.shell -->
</section><!-- /.app-block-heading-center -->
