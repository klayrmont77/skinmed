<?php
$section_title = get_field( 'section_title' );
$image 		   = get_field( 'image' );
$button 	   = get_field( 'button' );
$text 		   = get_field( 'text' );
?>
<section class="section-image-and-content" id="ansprechpartner">
	<div class="shell">
		<?php if ( ! empty( $section_title ) ) : ?>
			<div class="section__head">
				<h3>
					<?php echo esc_html( $section_title ); ?>
				</h3>
			</div><!-- /.section__head -->
		<?php endif ?>

		<div class="section__body">
			<?php if ( ! empty( $image ) || ! empty( $button ) ) : ?>
				<div class="section__body-image">
					<span>
						<?php
						echo wp_get_attachment_image( $image, 'app_full_width' );

						if ( ! empty( $button ) ) : ?>
							<i class="ico-circle-text">
								<a href="<?php echo esc_url( $button['url'] ); ?>" target="<?php echo esc_attr( $button['target'] ); ?>"></a>

								<span>
									<?php echo esc_html( $button['title'] ); ?>
								</span>
							</i>
						<?php endif ?>
					</span>
				</div><!-- /.section__body-image -->
			<?php endif ?>

			<div class="section__body-content">
				<?php echo app_content( $text ); ?>
			</div><!-- /.section__body-content -->
		</div><!-- /.section__body -->
	</div><!-- /.shell -->
</section><!-- /.section-image-and-content -->
