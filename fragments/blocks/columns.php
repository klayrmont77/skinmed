<?php
$title 	 = get_field( 'title' );
$columns = get_field( 'columns' );

if ( empty( $columns ) ) {
	return;
}
?>
<section class="app-block-custom-cols">
	<div class="shell">
		<?php if ( ! empty( $title ) ) : ?>
			<header class="app__block-head" data-aos="fade-up">
				<h4>
					<?php echo esc_html( $title ); ?>
				</h4>
			</header><!-- /.app__block-head -->
		<?php endif ?>

		<div class="app__block-cols">
			<?php foreach ( $columns as $index => $col ) : ?>
				<div class="app__block-col">
					<?php if ( ! empty( $col['add_spacing'] ) ) : ?>
						<div class="app__block-spacer"></div><!-- /.app__block-spacer -->
					<?php endif;

					if ( ! empty( $col['add_quote'] ) ) : ?>
						<div class="app__block-quote" data-aos="fade-<?php echo ( $index == 0 ) ? 'left' : 'right'; ?>">
							<blockquote>
								<?php echo wpautop( esc_html( $col['quote'] ) ); ?>
							</blockquote>
						</div><!-- /.app__block-quote -->
					<?php endif;

					if ( ! empty( $col['text'] ) ) : ?>
						<div class="app__block-entry richtext-entry" data-aos="fade-<?php echo ( $index == 0 ) ? 'left' : 'right'; ?>">
							<?php echo app_content( $col['text'] ); ?>
						</div><!-- /.app__block-entry -->
					<?php endif;

					if ( ! empty( $col['image'] ) ) : ?>
						<div class="app__block-image" data-aos="fade-<?php echo ( $index == 0 ) ? 'left' : 'right'; ?>">
							<?php echo wp_get_attachment_image( $col['image'], 'app_full_width' ); ?>
						</div><!-- /.app__block-image -->
					<?php endif;

					if ( ! empty( $col['add_footer_text'] ) ) : ?>
						<div class="app__block-foot" data-aos="fade-right">
							<h6>
								<?php echo esc_html( $col['footer_title'] ); ?>
							</h6>

							<?php echo wpautop( esc_html( $col['footer_subtitle'] ) ); ?>
						</div><!-- /.app__block-foot -->
					<?php endif ?>
				</div><!-- /.app__block-col -->
			<?php endforeach ?>
		</div><!-- /.app__block-cols -->
	</div><!-- /.shell -->
</section><!-- /.app-block-custom-cols -->
