<?php
$title 				  = get_field( 'title' );
$subtitle 			  = get_field( 'subtitle' );
$back_button 		  = get_field( 'back_button' );
$booking_button 	  = get_field( 'booking_button' );
$image 				  = get_field( 'image' );
$info_text 			  = get_field( 'info_text' );
$text 				  = get_field( 'text' );
$videos 			  = get_field( 'videos' );
$accordions 		  = get_field( 'accordions' );
$members_title 		  = get_field( 'members_title' );
$images_slider  	  = get_field( 'images_slider' );
$images_slider_title  = get_field( 'image_slider_title' );
$booking_shortcode	  = get_field( 'booking_shortcode' );
$booking_title		  = get_field( 'booking_title' );

$members = array();

global $post;
$page_id = $post->ID;

$members_query = get_posts( array(
	'post_type' => 'app_member',
	'posts_per_page' => -1,
)
);

foreach ( $members_query as $member ) {
	$featured_pages = get_field( 'app_page_assignment', $member->ID );

	if ( ! empty( $featured_pages ) ) {
		foreach ( $featured_pages as $featured_page ) {
			if ( $featured_page == $page_id ) {
				array_push( $members, $member->ID );
			}
		}
	}
}
?>
<section class="app-block-single-service">
	<div class="shell">
		<div class="app__block-inner">
			<?php if ( ! empty( $back_button ) && ! is_singular( 'app_landing_page' ) ) : ?>
				<div class="app__block-actions">
					<a href="<?php echo esc_url( $back_button ); ?>" class="btn-back-to js-sticky-el">
						<svg xmlns="http://www.w3.org/2000/svg" width="69.75" height="69.75" viewBox="0 0 69.75 69.75"><g id="Group_154" data-name="Group 154" transform="translate(-287.25 -135.904)"><circle id="Ellipse_7" data-name="Ellipse 7" cx="34.375" cy="34.375" r="34.375" transform="translate(287.75 136.404)" fill="transparent" stroke="#676d71" stroke-miterlimit="10" stroke-width="1"/><path id="Path_267" data-name="Path 267" d="M324.448,179.343l-8.564-8.564,8.564-8.564" fill="transparent" stroke="#676d71" stroke-miterlimit="10" stroke-width="1"/></g></svg>
					</a>
				</div><!-- /.app__block-actions -->
			<?php endif; ?>

			<div class="app__block-body">
				<div class="app__block-head" data-aos="fade-up">
					<?php if ( ! empty( $title ) ) : ?>
						<h1>
							<?php echo $title; ?>
						</h1>
					<?php endif;

					if ( ! empty( $subtitle ) ) : ?>
						<div class="app__block-head-entry">
							<p>
								<?php echo $subtitle; ?>
							</p>
						</div><!-- /.app__block-head-entry -->
					<?php endif ?>
				</div><!-- /.app__block-head -->

				<?php if ( ! empty( $booking_button ) && ! empty( $image ) ) : ?>
				<div class="app__blcok-image image-fit" data-aos="fade-up">
					<?php
					echo app_render_button_span( $booking_button, 'btn-circle btn-circle--dark' );

					echo wp_get_attachment_image( $image, 'app_full_width' );
					?>
				</div><!-- /.app__blcok-image -->
			<?php endif;

			if ( ! empty( $info_text ) ) : ?>
				<div class="app__block-info" data-aos="fade-up">
					<?php echo app_content( $info_text ); ?>
				</div><!-- /.app__block-info -->
			<?php endif;

			if ( ! empty( $text ) ) : ?>
				<div class="app__block-entry richtext-entry" data-aos="fade-up">
					<?php echo app_content( $text ); ?>
				</div><!-- /.app__block-entry -->
			<?php endif;

			if ( ! empty( $videos ) ) : ?>
				<div class="app-block-slider-media slider__video">
					<div class="app-block-inner" data-aos="fade-up">
						<div class="slider-media js-slider-video">
							<div class="slider__clip swiper">
								<div class="slider__slides swiper-wrapper">
									<?php //for ( $i = 0; $i <= 2; $i++ ) : ?>
										<?php foreach ( $videos as $video ) :
											$video_object = Carbon_Video::create( $video['url'] );	?>
											<div class="slider__slide swiper-slide">
												<div class="slider__slide-inner">
													<div class="slider__slide-image">
														<div class="video__wrap">
															<iframe width="560" height="315" src="<?php echo $video_object->get_embed_url(); ?>?enablejsapi=1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
														</div><!-- /.video__wrap -->
													</div><!-- /.slider__slide-image -->

													<div class="slider__slide-content">
														<?php if ( ! empty( $video['video_title'] ) ) : ?>
															<h6>
																<?php echo esc_html( $video['video_title'] ); ?>
															</h6>
														<?php endif;

														if ( ! empty( $video['video_text'] ) ) {
															echo wpautop( esc_html( $video['video_text'] ) );
														}
														?>
													</div><!-- /.slider__slide-content -->
												</div><!-- /.slider__slide-inner -->
											</div><!-- /.slider__slide swiper-slide -->
										<?php endforeach; ?>
									<?php //endfor; ?>
								</div><!-- /.slider__slides swiper-wrapper -->
							</div><!-- /.slider__clip swiper-container -->

							<?php if ( count( $videos ) > 1 ) : ?>
								<div class="swiper-pagination video-pagination"></div>

								<div class="slider__actions">
									<div class="swiper-button-prev video-prev">
										<svg xmlns="http://www.w3.org/2000/svg" width="48.048" height="48.047" viewBox="0 0 48.048 48.047"><g id="Group_243" data-name="Group 243" transform="translate(-287.25 -135.904)"><circle id="Ellipse_7" data-name="Ellipse 7" cx="23.524" cy="23.524" r="23.524" transform="translate(287.75 136.404)" fill="transparent" stroke="#2e2e2d" stroke-miterlimit="10" stroke-width="1"/><path id="Path_267" data-name="Path 267" d="M321.745,173.936l-5.861-5.861,5.861-5.861" transform="translate(-8.881 -8.148)" fill="transparent" stroke="#2e2e2d" stroke-miterlimit="10" stroke-width="1"/></g></svg>
									</div>

									<div class="swiper-button-next video-next">
										<svg xmlns="http://www.w3.org/2000/svg" width="48.048" height="48.047" viewBox="0 0 48.048 48.047"><g id="Group_242" data-name="Group 242" transform="translate(335.298 183.951) rotate(180)"><circle id="Ellipse_7" data-name="Ellipse 7" cx="23.524" cy="23.524" r="23.524" transform="translate(287.75 136.404)" fill="transparent" stroke="#2e2e2d" stroke-miterlimit="10" stroke-width="1"/><path id="Path_267" data-name="Path 267" d="M321.745,173.936l-5.861-5.861,5.861-5.861" transform="translate(-8.881 -8.148)" fill="transparent" stroke="#2e2e2d" stroke-miterlimit="10" stroke-width="1"/></g></svg>
									</div>
								</div><!-- /.slider__actions -->
							<?php endif; ?>
						</div><!-- /.slider-images js-slider-images -->
					</div><!-- /.app__block-inner -->
				</div><!-- /.app-block-slider-images -->
			<?php endif;

			if ( ! empty( $accordions ) ) : ?>
				<div class="app__block-accordion" data-aos="fade-up">
					<div class="accordion js-accordion">
						<?php foreach ( $accordions as $entry ) : ?>
							<div class="accordion__section">
								<div class="accordion__head">
									<div class="accordion__icon">
										<i class="fas fa-plus"></i>
										<i class="fas fa-minus"></i>
									</div><!-- /.accordion__icon -->

									<h6>
										<?php echo esc_html( $entry['title'] ); ?>
									</h6>
								</div><!-- /.accordion__head -->

								<?php if ( ! empty( $entry['text'] ) ) : ?>
									<div class="accordion__body richtext-entry">
										<?php echo app_content( $entry['text'] ); ?>
									</div><!-- /.accordion__body -->
								<?php endif ?>
							</div><!-- /.accordion__section -->
						<?php endforeach; ?>
					</div><!-- /.accordion js-accordion -->
				</div><!-- /.app__block-accordion -->
			<?php endif;

			if ( ! empty( $images_slider ) ) : ?>
				<div class="app-block-slider-media">
					<?php if ( ! empty( $images_slider_title ) ) : ?>
						<h5 data-aos="fade-up">
							<?php echo esc_html( $images_slider_title ); ?>
						</h5>
					<?php endif; ?>

					<div class="app-block-inner" data-aos="fade-up">
						<div class="slider-media js-slider-media">
							<div class="slider__clip swiper">
								<div class="slider__slides swiper-wrapper">
									<?php foreach ( $images_slider as $image ) : ?>
										<div class="slider__slide swiper-slide">
											<div class="slider__slide-inner">
												<div class="slider__slide-image">
													<?php echo wp_get_attachment_image( $image['image'], 'app_full_width' ); ?>
												</div><!-- /.slider__slide-image -->

												<div class="slider__slide-content">
													<?php if ( ! empty( $image['image_title'] ) ) : ?>
														<h6>
															<?php echo esc_html( $image['image_title'] ); ?>
														</h6>
													<?php endif;

													if ( ! empty( $image['image_text'] ) ) {
														echo wpautop( esc_html( $image['image_text'] ) );
													}
													?>
												</div><!-- /.slider__slide-content -->
											</div><!-- /.slider__slide-inner -->
										</div><!-- /.slider__slide swiper-slide -->
									<?php endforeach; ?>
								</div><!-- /.slider__slides swiper-wrapper -->
							</div><!-- /.slider__clip swiper-container -->

							<?php if ( count( $images_slider ) > 1 ) : ?>
								<div class="swiper-pagination media-pagination"></div>

								<div class="slider__actions">
									<div class="swiper-button-prev media-prev">
										<svg xmlns="http://www.w3.org/2000/svg" width="48.048" height="48.047" viewBox="0 0 48.048 48.047"><g id="Group_243" data-name="Group 243" transform="translate(-287.25 -135.904)"><circle id="Ellipse_7" data-name="Ellipse 7" cx="23.524" cy="23.524" r="23.524" transform="translate(287.75 136.404)" fill="transparent" stroke="#2e2e2d" stroke-miterlimit="10" stroke-width="1"/><path id="Path_267" data-name="Path 267" d="M321.745,173.936l-5.861-5.861,5.861-5.861" transform="translate(-8.881 -8.148)" fill="transparent" stroke="#2e2e2d" stroke-miterlimit="10" stroke-width="1"/></g></svg>
									</div>

									<div class="swiper-button-next media-next">
										<svg xmlns="http://www.w3.org/2000/svg" width="48.048" height="48.047" viewBox="0 0 48.048 48.047"><g id="Group_242" data-name="Group 242" transform="translate(335.298 183.951) rotate(180)"><circle id="Ellipse_7" data-name="Ellipse 7" cx="23.524" cy="23.524" r="23.524" transform="translate(287.75 136.404)" fill="transparent" stroke="#2e2e2d" stroke-miterlimit="10" stroke-width="1"/><path id="Path_267" data-name="Path 267" d="M321.745,173.936l-5.861-5.861,5.861-5.861" transform="translate(-8.881 -8.148)" fill="transparent" stroke="#2e2e2d" stroke-miterlimit="10" stroke-width="1"/></g></svg>
									</div>
								</div><!-- /.slider__actions -->
							<?php endif ?>
						</div><!-- /.slider-images js-slider-images -->
					</div><!-- /.app__block-inner -->
				</div><!-- /.app-block-slider-images -->
			<?php endif;

			if ( ! empty( $booking_shortcode ) && ! empty( $booking_title )) : ?>

				<div class="app__block-specialists" data-aos="fade-up">
					<h5><?php echo $booking_title; ?></h5>
					<?php echo $booking_shortcode; ?>
				</div>

			<?php endif;

			if ( ! empty( $members ) ) : ?>
				<div class="app__block-specialists" data-aos="fade-up">
					<?php if ( ! empty( $members_title ) ) : ?>
						<h5>
							<?php echo esc_html( $members_title ); ?>
						</h5>
					<?php endif; ?>

					<div class="slider-specialists js-slider-specialists">
						<span class="slider__specialists-line"></span>
						<div class="slider__actions">
							<div class="swiper-button-prev">
								<svg xmlns="http://www.w3.org/2000/svg" width="48.048" height="48.047" viewBox="0 0 48.048 48.047"><g id="Group_243" data-name="Group 243" transform="translate(-287.25 -135.904)"><circle id="Ellipse_7" data-name="Ellipse 7" cx="23.524" cy="23.524" r="23.524" transform="translate(287.75 136.404)" fill="transparent" stroke="#2e2e2d" stroke-miterlimit="10" stroke-width="1"/><path id="Path_267" data-name="Path 267" d="M321.745,173.936l-5.861-5.861,5.861-5.861" transform="translate(-8.881 -8.148)" fill="transparent" stroke="#2e2e2d" stroke-miterlimit="10" stroke-width="1"/></g></svg>
							</div>

							<div class="swiper-button-next">
								<svg xmlns="http://www.w3.org/2000/svg" width="48.048" height="48.047" viewBox="0 0 48.048 48.047"><g id="Group_242" data-name="Group 242" transform="translate(335.298 183.951) rotate(180)"><circle id="Ellipse_7" data-name="Ellipse 7" cx="23.524" cy="23.524" r="23.524" transform="translate(287.75 136.404)" fill="transparent" stroke="#2e2e2d" stroke-miterlimit="10" stroke-width="1"/><path id="Path_267" data-name="Path 267" d="M321.745,173.936l-5.861-5.861,5.861-5.861" transform="translate(-8.881 -8.148)" fill="transparent" stroke="#2e2e2d" stroke-miterlimit="10" stroke-width="1"/></g></svg>
							</div>
						</div><!-- /.slider__actions -->

						<div class="slider__clip swiper">
							<div class="slider__slides swiper-wrapper">
								<?php foreach ( $members as $member ) :
									$position = get_field( 'app_member_position', $member );
									?>
									<div class="slider__slide swiper-slide">
										<div class="specialist" data-aos="fade-up">
											<div class="specialist__inner">
												<a href="<?php the_permalink( $member ); ?>" class="specialist__link"></a>
												<div class="specialist__image js-equalize-height">
													<?php echo get_the_post_thumbnail( $member ); ?>
												</div><!-- /.specialist__image -->

												<div class="specialist__content">
													<div class="specialist__name">
														<p>
															<?php echo get_the_title( $member ); ?>
														</p>
													</div><!-- /.specialist__name -->

													<?php if ( ! empty( $position ) ) : ?>
														<div class="specialist__pos">
															<p>
																<?php echo esc_html( $position ); ?>
															</p>
														</div><!-- /.specialist__pos -->
													<?php endif ?>
												</div><!-- /.specialist__content -->
											</div><!-- /.specialist__inner -->
										</div><!-- /.specialist -->
									</div><!-- /.slider__slide swiper-slide -->
								<?php endforeach ?>
							</div><!-- /.slider__slides swiper-wrapper -->
						</div><!-- /.slider__clip swiper-container -->
					</div><!-- /.slider-specialists js-slider-specialists -->
				</div><!-- /.app__block-specialists -->
			<?php endif ?>
		</div><!-- /.app__block-body -->
	</div><!-- /.app__block-inner -->
</div><!-- /.shell -->
</section><!-- /.app-block-single-service -->
