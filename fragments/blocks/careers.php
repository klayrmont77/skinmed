<?php
$hero_title    = get_field( 'hero_title' );
$hero_subtitle = get_field( 'hero_subtitle' );
$hero_text 	   = get_field( 'hero_text' );
$image 		   = get_field( 'image' );

$args = array(
	'post_type' 	 => 'job_listing',
	'posts_per_page' => -1,
	'orderby' 		 => 'date',
	'post_status'	=> [
			'publish'
	],
	'meta_query'	=> array( array(
			'key'     => '_filled',
			'value'   => true,
			'compare' => '!=',
	)),
);

$careers_query = new WP_Query( $args );

?>
<section class="app-block-careers">
	<div class="shell">
		<div class="app__block-inner">
			<div class="app__block-head" data-aos="fade-up">
				<?php if ( ! empty( $hero_title ) ) : ?>
					<h5>
						<?php echo esc_html( $hero_title ); ?>
					</h5>
				<?php endif;

				if ( ! empty( $hero_subtitle ) ) : ?>
					<h2>
						<?php echo esc_html( $hero_subtitle ); ?>
					</h2>
				<?php endif;

				echo wpautop( esc_html( $hero_text ) );
				?>
			</div><!-- /.app__block-head -->

			<?php if ( ! empty( $image ) ) : ?>
				<div class="app__block-image" data-aos="fade-left">
					<div class="app__block-image-inner">
						<?php echo wp_get_attachment_image( $image, 'app_full_width' ); ?>
					</div><!-- /.app__block-image-inner -->
				</div><!-- /.app__block-image -->
			<?php endif; ?>

			<div class="app__block-list">
				<div class="careers">
					<?php while ( $careers_query->have_posts() ) : $careers_query->the_post();
						$locations = get_field( 'app_career_locations', get_the_ID() );
						?>
						<div class="career" data-aos="fade-up">
							<?php if ( ! empty( $locations ) ) : ?>
								<div class="career__cities">
									<ul>
										<?php foreach ( $locations as $location ) : ?>
											<li>
												<?php echo esc_html( $location['location'] ); ?>
											</li>
										<?php endforeach; ?>
									</ul>
								</div><!-- /.career__cities -->
							<?php endif; ?>

							<h3 class="career__title">
								<a href="<?php the_permalink(); ?>">
									<?php the_title(); ?>
								</a>
							</h3><!-- /.career__title -->

							<div class="career__actions">
								<a href="<?php the_permalink(); ?>" class="btn btn--dark-border">
									<?php _e( 'Mehr erfahren', 'app' ); ?>
								</a>
							</div><!-- /.career__actions -->
						</div><!-- /.career -->
					<?php endwhile;
					wp_reset_postdata();
					?>
				</div><!-- /.careers -->
			</div><!-- /.app__block-list -->
		</div><!-- /.app__block-inner -->
	</div><!-- /.shell -->
</section><!-- /.app-block-careers -->
