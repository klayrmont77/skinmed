<?php
$section_title = get_field( 'section_title' );
$slides 	   = get_field( 'slides' );
$logos_title   = get_field( 'logos_title' );
$logos 		   = get_field( 'logos' );
$button_text   = get_field( 'button_text' );
$button_link   = get_field( 'button_link' );

if ( empty( $slides ) ) {
	return;
}
?>
<section class="app-block-slider-boxes js-fullheight">
	<div class="app__block-container">
		<div class="app__block-inner">
			<?php if ( ! empty( $section_title ) ) : ?>
				<div class="app__block-head">
					<?php echo app_content( $section_title ); ?>
				</div><!-- /.app__block-head -->
			<?php endif ?>

			<div class="app__block-body">
				<div class="slider-boxes js-slider-boxes">
					<div class="slider__clip">
						<div class="slider__slides">
							<?php foreach ( $slides as $slide ) : ?>
								<div class="slider__slide">
									<div class="slider__slide-inner">
										<div class="slider__slide-box">
											<?php if ( $slide['type'] == 'video' ) :
												if ( empty( $slide['video_url'] ) ) {
													continue;
												}
												?>
												<div class="slider__slide-image image-fit">
													<span data-href="<?php echo esc_url( $slide['video_url'] ); ?>" class="js-popup-iframe-in-slider"></span>
													<?php echo wp_get_attachment_image( $slide['video_thumbnail'], 'app_full_width' ); ?>
												</div><!-- /.slider__slide-image -->
											<?php else: ?>
												<div class="slider__slide-quote">
													<blockquote>
														<?php echo wpautop( esc_html( $slide['quote'] ) ); ?>
													</blockquote>
												</div><!-- /.slider__slide-quote -->
											<?php endif ?>
										</div><!-- /.slider__slide-box -->

										<div class="slider__slide-text">
											<?php echo app_content( $slide['bottom_text'] ); ?>
										</div><!-- /.slider__slide-text -->
									</div><!-- /.slider__slide-inner -->
								</div><!-- /.slider__slide -->
							<?php endforeach; ?>
						</div><!-- /.slider__slides -->

						<div class="slider__actions">
							<a href="#" class="btn-next">
								<svg xmlns="http://www.w3.org/2000/svg" width="48.047" height="48.047" viewBox="0 0 48.047 48.047"><g id="Group_174" data-name="Group 174" transform="translate(335.297 183.951) rotate(180)"><circle id="Ellipse_7" data-name="Ellipse 7" cx="23.524" cy="23.524" r="23.524" transform="translate(287.75 136.404)" fill="transparent" stroke="#fff" stroke-miterlimit="10" stroke-width="1"></circle><path id="Path_267" data-name="Path 267" d="M321.745,173.936l-5.861-5.861,5.861-5.861" transform="translate(-8.881 -8.148)" fill="transparent" stroke="#fff" stroke-miterlimit="10" stroke-width="1"></path></g></svg>
							</a>
						</div><!-- /.slider__actions -->


					</div><!-- /.slider__clip -->
				</div><!-- /.slider js-slider -->
			</div><!-- /.app__block-body -->

			<?php if ( ! empty( $button_text ) && ! empty( $button_link ) ) : ?>
				<div class="app__block-button">
					<a href="<?php echo $button_link; ?>"
					   class="btn"
					   target="_blank">
						<?php echo $button_text; ?>
					</a>
				</div>
			<?php endif ?>

			<?php if ( ! empty( $logos ) ) : ?>
				<div class="app__block-logos">
					<?php if ( ! empty( $logos_title ) ) : ?>
						<h5>
							<?php echo esc_html( $logos_title ); ?>
						</h5>
					<?php endif ?>

					<ul>
						<?php foreach ( $logos as $logo ) : ?>
							<li>
								<a href="<?php echo esc_url( $logo['url'] ); ?>">
									<?php
									echo wp_get_attachment_image( $logo['logo'], 'app_image_slide' );

									echo wp_get_attachment_image( $logo['logo_mobile'], 'app_image_slide' );
									?>
								</a>
							</li>
						<?php endforeach ?>
					</ul>
				</div><!-- /.app__block-logos -->
			<?php endif ?>
		</div><!-- /.app__block-inner -->
	</div><!-- /.app__block-container -->
</section><!-- /.app-block-slider-boxes js-fullheight -->
