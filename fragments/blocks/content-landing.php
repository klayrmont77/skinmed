<?php
$heading_text 	 = get_field( 'heading_text' );
$text 			 = get_field( 'text' );
$button 		 = get_field( 'button' );
$video_thumbnail = get_field( 'video_thumbnail' );
$video_url 		 = get_field( 'video_url' );
?>
<section class="section-content" id="fotofinder">
	<div class="shell">
		<div class="section__body">
			<?php
			echo app_content( $heading_text );

			if ( ! empty( $video_thumbnail ) && ! empty( $video_url ) ) : ?>
				<div class="block-video alignleft">
					<a href="<?php echo esc_url( $video_url ); ?>" class="js-popup-iframe">
						<i class="ico-play-video"></i>
					</a>

					<div class="block__image image-fit">
						<?php echo wp_get_attachment_image( $video_thumbnail, 'app_full_width' ); ?>
					</div><!-- /.block__image -->
				</div><!-- /.block-video -->
			<?php endif ?>

			<?php echo app_content( $text ); ?>
		</div><!-- /.section__body -->

		<?php if ( ! empty( $button ) ) : ?>
				<i class="ico-circle-text ico-circle-text--right">
					<a href="<?php echo esc_url( $button['url'] ); ?>" target="<?php echo esc_attr( $button['target'] ); ?>"></a>

					<span>
						<?php echo esc_html( $button['title'] ); ?>
					</span>
				</i>
		<?php endif ?>
	</div><!-- /.shell -->
</section><!-- /.section-content -->
