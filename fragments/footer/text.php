<?php

if ( is_singular( 'app_landing_page' ) ) {
	$text = get_field( 'app_footer_landing_text', 'option' );
	$online_button = get_field( 'app_footer_landing_booking_button', 'option' );
} else {
	$text = get_field( 'app_footer_text', 'option' );
	$online_button = get_field( 'app_header_booking_button', 'option' );
}


if ( ! empty( $text ) ) : ?>
	<div class="footer__text">
		<div class="footer__text-entry">
			<?php echo apply_filters( 'the_content', $text ); ?>
		</div><!-- /.footer__text-entry -->

		<?php if ( ! empty( $online_button ) ) : ?>
			<div class="footer__text-actions">
				<?php echo app_render_button_span( $online_button, 'btn-circle' ); ?>
			</div><!-- /.footer__text-actions -->
		<?php endif ?>
	</div><!-- /.footer__text -->
<?php endif; ?>

<div class="footer__logo">
	<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
		<img src="<?php bloginfo('template_directory'); ?>/resources/images/logo-white.svg" alt="Skinmed">
	</a>
</div><!-- /.footer__logo -->
