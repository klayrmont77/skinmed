<?php
if ( is_singular( 'app_landing_page' ) ) {
	$locations = get_field( 'app_footer_landing_locations', 'option' );
} else {
	$locations = get_field( 'app_footer_locations', 'option' );
}

if ( empty( $locations ) ) {
	return;
}
?>
<div class="footer__addresses">
	<ul>
		<?php foreach ( $locations as $location ) : ?>
			<li>
				<strong>
					<?php echo esc_html( $location['title'] ); ?>
				</strong> <br>
				<?php echo nl2br( $location['location'] ); ?>
			</li>
		<?php endforeach ?>
	</ul>
</div><!-- /.footer__addresses -->
