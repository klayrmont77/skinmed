<?php

if ( is_singular( 'app_landing_page' ) ) {
	$phone 	 	   = get_field( 'app_footer_landing_phone', 'option' );
	$socials 	   = get_field( 'app_footer_landing_socials', 'option' );
	$socials_title = get_field( 'app_footer_socials_title', 'option' );
	$form_title    = get_field( 'app_footer_form_title', 'option' );
	$form    	   = get_field( 'app_footer_form', 'option' );
	$button_url    = get_field( 'app_footer_carrer_button_url', 'option' );
} else {
	$phone 	 	   = get_field( 'app_header_phone', 'option' );
	$socials 	   = get_field( 'app_header_socials', 'option' );
	$socials_title = get_field( 'app_footer_socials_title', 'option' );
	$form_title    = get_field( 'app_footer_form_title', 'option' );
	$form    	   = get_field( 'app_footer_form', 'option' );
	$button_url    = get_field( 'app_footer_carrer_button_url', 'option' );
}

if ( ! empty( $phone ) ) : ?>
	<div class="footer__phone">
		<a href="tel:<?php echo $phone; ?>">
			<img src="<?php bloginfo('template_directory'); ?>/resources/images/ico-phone-white.svg" alt="">

			<?php echo esc_html( $phone ); ?>
		</a>
	</div><!-- /.footer__phone -->
<?php endif ?>

<div class="footer__cta">
	<?php if ( ! empty( $socials ) ) : ?>
		<div class="socials">
			<?php if ( ! empty( $socials_title ) ) : ?>
				<h5>
					<?php echo esc_html( $socials_title ); ?>
				</h5>
			<?php endif ?>

			<ul>
				<?php foreach ( $socials as $social ) : ?>
					<li>
						<a href="<?php echo esc_url( $social['url'] ); ?>" target="_blank">
							<i class="<?php echo esc_attr( $social['icon'] ); ?>"></i>
						</a>
					</li>
				<?php endforeach ?>
			</ul>
		</div><!-- /.socials -->
	<?php endif;

	if ( ! empty( $form ) ) : ?>
		<div class="footer__form">
			<?php if ( ! empty( $form_title ) ) : ?>
				<p>
					<?php echo esc_html( $form_title ); ?>
				</p>
			<?php endif;

			app_render_gform( $form, true ); ?>
		</div><!-- /.footer__form -->
	<?php endif;

	if ( ! empty( $button_url ) ) : ?>
		<div class="footer__btn">
			<a href="<?php echo esc_url( $button_url ); ?>">
				<?php _e( 'Career', 'crb' ); ?>
			</a>
		</div><!-- /.footer__btn -->
	<?php endif ?>
</div><!-- /.footer__cta -->
