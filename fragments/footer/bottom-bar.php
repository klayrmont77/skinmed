<?php
if ( is_singular( 'app_landing_page' ) ) {
	$links 	   = get_field( 'app_footer_landing_links', 'option' );
	$copyright = get_field( 'app_footer_landing_copyright', 'option' );
} else {
	$links 	   = get_field( 'app_header_links', 'option' );
	$copyright = get_field( 'app_header_copyright', 'option' );
}
?>
<div class="footer__bar">

	<ul>
		<?php if ( ! empty( $copyright ) ) : ?>
			<li class="copyright">
				<?php echo apply_filters( 'the_content', $copyright ); ?>
			</li><!-- /.copyright -->
		<?php endif;
		?>
		<?php foreach ( $links as $link ) : ?>
			<li>
				<a href="<?php echo esc_url( $link['url'] ); ?>">
					<?php echo esc_html( $link['label'] ); ?>
				</a>
			</li>
		<?php endforeach ?>
	</ul>

	<a class="labor" href="https://www.team-w.ch/de" target="_blank">
		<span>Partner:</span>
		<img src="<?php echo get_stylesheet_directory_uri();?>/dist/images/labor-team-logo.png"
			 alt=""
		/>
	</a>
</div><!-- /.footer__bar -->
