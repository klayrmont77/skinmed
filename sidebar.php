<div class="sidebar">
	<ul class="widgets">
		<?php
		$page_ID = app_get_page_context();
		$sidebar = '';

		# If $page_ID is present, check for custom sidebar
		if ( ! empty( $page_ID ) ) {
			$sidebar = get_field( 'app_custom_sidebar', $page_ID );
		}

		# If sidebar is not set or the $page_ID is not present, assign 'default-sidebar'
		if ( empty( $sidebar ) ) {
			$sidebar = 'default-sidebar';
		}

		dynamic_sidebar( $sidebar );
		?>
	</ul><!-- /.widgets -->
</div><!-- /.sidebar -->
