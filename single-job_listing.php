<?php
$new_dropdown = get_field( 'app_header_use_new_dropdown_styles', 'option' );
$header_style = get_field( 'app_career_header_style' );

if ( empty( $header_style ) ) {
	$header_style = 'dermatology';
}

if ( ! $new_dropdown ) {
	get_header( '', array(
		'style' => $header_style,
	) );
} else {
	get_header( 'nav-dropdown', array(
		'style' => $header_style,
	) );
}


$job_post_id		= get_the_ID();

$locations				= get_field( 'app_career_locations' );
$hero_title				= get_field( 'app_job_detail_title', 'option' );
$hero_text				= get_field( 'app_job_detail_text', 'option' );
$button_text			= get_field( 'app_bewerben_button_text', 'option' );
$hero_subtitle			= get_field( 'app_job_subtitle', 'option' );
$hero_back_button_url	= get_field( 'app_career_back_button_link', 'option' );

$filled = get_post_meta( $job_post_id, '_filled', true );

if (get_post_status(get_the_ID()) == 'expired' || $filled) {
	wp_redirect($hero_back_button_url);
	die;
}

?>

<section class="app-block-single-career">
	<div class="shell">

		<div class="section__title" data-aos="fade-up">
			<?php if ( ! empty( $hero_title ) ) : ?>
				<h6>
					<?php echo $hero_title; ?>
				</h6>
			<?php endif;

			if ( ! empty( $hero_subtitle ) ) : ?>
				<h2>
					<?php echo $hero_subtitle; ?>
				</h2>
			<?php endif;

			if ( ! empty( $hero_text ) ) {
				echo wpautop( $hero_text );
			}
			?>

			<a href="<?php echo esc_url( $hero_back_button_url ); ?>" class="btn--back"></a>
		</div><!-- /.section__title -->

		<div class="section__body" data-aos="fade-up">
			<?php if ( ! empty( $locations ) ) : ?>
				<p class="tags">

					<?php app_display_locations( $locations ); ?>

				</p><!-- /.tags -->
			<?php endif; ?>

			<h3>
				<?php
				if ( ! empty( $career_title_overwrite ) ) {
					echo nl2br( esc_html( $career_title_overwrite ) );
				} else {
					the_title();
				}
				?>
			</h3>

			<?php

			the_content();

			?>
			<a href="#" class="btn btn-blue js-open-form">
				<?php if ( ! empty( $apply_button_text ) ) {
					echo esc_html( $apply_button_text );
				} else {
					echo $button_text;
				} ?>
			</a>
		</div><!-- /.section__body -->

		<div class="job_application application">
			<div class="application_details" style="">
				<?php echo do_shortcode( "[job_apply id={$job_post_id}]" ); ?>
			</div>
		</div>
	</div><!-- /.shell -->
</section><!-- /.single-career -->

<?php get_footer(); ?>
