/*
 * Polyfills
 */
import './polyfills/object-fit';

/*
 * Modules
 */


import './modules/navigation';
import './modules/custom-scroll';
import './modules/categories';
import './modules/slider-fullheight';
import './modules/slider-members';
import './modules/acc';
import './modules/main-margin';
import './modules/slider-timeline';
import './modules/equalize-height';
import './modules/sticky-el';
import './modules/slider-members-related';
import './modules/map';
import './modules/isotope';
import './modules/popup-iframe';
import './modules/slider-specialists';
import './modules/search-overlay';
import './modules/slider-images';
import './modules/nav-categories';
import './modules/fullheight';
import './modules/aos-animations';
import './modules/remove-p';
import './modules/load-event';
import './modules/open-form';
import './modules/blog-filter';
import './modules/slider-media';
import './modules/slider-video';
import './modules/slider-boxes';
import './modules/slider-logos';
import './modules/slider-articles';
import './modules/slider-before-after';
