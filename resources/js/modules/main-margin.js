import { $win } from '../utils/globals';

$win.on('resize load orientationchange', function() {
	const headerHeight = $('.header').outerHeight();
	const $main = $('.main:not(.main--fullpage)');
	$main.css('margin-top', headerHeight);
	$main.find( '> .section-intro:first-child' ).css( 'margin-top', - headerHeight);
});
