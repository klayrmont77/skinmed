$('.nav__categories li a').on( 'click', function (event) {
	event.preventDefault();

	const sortQuery = $(this).data('category');

	$(this).parent().addClass('current');
	$(this).parent().siblings().removeClass('current');

	$.ajax({
		url: window.location.href,
		method: 'GET',
		data: {
			app_sort_query: sortQuery,
			paged: 1,
		},
		beforeSend: function () {
			$('.js-spinner').show();
			$('.articles').hide();
		},
		success: function (response) {
			const newPosts = $(response).find('.articles');
			const newPagination = $(response).find('.paging');

			if ( newPosts.length ) {
				$('.articles').parent().html(newPosts);
			}

			$('.paging').remove();
			
			if ( newPagination.length ) {
				$('.articles').parent().after(newPagination);
			}
		},
		complete: function () {
			$('.js-spinner').hide();
			$('.articles').show();
		},
		error: function (error) {
			console.log(error);
		}
	})
})

$(document).on( 'click', '.paging li a', function (event) {
	event.preventDefault();

	const url  = $(this).attr('href');

	$.ajax({
		url: url,
		method: 'POST',
		beforeSend: function () {
			$('.js-spinner').show();
			$('.articles').children().hide();
		},
		success: function (response) {
			const newPosts = $(response).find('.articles');
			const newPagination = $(response).find('.paging');

			if ( newPosts.length ) {
				$('.articles').parent().html(newPosts);
			}

			$('.paging').remove();
			
			if ( newPagination.length ) {
				$('.articles').parent().after(newPagination);
			}
		},
		complete: function () {
			$('.js-spinner').hide();
			$('.articles').children().show();
		},
		error: function (error) {
			console.log(error);
		}
	})
})
