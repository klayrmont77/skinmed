import { $win } from '../utils/globals';
import 'jquery-mousewheel';
import 'malihu-custom-scrollbar-plugin';

const $btnMenu = $('.btn-menu');
const $btnCloseMenu = $('.section-navigation .btn-close-menu');

$btnMenu.on('click', function (event) {
	event.preventDefault();
	$('body').addClass('has-navigation');
});

$btnCloseMenu.on('click', function (event) {
	event.preventDefault();
	$('body').removeClass('has-navigation');
});

function mobileNavigation () {
	$('.menu-item-has-children > a').on('click', function(e) {
		if ( !$(this).parent().hasClass('js-show') ) {
			e.preventDefault();

			$(this)
				.next()
				.slideDown()
				.parent()
				.addClass('js-show')
				.siblings()
				.removeClass('js-show')
				.find('.sub-menu')
				.slideUp()
				.find('.js-show')
				.removeClass('js-show');
		}
	});
}

if ($win.width() < 767) {
	mobileNavigation();
}

// main header navivation links
$('.nav-utilities #menu-top-bar-menu .open_sub_menu a').on('click', function(e) {
	e.preventDefault();

	let $menuText = $(this).attr('href');

	$('.nav-tabs a').each(function() {
		let $menuItem = $(this);
		let $menuItemHref = $menuItem.attr('href');
		if ($menuItemHref === $menuText) {
			$menuItem.trigger('click');
		}
	});
});

// back button on subpages
$('a.btn-back-to').on('click', function(e) {
	let $menuText = $(this).attr('href');

	// bail if normal link
	if ($menuText.indexOf('#') === -1) return;

	e.preventDefault();

	// trigger main menu
	if ($menuText === '#main') {
		$('.btn-menu').trigger('click');
		return;
	}

	// trigger sub menus
	$('.nav-tabs a').each(function() {
		let $menuItem = $(this);
		let $menuItemHref = $menuItem.attr('href');
		if ($menuItemHref === $menuText) {
			$menuItem.trigger('click');
		}
	});
});
