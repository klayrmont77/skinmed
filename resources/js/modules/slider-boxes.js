import 'slick-carousel';

import { $win } from '../utils/globals';

$('.js-slider-boxes .slider__slides').slick({
	infinite: true,
	slidesToShow: 3,
	slidesToScroll: 1,
	nextArrow: $('.js-slider-boxes .btn-next'),
	autoHeight: true,
	responsive: [
		{
		breakpoint: 1279,
			settings: {
				slidesToShow: 2,
			}
		},
		{
		breakpoint: 767,
			settings: {
				slidesToShow: 1,
			}
		}
	]
});

$win.on('load resize', function () {
	const boxH = $('.js-slider-boxes .slider__slide-box').outerHeight() / 2;
	const imageH = $('.slider-boxes .slider__slide-image').outerHeight();
	const $sliderActions = $('.js-slider-boxes .slider__actions');
	const $sliderQuote = $('.js-slider-boxes .slider__slide-quote');

	$sliderActions.css('top', boxH)
	$sliderQuote.css('min-height', imageH)
});
