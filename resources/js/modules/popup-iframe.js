import 'magnific-popup';
import { $document } from '../utils/globals';

$('.js-popup-iframe').magnificPopup({
	type: 'iframe',
	iframe: {
			markup: '<div class="mfp-iframe-scaler">' +
				'<div class="mfp-close"></div>' +
				'<iframe class="mfp-iframe" frameborder="0" allow="autoplay"></iframe>' +
				'</div>',
			patterns: {
				youtube: {
					index: 'youtube.com/',
					id: 'v=',
					src: 'https://www.youtube.com/embed/%id%?autoplay=1'
				},
				vimeo: {
					index: 'vimeo.com/',
					id: function(url) {
						var m = url.match(/(https?:\/\/)?(www.)?(player.)?vimeo.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/);
						if ( !m || !m[5] ) return null;
						return m[5];
					},
					src: 'https://player.vimeo.com/video/%id%?autoplay=1'
				}
			}
		}
});

$document.on('click', '.js-popup-iframe-in-slider' , function () {
	const src = $(this).data('href');
	$.magnificPopup.open({
		items: {
			src,
		},
		type: 'iframe'
	})
})
