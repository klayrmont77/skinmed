import imagesLoaded from 'imagesloaded';
var jQueryBridget = require('jquery-bridget');
var Isotope = require('isotope-layout');

jQueryBridget( 'isotope', Isotope, $ );

// init Isotope
var $grid = $('.js-articles-isotope').isotope({
	itemSelector: '.js-article-isotope',
	percentPosition: true,
	masonry: {
		columnWidth: '.js-article-isotope'
	}
});
// layout Isotope after each image loads
$grid.imagesLoaded().progress( function() {
	$grid.isotope('layout');
});
