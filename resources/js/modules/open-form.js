$('.js-open-form').on('click', function(e) {
	e.preventDefault();

	let $targetTab = $(this).parents('.app-block-single-career').find('.job_application');

	if ( $targetTab.is(':hidden')) {
		$targetTab.show(400);

		$('.app-block-single-career .application_details .input-text, .app-block-single-career .application_details textarea').attr('value','');
	} else {
		$targetTab.hide(400);
	}
});
