import Swiper from 'swiper/swiper-bundle';

import { $win } from '../utils/globals';

$('.js-slider-images .swiper').each((i, slider) => {
	const $slider = $(slider).closest('.js-slider-images');

	if ($slider.find('.slider__slide').length > 1) {
		$slider.addClass('is-visible');

		const swiper = new Swiper(slider, {
			slidesPerView: 1,
			loop: true,
			speed: 400,
			effect: "fade",
			fadeEffect: {
				crossFade: true
			},
			autoHeight: true,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
		});
	}
});
