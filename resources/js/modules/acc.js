const $accordion = $('.js-accordion')

$accordion.on('click', '.accordion__head', function (event) {
	const $accordionSection = $(this).closest('.accordion__section');
	const $accordionBody = $accordionSection.find('.accordion__body');

	$accordionBody.stop().slideToggle();

	$accordionSection.toggleClass('is-current');
});
