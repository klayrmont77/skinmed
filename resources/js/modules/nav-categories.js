$('.nav-categories .menu-item-has-children > a').on('click', function(e) {
	if ( !$(this).parent().hasClass('js-show') ) {
		e.preventDefault();

		$(this)
			.next()
			.slideDown()
			.parent()
			.addClass('js-show')
			.siblings()
			.removeClass('js-show')
			.find('.sub-menu')
			.slideUp()
			.find('.js-show')
			.removeClass('js-show');
	}
});
