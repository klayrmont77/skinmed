import { $win } from '../utils/globals';

//Equalize height
$.fn.equalizeHeight = function() {
	var maxHeight = 0, itemHeight;

	for (var i = 0; i < this.length; i++) {
		itemHeight = $(this[i]).height();
		if (maxHeight < itemHeight) {
			maxHeight = itemHeight;
		}
	}

	return this.height(maxHeight);
}

$win.on('load', function() {
	$('.js-equalize-height').equalizeHeight();
});

$win.on('resize', function() {
	if ( $win.width() > 575 ) {
		$('.js-equalize-height').removeAttr('style').equalizeHeight();
	} else {
		$('.js-equalize-height:not(.slider__slide-box)').removeAttr('style');
		$('.js-equalize-height.slider__slide-box').removeAttr('style').equalizeHeight();
	}
});
