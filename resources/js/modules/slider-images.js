import Swiper from 'swiper/swiper-bundle';
import { $win } from '../utils/globals';

$('.js-slider-media .swiper').each((i, slider) => {
	const $slider = $(slider).closest('.js-slider-media');

	if ($slider.find('.slider__slide').length > 1) {
		$slider.addClass('is-visible');

		const swiper = new Swiper(slider, {
			slidesPerView: 1,
			loop: true,
			speed: 400,
			effect: "fade",
			fadeEffect: {
				crossFade: true
			},
			autoHeight: true,
			navigation: {
				nextEl: '.media-next',
				prevEl: '.media-prev',
			},
			pagination: {
				el: '.media-pagination',
				clickable: true,
			},
		});
	}
});

$('.js-slider-video .swiper').each((i, slider) => {
	const $slider = $(slider).closest('.js-slider-video');

	if ($slider.find('.slider__slide').length > 1) {
		$slider.addClass('is-visible');

		const swiper = new Swiper(slider, {
			slidesPerView: 1,
			loop: true,
			speed: 400,
			effect: "fade",
			fadeEffect: {
				crossFade: true
			},
			autoHeight: true,
			navigation: {
				nextEl: '.video-next',
				prevEl: '.video-prev',
			},
			pagination: {
				el: '.video-pagination',
				clickable: true,
			},
			on: {
				slideChange: function (el) {
					$('.swiper-slide').each(function () {
						var youtubePlayer = $(this).find('iframe').get(0);
						if (youtubePlayer) {
							youtubePlayer.contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
						}
					});
				},
			},
		});
	}
});
