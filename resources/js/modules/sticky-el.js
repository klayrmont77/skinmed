import { $win } from '../utils/globals';
import 'lodash';

if ($('.js-sticky-el').length > 0) {
	const $sticky = $('.js-sticky-el');
	let stickyOffset = $sticky.offset().top;


	$win.on('load scroll', _.throttle( () => {
		const top = $win.scrollTop();
		$sticky.toggleClass('is-active', top > stickyOffset)
	}, 50 ));
}
