import Swiper from 'swiper/swiper-bundle';
import { $win } from '../utils/globals';

$('.js-slider-members .swiper').each((i, slider) => {
	const $slider = $(slider).closest('.js-slider-members');

	if ($slider.find('.slider__slide').length > 1) {
		$slider.addClass('is-visible');

		const swiper = new Swiper(slider, {
			loop: true,
			speed: 500,
			slidesPerView: 3,
			slidesPerGroup: 1,
			watchSlidesProgress: true,
			allowTouchMove: true,
			simulateTouch: false,
			on: {
				init: function () {
					$('.js-slider-members .swiper-slide-visible').each( function (index) {
						$(this).find('.slider__slide-member-image').addClass('is-visible')
					})
				},
			},
			breakpoints: {
				500: {
					slidesPerView: 3,
					slidesPerGroup: 1,
				},
				1023: {
					slidesPerView: 5,
					slidesPerGroup: 1,
				},
				1279: {
					slidesPerView: 6,
					slidesPerGroup: 1,
				},
			},
		});

		const swiperDelay = 1;

		setInterval(() => {
			$('.js-slider-members .swiper-button-next.enabled').trigger('click');
		}, 4000)

		$('.js-slider-members .swiper-button-prev.enabled').on('click', function()	{

			$('.js-slider-members .swiper-button-prev').removeClass('enabled');

			$('.js-slider-members .swiper-slide-visible').each( function (index) {
				$(this).find('.slider__slide-member-image').removeClass('is-visible')
			})

			setTimeout(function(){
				swiper.slidePrev();
				$('.js-slider-members .swiper-button-prev').addClass('enabled');

				$('.js-slider-members .swiper-slide-visible').each( function (index) {
					$(this).find('.slider__slide-member-image').addClass('is-visible')
				})

			}, swiperDelay + 400);
		});

		$('.js-slider-members .swiper-button-next.enabled').on('click', function(){

			$('.js-slider-members .swiper-button-next').removeClass('enabled');

			$('.js-slider-members .swiper-slide-visible').each( function (index) {
				$(this).find('.slider__slide-member-image').removeClass('is-visible')
			})

			setTimeout(function(){
				swiper.slideNext();
				$('.js-slider-members .swiper-button-next').addClass('enabled');

				$('.js-slider-members .swiper-slide-visible').each( function (index) {
					$(this).find('.slider__slide-member-image').addClass('is-visible')
				})

			}, swiperDelay + 400);
		});
	}
});
