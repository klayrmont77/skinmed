import 'fullpage.js';
import { $win, $body } from '../utils/globals';

let _offsetTop    = [];
let _offsetBottom = [];
let _scroll       = 0;

$win.on('load', function() {
	setTimeout(function() {
		$('body').addClass('is-loaded');
	}, 100);
});

$win.on('resize load orientationchange', function() {
	const windowHeight = ( $body.hasClass( 'home' ) ) ? parseInt($win.height(), 10) : parseInt($win.height() - $('.header').outerHeight(), 10);
	const windowHeightWithHeader = $win.height();
	const $fullheight = $('.js-fullheight');
	const $fullheightWithHeader = $('.js-fullheight-with-header');
	$fullheight.css('min-height', windowHeight);
	$fullheightWithHeader.css('min-height', windowHeightWithHeader);

	setOffsets();
});

$win.on('scroll load', function() {
	_scroll = $win.scrollTop();

	if (_scroll > 50 ) {
		$('body').addClass('has-small-header');
	} else {
		$('body').removeClass('has-small-header');
	}

	setOffsets();
	setActiveNavLink();
});

$win.on('load', function () {
	if (('#js-fullpage').length) {
		initFullpage();
	}
});

function setActiveNavLink() {
    $.each( _offsetTop, function( _index, _value ) {
        const $element      = $( '.animation-parent' ).eq( _index );
    	const _isInViewport = _scroll >= _offsetTop[_index] && _scroll <= _offsetBottom[_index] && !$element.hasClass( 'is-active' );

        if ( _isInViewport ) {
        	$element.addClass( 'is-active' );
        }
    });
}

function setOffsets() {
    $( '.animation-parent' ).each( function( i ) {
        const $this = $(this);
        let _offset = parseInt($this.offset().top - ( $win.height() / 1.5 ), 10);

        _offset = (_offset < 0 ) ? 0 : _offset;
        _offsetTop[i] = _offset
    });

    $.each( _offsetTop, function( _index, _value ) {
        let _nextOffset = _offsetTop[_index + 1];

        _nextOffset = ( _nextOffset != undefined && _nextOffset - 1 >= 0 ) ? _nextOffset - 1 : 9999999;

        _offsetBottom[_index] = _nextOffset;
    });
}

function initFullpage() {
	$('#js-fullpage').fullpage({
		licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
		sectionSelector: '.js-fullheight',
		navigation: false,
		scrollingSpeed: 700,
		easingcss3: 'cubic-bezier(0.86, 0.19, 0.3, 0.76)',
		autoScrolling: true,
		scrollOverflow: true,
		scrollOverflowOptions: {
			preventDefault: true,
		},
		afterRender: function () {
			$('.js-fullheight.active').find('.animation-parent').addClass('is-active');
		},
		afterLoad: function (anchorLink, index) {
			$('.js-fullheight.active').find('.animation-parent').addClass('is-active');
			if ( index > 1 ){
				$('body').addClass('has-small-header');
			} else {
				$('body').removeClass('has-small-header');
			}
		},
		onLeave: function (index, nextIndex, direction) {
			$('.js-fullheight').find('.animation-parent').removeClass('is-active');
		},
	});
}
