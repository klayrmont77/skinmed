import { $win } from '../utils/globals';
import 'slick-carousel';

const _element = '.js-slider-articles';
const $element = $( _element );

if ( $element.length ) {
	$element.each( function() {
		const $this   = $(this);
		const $slider = $this.find( '.js__slides' );

		$slider.slick({
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			nextArrow: $this.find( '.js__next' ),
			prevArrow: $this.find( '.js__prev' )
		});

		$slider.on( 'beforeChange', function() {
			$element.addClass( 'is-animate' );
		});

		$slider.on( 'afterChange', function() {
			$element.removeClass( 'is-animate' );
		});
	});
}
