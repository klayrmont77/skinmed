const activeTabClass = 'is-current';

$('.nav-tabs a').on('click', function(event) {
	// event.preventDefault();

	const $tabLink = $(this);
	const $targetTab = $($tabLink.attr('href'));

	$tabLink
		.parent()
		.add($targetTab)
		.addClass(activeTabClass)
			.siblings()
			.removeClass(activeTabClass);
});


const $btnCloseMenu = $('.section-category .btn-close-menu');

$btnCloseMenu.on('click', function (event) {
	event.preventDefault();
	$(this).closest('.section-category').removeClass(activeTabClass);
});
