import { $body } from '../utils/globals';

/**
 * Nav is hover on touch
 *
 * @param  {String} _class [Class]
 * @return {Void}
 */
const navIsHoverOnTouch = (_class = '.js-nav-tabs') => {
	const $container = $(_class);

	if (! $container.length ) {
		return false;
	}

	$container.find('a').on('click', function(event) {
		const $parent    = $(this).parent();
		const _haveChild = $parent.find('> ul').length;

		if (! $parent.hasClass( 'hover' ) && _haveChild ) {
			event.preventDefault();

			$parent.addClass( 'hover' ).siblings().removeClass( 'hover' );
		}
	});

	$body.on('click', function(event) {
		const $target = $(event.target);

		if (! $target.parents(_class).length && ! $target.hasClass(_class) ) {
			$container.find('.' +  'hover' ).removeClass( 'hover' );
		}
	});
};

export default navIsHoverOnTouch;
