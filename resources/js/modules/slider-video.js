import Swiper from 'swiper/swiper-bundle';

import { $win } from '../utils/globals';

$('.js-slider-videos .swiper').each((i, slider) => {
	const $slider = $(slider).closest('.js-slider-videos');

	if ($slider.find('.slider__slide').length > 1) {
		$slider.addClass('is-visible');

		const swiper = new Swiper(slider, {
			slidesPerView: 1,
			loop: true,
			speed: 1000,
			autoHeight: true,
			spaceBetween: 18,
			navigation: {
				nextEl: '.swiper-button-next-video',
				prevEl: '.swiper-button-prev-video',
			},
			on: {
				slideChange: function (el) {
					$('.js-slider-videos .swiper-slide').each(function () {
						var youtubePlayer = $(this).find('iframe').get(0);
						if (youtubePlayer) {
							youtubePlayer.contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
						}
					});
				},
			},
		});
	}
});

$('.js-slider-bilder .swiper').each((i, slider) => {
	const $slider = $(slider).closest('.js-slider-bilder');

	const slides = $slider.find('.slider__slide').length;

	if (slides < 3) {
		$slider.addClass('less-than-3');
	}

	if ($slider.find('.slider__slide').length > 1) {
		$slider.addClass('is-visible');

		const swiper = new Swiper(slider, {
			slidesPerView: 1,
			loop: true,
			allowTouchMove: true,
			loopAdditionalSlides: 2,
			speed: 1000,
			autoHeight: true,
			spaceBetween: 18,
			navigation: {
				nextEl: '.swiper-button-next-bilder',
				prevEl: '.swiper-button-prev-bilder',
			},
			breakpoints: {
				575: {
					slidesPerView: 2,
					allowTouchMove: slides > 2,
				},
			},
		});
	}
});
