import { $win } from '../utils/globals';
import 'slick-carousel';

const _element = '.js-slider-before-after';
const $element = $( _element );

if ( $element.length ) {
	$element.each( function() {
		const $this = $(this);

		$this.find( '.js__slides' ).slick({
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			nextArrow: $this.find( '.js__next' ),
			prevArrow: $this.find( '.js__prev' )
		});
	});
}
