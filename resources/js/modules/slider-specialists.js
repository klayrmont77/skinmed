import Swiper from 'swiper/swiper-bundle';

import { $win } from '../utils/globals';

var elementNumbers = $('.js-slider-specialists .slider__slide').length;

if ( $win.width() > 1023 ) {
	if (elementNumbers > 3) {
		sliderSpecialists();
		$('.js-slider-specialists').addClass('hasSlider');
	};
} else if ( $win.width() > 575 && $win.width() < 1024 ) {
	if (elementNumbers > 2) {
		sliderSpecialists();
		$('.js-slider-specialists').addClass('hasSlider');
	};
} else {
	if (elementNumbers > 1) {
		sliderSpecialists();
		$('.js-slider-specialists').addClass('hasSlider');
	}
}

function sliderSpecialists () {
	$('.js-slider-specialists .swiper').each((i, slider) => {
		const $slider = $(slider).closest('.js-slider-specialists');

		if ($slider.find('.slider__slide').length > 1) {
			$slider.addClass('is-visible');

			const swiper = new Swiper(slider, {
				slidesPerView: 1,
				loop: true,
				speed: 1000,
				watchOverflow: true,
				autoHeight: true,
				spaceBetween: 0,
				navigation: {
					nextEl: '.swiper-button-next',
					prevEl: '.swiper-button-prev',
				},
				breakpoints: {
					575: {
						slidesPerView: 2,
					},
					1023: {
						slidesPerView: 2,
					},
					1279: {
						slidesPerView: 3,
					},
				},
			});
		}
	});
}

$win.on('load resize', function () {
	setTimeout(() => {
		const imageH = $('.js-slider-specialists .specialist__image.js-equalize-height').outerHeight();
		$('.js-slider-specialists .slider__actions').css('top', imageH / 2)
		$('.js-slider-specialists .slider__specialists-line').css('top', imageH)
	}, 100);
});
