import { $win } from '../utils/globals';

const $customScroll = $('.js-custom-scroll');

$win.on('load resize orientationchange', function () {
	$customScroll.each( function () {
		if ($win.width() > 1025) {
			$(this).mCustomScrollbar({
				theme: 'dark',
			});
		} else {
			$(this).mCustomScrollbar('destroy');
		}
	});
});


