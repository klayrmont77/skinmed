import Swiper from 'swiper/swiper-bundle';

import { $win } from '../utils/globals';

$win.on('load', function () {
	$('.js-slider-timeline .swiper').each((i, slider) => {
		const $slider = $(slider).closest('.js-slider-timeline');

		if ($slider.find('.slider__slide').length > 1) {
			$slider.addClass('is-visible');

			const swiper = new Swiper(slider, {
				slidesPerView: 'auto',
				loop: false,
				speed: 1000,
				watchOverflow: true,
				spaceBetween: 0,
				navigation: {
					nextEl: '.swiper-button-next',
					prevEl: '.swiper-button-prev',
				},
			});
		}
	});

})
