import Swiper from 'swiper/swiper-bundle';

import { $win } from '../utils/globals';

$('.js-slider-members-related .swiper').each((i, slider) => {
	const $slider = $(slider).closest('.js-slider-members-related');

	if ($slider.find('.slider__slide').length > 1) {
		$slider.addClass('is-visible');

		let loop = true;
		if ($slider.find('.slider__slide').length <= 3) {
			loop = false;
		}

		const swiper = new Swiper(slider, {
			slidesPerView: 1,
			loop: loop,
			speed: 1000,
			watchOverflow: true,
			autoHeight: true,
			spaceBetween: 75,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			breakpoints: {
				1025: {
					slidesPerView: 2,
				},
				1279: {
					slidesPerView: 3,
				},
			},
		});
	}
});

$win.on('load resize', function () {
	setTimeout(() => {
		const imageH = $('.js-slider-members-related .member__image.js-equalize-height').outerHeight();
		$('.slider-members-related .slider__actions').css('top', imageH)
	}, 100);
});

