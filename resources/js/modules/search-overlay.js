const $btn = $('.js-btn-search-overlay');
const $btnClose = $('.section-search-overlay .js-btn-close');

$btn.on('click', function () {
	$('body').addClass('has-search-overlay');
});

$btnClose.on('click', function () {
	$('body').removeClass('has-search-overlay');
});
