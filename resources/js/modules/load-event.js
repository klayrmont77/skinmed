import { $win } from '../utils/globals';

if ($('.app-block-single-career').length) {
	$('body').addClass('darkHeader');

	$('.input-text').each(function() {
		let $this = $(this);
		let label = $this.parents('fieldset').find('label').text();
		$this.attr('placeholder', label);
	})
}