import 'slick-carousel';

import { $win } from '../utils/globals';

$('.app__block-logos ul').slick({
	infinite: true,
	slidesToShow: 5,
	slidesToScroll: 1,
	variableWidth: true,
	responsive: [
		{
		breakpoint: 1279,
			settings: {
				slidesToShow: 4,
			}
		},
		{
		breakpoint: 767,
			settings: {
				slidesToShow: 3,
			}
		}
	]
});

$('.js-thumbnail').on( 'click', function () {
	let videoSrc = $(this).siblings('iframe').prop('src');
	videoSrc += '&autoplay=1';
	$(this).siblings('iframe').prop('src', videoSrc);
	$(this).css('display', 'none');
});
