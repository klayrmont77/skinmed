import Swiper from 'swiper/swiper-bundle';

import { $win } from '../utils/globals';

$('.js-slider-fullheight .swiper').each((i, slider) => {
	const $slider = $(slider).closest('.js-slider-fullheight');

	if ($slider.find('.slider__slide').length > 1) {
		$slider.addClass('is-visible');

		const swiper = new Swiper(slider, {
			slidesPerView: 1,
			loop: true,
			speed: 1000,
			slidesPerView: 1,
			slidesPerGroup: 1,
			watchOverflow: true,
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
			},
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
		});
	}
});
