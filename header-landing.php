<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>

	<meta http-equiv="Content-Type" content="<?php bloginfo( 'html_type' ); ?>; charset=<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<link rel="stylesheet" href="https://use.typekit.net/mql5urv.css">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/site.webmanifest">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">

	<?php wp_head(); ?>

</head>

<?php

extract($args);
$logo 		  	 = get_field( 'app_landing_header_logo', 'options' );
$location_url 	 = get_field( 'app_landing_header_location_url', 'options' );
$location_target = get_field( 'app_landing_header_location_target', 'options' );
$phone 		  	 = get_field( 'app_landing_header_phone', 'options' );
?>

<body <?php body_class(); ?>>

	<div class="wrapper">
		<div class="wrapper__inner">
			<header class="header header--white-bg">
				<div class="shell">
					<div class="header__bar">
						<?php if ( ! empty( $location_url ) ) : ?>
							<a href="<?php echo esc_url( $location_url ); ?>" class="btn-icon" target="<?php echo esc_attr( $location_target ); ?>">
								<img src="<?php bloginfo('template_directory'); ?>/resources/images/ico-pin-dark.svg" alt="">
							</a>
						<?php endif ?>

						<?php
							if ( has_nav_menu( 'top-bar-menu' ) ) :

								wp_nav_menu( array(

									'theme_location'  => 'landing-page-menu',

									'container'       => 'nav',

									'container_class' => 'nav-utilities',

									'depth'           => 1,

								) );

							endif;
						
						if ( ! empty( $phone ) ) : ?>
							<a href="tel:<?php echo app_strip_phone_digits( $phone ); ?>" class="btn-icon">
								<img src="<?php bloginfo('template_directory'); ?>/resources/images/ico-phone-dark.svg" alt="">
							</a>
						<?php endif ?>
					</div>

					<div class="header__content">
						<a href="<?php echo home_url( '/' ); ?>" class="logo">
							<?php if ( ! empty( $logo ) ): ?>
								<?php echo wp_get_attachment_image( $logo, 'app_full_width' ); ?>
							<?php else: ?>
								<img src="<?php bloginfo('template_directory'); ?>/resources/images/logo-dark.svg" alt="Skinmed">
							<?php endif ?>
						</a>
					</div><!-- /.header__content -->
				</div><!-- /.shell -->

			</header><!-- /.header -->



			<?php if ( is_front_page() ): ?>

				<div class="main main--fullpage">

			<?php else: ?>

				<div class="main">

			<?php endif ?>
