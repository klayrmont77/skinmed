<?php

register_post_type( 'app_member', array(
	'labels' => array(
		'name' => __( 'Team Members', 'app' ),
		'singular_name' => __( 'Team Member', 'app' ),
		'add_new' => __( 'Add New', 'app' ),
		'add_new_item' => __( 'Add new Team Member', 'app' ),
		'view_item' => __( 'View Team Member', 'app' ),
		'edit_item' => __( 'Edit Team Member', 'app' ),
		'new_item' => __( 'New Team Member', 'app' ),
		'view_item' => __( 'View Team Member', 'app' ),
		'search_items' => __( 'Search Team Members', 'app' ),
		'not_found' =>  __( 'No Team Members found', 'app' ),
		'not_found_in_trash' => __( 'No Team Members found in trash', 'app' ),
	),
	'public' => true,
	'exclude_from_search' => false,
	'show_ui' => true,
	'capability_type' => 'post',
	'hierarchical' => false,
	'_edit_link' => 'post.php?post=%d',
	'rewrite' => array(
		'slug' => false,
		'with_front' => false,
	),
	'query_var' => true,
	'menu_icon' => 'dashicons-groups',
	'supports' => array( 'title', 'thumbnail', 'editor', 'page-attributes' ),
) );



register_post_type( 'app_media', array(
	'labels' => array(
		'name' => __( 'Media Articles', 'app' ),
		'singular_name' => __( 'Media Article', 'app' ),
		'add_new' => __( 'Add New', 'app' ),
		'add_new_item' => __( 'Add new Media Article', 'app' ),
		'view_item' => __( 'View Media Article', 'app' ),
		'edit_item' => __( 'Edit Media Article', 'app' ),
		'new_item' => __( 'New Media Article', 'app' ),
		'view_item' => __( 'View Media Article', 'app' ),
		'search_items' => __( 'Search Media Articles', 'app' ),
		'not_found' =>  __( 'No Media Articles found', 'app' ),
		'not_found_in_trash' => __( 'No Media Articles found in trash', 'app' ),
	),
	'public' => true,
	'exclude_from_search' => false,
	'show_ui' => true,
	'capability_type' => 'post',
	'hierarchical' => false,
	'_edit_link' => 'post.php?post=%d',
	'rewrite' => array(
		'slug' => 'news-medien',
		'with_front' => false,
	),
	'query_var' => true,
	'menu_icon' => 'dashicons-media-archive',
	'supports' => array( 'title', 'thumbnail', 'editor', 'page-attributes' ),
) );

register_post_type( 'app_landing_page', array(
	'labels' => array(
		'name' => __( 'Landing Pages', 'app' ),
		'singular_name' => __( 'Landing Page', 'app' ),
		'add_new' => __( 'Add New', 'app' ),
		'add_new_item' => __( 'Add new Landing Page', 'app' ),
		'view_item' => __( 'View Landing Page', 'app' ),
		'edit_item' => __( 'Edit Landing Page', 'app' ),
		'new_item' => __( 'New Landing Page', 'app' ),
		'search_items' => __( 'Search Landing Pages', 'app' ),
		'not_found' =>  __( 'No Landing Pages found', 'app' ),
		'not_found_in_trash' => __( 'No Landing Pages found in trash', 'app' ),
	),
	'public' => true,
	'exclude_from_search' => true,
	'show_ui' => true,
	'capability_type' => 'post',
	'hierarchical' => false,
	'_edit_link' => 'post.php?post=%d',
	'rewrite' => array(
		'slug' => 'lp',
		'with_front' => false,
	),
	'query_var' => true,
	'menu_icon' => 'dashicons-admin-page',
	'show_in_rest' => true,
	'supports' => array( 'title', 'editor', 'page-attributes' ),
) );

