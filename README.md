## Jobs / Karriere - Videoanleitung
Jobs bearbeiten und grundlegendes
https://www.loom.com/share/cacd3bfcc39742c0b882783fabf8aa28

Neue Jobs anlegen:
https://www.loom.com/share/58b3b6bc937546a3906fa1b59f482df8

## Mediathek / Ordnerstruktur
https://www.loom.com/share/8373041982124151bdb14674f69e88fa

## 2-Faktor-Authentifizierung einrichten
Selbst einrichten (für Administratoren)
https://www.loom.com/share/67d61a55897a4db3b4dfbae3996b74ec

Für Mitarbeiter einrichten (keine Administratoren)
https://www.loom.com/share/57e289d0328f44a886fccd0d04a948f9

