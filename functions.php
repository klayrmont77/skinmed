<?php

add_shortcode('booking', function() {
	// widget includes specific tracking parameters
	$tracking_UA_and_GA4 = '<script>var measurementIds=["UA-39719627-1","G-G0PFS232HC"];window.addEventListener("message",function(a){var e=a.data["od-widget-id"],i=a.data["od-widget-height"],j=a.data["od-widget-ios"];if(e){var c=document.getElementById("od-widget-"+e);i&&(c.style.height=i+"px"),!0===j&&(c.style.width="100px",c.style["min-width"]="100%",c.scrolling="no");var f=a.data.GA4,b=a.data.UA;if(f)for(var g=0;g<measurementIds.length;g++){var h=measurementIds[g];if(void 0!==window.gtag){if("UA"===h.substring(0,2)){var d={send_to:h,event_category:b.category,event_label:b.label};gtag("event",b.action,d)}else{var k=Object.keys(f.properties),d={send_to:h};k.forEach(a=>{d[a]=f.properties[a]}),gtag("event",f.name,d)}}else if(void 0!==window.ga){var d={hitType:"event",eventCategory:b.category,eventAction:b.action,eventLabel:e};ga(function(){ga.getAll().forEach(a=>{h===a.get("trackingId")&&ga(a.get("name")+".send",d)})})}}}}),window.addEventListener("load",function(d){for(var c=document.querySelectorAll("iframe.od-widget"),b=0;b<c.length;b++){var a=c[b];a.dataset&&a.dataset.src&&(a.src=a.dataset.src)}})</script><iframe class="od-widget" id="od-widget-6d80d9b076010e535559189a5dd91f3c120fb9e1ca0230304402b307f62c3656" src="about:blank" data-src="https://www.onedoc.ch/de/widget/6d80d9b076010e535559189a5dd91f3c120fb9e1ca0230304402b307f62c3656" frameborder="0" style="width:100%;max-width:1024px;height:400px"></iframe>';
	return $tracking_UA_and_GA4;
});

add_shortcode('booking-hairlounge', function() {
	// widget includes specific tracking parameters
	$tracking_UA_and_GA4 = '<script>var measurementIds=["UA-39719627-1","G-G0PFS232HC"];window.addEventListener("message",function(a){var e=a.data["od-widget-id"],i=a.data["od-widget-height"],j=a.data["od-widget-ios"];if(e){var c=document.getElementById("od-widget-"+e);i&&(c.style.height=i+"px"),!0===j&&(c.style.width="100px",c.style["min-width"]="100%",c.scrolling="no");var f=a.data.GA4,b=a.data.UA;if(f)for(var g=0;g<measurementIds.length;g++){var h=measurementIds[g];if(void 0!==window.gtag){if("UA"===h.substring(0,2)){var d={send_to:h,event_category:b.category,event_label:b.label};gtag("event",b.action,d)}else{var k=Object.keys(f.properties),d={send_to:h};k.forEach(a=>{d[a]=f.properties[a]}),gtag("event",f.name,d)}}else if(void 0!==window.ga){var d={hitType:"event",eventCategory:b.category,eventAction:b.action,eventLabel:e};ga(function(){ga.getAll().forEach(a=>{h===a.get("trackingId")&&ga(a.get("name")+".send",d)})})}}}}),window.addEventListener("load",function(d){for(var c=document.querySelectorAll("iframe.od-widget"),b=0;b<c.length;b++){var a=c[b];a.dataset&&a.dataset.src&&(a.src=a.dataset.src)}})</script><iframe class="od-widget" id="od-widget-2124101e129c19ac07bcfd885acbcd1870ce56d36d85a19c801d3219594aca5a" src="about:blank" data-src="https://www.onedoc.ch/de/widget/2124101e129c19ac07bcfd885acbcd1870ce56d36d85a19c801d3219594aca5a" frameborder="0" style="width:100%;max-width:1024px;height:400px"></iframe>';
	return $tracking_UA_and_GA4;
});



/**
 * Remove jobs from wp search
 */
add_action( 'init', 'exclude_jobs_from_search', 99 );
function exclude_jobs_from_search() {
	global $wp_post_types;

	if ( post_type_exists( 'job_listing' ) ) {

		// exclude from search results
		$wp_post_types['job_listing']->exclude_from_search = true;
	}
}

/**
 * Remove backend fields from WP Job Manager single post CPT
 */
add_filter( 'job_manager_job_listing_data_fields', 'admin_remove_job_metafields' );
function admin_remove_job_metafields( $fields ) {
	unset($fields['_featured']);
	unset($fields['_job_location']);
	unset($fields['_company_website']);
	unset($fields['_company_twitter']);
	unset($fields['_company_tagline']);
	unset($fields['_company_video']);
	unset($fields['_company_featured']);
	unset($fields['_company_name']);
	return $fields;
}

add_filter( 'manage_edit-job_listing_columns', 'remove_expires_column_admin' );
function remove_expires_column_admin( $columns ) {
	unset( $columns['job_location'] );
	unset( $columns['wpseo-title'] );
	unset( $columns['wpseo-metadesc'] );
	unset( $columns['job_listing_type'] );
	return $columns;
}


define( 'APP_THEME_DIR', dirname( __FILE__ ) . DIRECTORY_SEPARATOR );

# Enqueue JS and CSS assets on the front-end
add_action( 'wp_enqueue_scripts', 'app_enqueue_assets' );
function app_enqueue_assets() {
	$template_dir = get_template_directory_uri();

	# Enqueue Google API JS
	wp_enqueue_script(
		'google-map-api',
		'https://maps.google.com/maps/api/js?v=weekly&key=AIzaSyBX_ZpMk4oSxXaojZeu7uh6hJEgSPTllp4',
		array( 'jquery' ), // deps
		null,
		true
	);

	wp_enqueue_script( 'theme-scrolloverflow', $template_dir . '/resources/js/scrolloverflow.js', array( 'jquery' ) );

	# Enqueue Youtube Api
	wp_enqueue_script( 'youtube-functions', 'https://www.youtube.com/iframe_api', array( 'jquery' ) );
	# Enqueue Custom JS files
	wp_enqueue_script(
		'theme-js-bundle',
		$template_dir . app_assets_bundle( 'js/bundle.js' ),
		array( 'jquery' ), // deps
		null, // version -- this is handled by the bundle manifest
		true // in footer
	);

	# Enqueue Custom CSS files
	wp_enqueue_style(
		'theme-css-bundle',
		$template_dir . app_assets_bundle( 'css/bundle.css' )
	);

	# The theme style.css file may contain overrides for the bundled styles
	app_enqueue_style( 'theme-styles', $template_dir . '/style.css' );

	# Enqueue Comments JS file
	if ( is_singular() ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

# Enqueue JS and CSS assets on admin pages
add_action( 'admin_enqueue_scripts', 'app_admin_enqueue_scripts' );
function app_admin_enqueue_scripts() {
	$template_dir = get_template_directory_uri();

	# Enqueue Scripts
	# @app_enqueue_script attributes -- id, location, dependencies, in_footer = false
	# app_enqueue_script( 'theme-admin-functions', $template_dir . '/js/admin-functions.js', array( 'jquery' ) );

	# Enqueue Styles
	# @app_enqueue_style attributes -- id, location, dependencies, media = all
	# app_enqueue_style( 'theme-admin-styles', $template_dir . '/css/admin-style.css' );

	# Editor Styles
	# add_editor_style( 'css/custom-editor-style.css' );
}

add_action( 'login_enqueue_scripts', 'app_login_enqueue' );
function app_login_enqueue() {
	# app_enqueue_style( 'theme-login-styles', get_template_directory_uri() . '/css/login-style.css' );
}

# Attach Custom Post Types and Custom Taxonomies
add_action( 'init', 'app_attach_post_types_and_taxonomies', 0 );
function app_attach_post_types_and_taxonomies() {
	# Attach Custom Post Types
	include_once( APP_THEME_DIR . 'options/post-types.php' );

	# Attach Custom Taxonomies
	include_once( APP_THEME_DIR . 'options/taxonomies.php' );
}

add_action( 'after_setup_theme', 'app_setup_theme' );

# To override theme setup process in a child theme, add your own app_setup_theme() to your child theme's
# functions.php file.
if ( ! function_exists( 'app_setup_theme' ) ) {
	function app_setup_theme() {
		# Make this theme available for translation.
		load_theme_textdomain( 'app', get_template_directory() . '/languages' );

		$autoload_dir = APP_THEME_DIR . 'lib/autoload.php';
		include_once( $autoload_dir );

		# Autoload dependencies
		$autoload_dir = APP_THEME_DIR . 'vendor/autoload.php';
		if ( ! is_readable( $autoload_dir ) ) {
			wp_die( __( 'Please, run <code>composer install</code> to download and install the theme dependencies.', 'app' ) );
		}
		include_once( $autoload_dir );

		# Additional libraries and includes
		include_once( APP_THEME_DIR . 'includes/admin-login.php' );
		include_once( APP_THEME_DIR . 'includes/comments.php' );
		include_once( APP_THEME_DIR . 'includes/title.php' );
		include_once( APP_THEME_DIR . 'includes/gravity-forms.php' );
		include_once( APP_THEME_DIR . 'includes/helpers.php' );
		include_once( APP_THEME_DIR . 'includes/blocks.php' );

		# Theme supports
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'title-tag' );
		add_theme_support( 'menus' );
		add_theme_support( 'html5', array( 'gallery' ) );

		// Add support for full and wide align images.
		add_theme_support( 'align-wide' );

		// Add support for responsive embedded content.
		add_theme_support( 'responsive-embeds' );

		# Manually select Post Formats to be supported - http://codex.wordpress.org/Post_Formats
		// add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat' ) );

		# Register Theme Menu Locations

		register_nav_menus( array(
			'main-menu' 	   	   => __( 'Main Menu', 'app' ),
			'landing-page-menu'    => __( 'Landing Top Bar Menu', 'app' ),
			'top-bar-menu' 	   	   => __( 'Top Bar Menu', 'app' ),
			'top-bar-menu-dropdown'=> __( 'Top Bar Menu Dropdown', 'app' ),
			'popup-tabs-menu' 	   => __( 'Popup Tabs Menu', 'app' ),
			'plastic-surgery-menu' => __( 'Plastic Surgery Menu', 'app' ),
			'dermatology-menu' 	   => __( 'Dermatology Menu', 'app' ),
			'cosmetology-menu' 	   => __( 'Cosmetology Menu', 'app' ),
			'hairlounge-menu' 	   => __( 'Hairlounge Menu', 'app' ),
		) );


		# Attach custom shortcodes
		include_once( APP_THEME_DIR . 'options/shortcodes.php' );

		# Add Actions
		add_action( 'widgets_init', 'app_widgets_init' );

		# Add Filters
		add_filter( 'excerpt_more', 'app_excerpt_more' );
		add_filter( 'excerpt_length', 'app_excerpt_length', 999 );
		add_filter( 'app_theme_favicon_uri', function() {
			//return get_template_directory_uri() . '/dist/images/favicon.ico';
		} );

		# Add Image Size
		add_image_size( 'app_full_width', 1920, 0, false );
		add_image_size( 'app_home_image', 1685, 1000, false );
		add_image_size( 'app_team_member', 345, 380, false );
		add_image_size( 'app_image_slide', 536, 342, false );
		add_image_size( 'app_team_member_home', 350, 400, false );
	}
}

# Register Sidebars
# Note: In a child theme with custom app_setup_theme() this function is not hooked to widgets_init
function app_widgets_init() {
	$sidebar_options = array_merge( app_get_default_sidebar_options(), array(
		'name' => __( 'Default Sidebar', 'app' ),
		'id'   => 'default-sidebar',
	) );

	register_sidebar( $sidebar_options );
}

# Sidebar Options
function app_get_default_sidebar_options() {
	return array(
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget'  => '</li>',
		'before_title'  => '<h2 class="widget__title">',
		'after_title'   => '</h2>',
	);
}

function app_excerpt_more() {
	return '[...]';
}

function app_excerpt_length() {
	return 23;
}

/**
 * Get the path to a versioned bundle relative to the theme directory.
 *
 * @param  string $path
 * @return string
 */
function app_assets_bundle( $path ) {
	static $manifest = null;

	if ( is_null( $manifest ) ) {
		$manifest_path = APP_THEME_DIR . 'dist/manifest.json';

		if ( file_exists( $manifest_path ) ) {
			$manifest = json_decode( file_get_contents( $manifest_path ), true );
		} else {
			$manifest = array();
		}
	}

	$path = isset( $manifest[ $path ] ) ? $manifest[ $path ] : $path;

	return '/dist/' . $path;
}

/**
 * Sometimes, when using Gutenberg blocks the content output
 * contains empty unnecessary paragraph tags.
 *
 * In WP v5.2 this will be fixed, however, until then this function
 * acts as a temporary solution.
 *
 * @see https://core.trac.wordpress.org/ticket/45495
 *
 * @param  string $content
 * @return string
 */
remove_filter( 'the_content', 'wpautop' );
add_filter( 'the_content', 'app_fix_empty_paragraphs_in_blocks' );
function app_fix_empty_paragraphs_in_blocks( $content ) {
	global $wp_version;

	if ( version_compare( $wp_version, '9.9', '<' ) && has_blocks() ) {
		return $content;
	}

	return wpautop( $content );
}

/**
 * Filters the ACF json save point
 *
 * @see https://www.advancedcustomfields.com/resources/local-json/
 *
 * @param  string $path
 * @return string
 */
add_filter( 'acf/settings/save_json', 'app_filter_save_acf_json' );
function app_filter_save_acf_json( $path ) {
	$path = get_stylesheet_directory() . '/acf-json';

	return $path;
}

/**
 * Filters the ACF json load point(s)
 *
 * @see https://www.advancedcustomfields.com/resources/local-json/
 *
 * @param  array $paths ACF loads all .json files from multiple load points
 * @return array
 */
add_filter( 'acf/settings/load_json', 'app_filter_set_acf_load_json' );
function app_filter_set_acf_load_json( $paths ) {
	// remove original path
	unset( $paths[0] );

	// append path
	$paths[] = get_stylesheet_directory() . '/acf-json';

	return $paths;
}

function app_is_gutenberg_editor() {
	if ( function_exists( 'is_gutenberg_page' ) && is_gutenberg_page() ) {
		return true;
	}

	$current_screen = get_current_screen();

	if ( method_exists( $current_screen, 'is_block_editor' ) && $current_screen->is_block_editor() ) {
		return true;
	}

	return false;
}



