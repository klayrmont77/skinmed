��          4      L       `      a   <   w   m  �   
   "  O   -                    Error 404 - Not Found Please check the URL for proper spelling and capitalization. Project-Id-Version: Base
PO-Revision-Date: 2022-03-22 15:01+0100
Last-Translator: 
Language-Team: 
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: vendor
X-Poedit-SearchPathExcluded-1: node_modules
 Fehler 404 Seite nicht gefunden. Bitte überprüfen Sie die korrekte Schreibweise der URL. 