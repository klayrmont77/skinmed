			<?php if ( ! is_front_page() ): ?>
				</div><!-- /.main -->
			<?php endif ?>

			<footer class="footer <?php echo ( is_front_page() ) ? 'js-fullheight' : '' ; ?>">
				<div class="shell">
					<div class="footer__inner">
						<?php
						app_render_fragment( 'footer/text' );

						app_render_fragment( 'footer/locations' );

						app_render_fragment( 'footer/contact' );

						app_render_fragment( 'footer/bottom-bar' );
						?>
					</div><!-- /.footer__inner -->
				</div><!-- /.shell -->
			</footer><!-- /.footer -->
			<?php if ( is_front_page() ) : ?>
				</div><!-- /.main -->
			<?php endif ?>
		</div><!-- /.wrapper__inner -->
	</div><!-- /.wrapper -->
	<?php wp_footer(); ?>
</body>
</html>
