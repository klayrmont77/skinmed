<?php

$header_style = get_field( 'app_page_header_style' );
$new_dropdown = get_field( 'app_header_use_new_dropdown_styles', 'option' );

if ( ! $new_dropdown ) {
	get_header( '', array(
		'style' => $header_style,
	) );
} else {
	get_header( 'nav-dropdown', array(
		'style' => $header_style,
	) );
}

the_content();

get_footer();
