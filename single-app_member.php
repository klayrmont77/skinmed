<?php
$new_dropdown = get_field( 'app_header_use_new_dropdown_styles', 'option' );

if ( ! $new_dropdown ) {
	get_header( '', array(
		'style' => 'white',
	) );
} else {
	get_header( 'nav-dropdown', array(
		'style' => 'white',
	) );
}

$member_id 	  	 = get_the_ID();

$position  	  	 = get_field( 'app_member_position', $member_id );

$back_button_url = get_field( 'app_global_member_back_button_url', 'option' );

$quote 	   	  	 = get_field( 'app_member_quote', $member_id );

$quote_author 	 = get_field( 'app_member_quote_author', $member_id );

$features  	  	 = get_field( 'app_member_features', $member_id );

$columns   	  	 = get_field( 'app_member_columns', $member_id );



?>



<section class="app-block-single-member">

	<div class="shell">

		<div class="app__block-inner js-sticky-el-parent">

			<div class="app__block-head">

				<div class="app__block-button">

					<a href="<?php echo esc_url( $back_button_url ); ?>" class="btn-back-to js-sticky-el">

						<svg xmlns="http://www.w3.org/2000/svg" width="69.75" height="69.75" viewBox="0 0 69.75 69.75"><g id="Group_154" data-name="Group 154" transform="translate(-287.25 -135.904)"><circle id="Ellipse_7" data-name="Ellipse 7" cx="34.375" cy="34.375" r="34.375" transform="translate(287.75 136.404)" fill="transparent" stroke="#676d71" stroke-miterlimit="10" stroke-width="1"/><path id="Path_267" data-name="Path 267" d="M324.448,179.343l-8.564-8.564,8.564-8.564" fill="transparent" stroke="#676d71" stroke-miterlimit="10" stroke-width="1"/></g></svg>

					</a>

				</div><!-- /.app__block-button -->



				<div class="app__block-head-inner">

					<?php if ( has_post_thumbnail() ) : ?>

						<div class="app__block-avatar" data-aos="zoom-in">

							<?php the_post_thumbnail( 'app_team_member' ); ?>

						</div><!-- /.app__block-avatar -->

					<?php endif; ?>



					<div class="app__block-name" data-aos="fade-right">

						<h1>

							<?php the_title(); ?>

						</h1>



						<p>

							<?php echo esc_html( $position ); ?>

						</p>

					</div><!-- /.app__block-name -->

				</div><!-- /.app__block-head-inner -->

			</div><!-- /.app__block-head -->



			<div class="app__block-content">

				<?php if ( ! empty( $quote ) ) : ?>

					<div class="app__block-quote" data-aos="fade-up">

						<blockquote>

							<?php echo esc_html( $quote ); ?>

						</blockquote>



						<h6>

							<?php echo esc_html( $quote_author ); ?>

						</h6>

					</div><!-- /.app__block-quote -->

				<?php endif;



				if ( ! empty( $features ) ) : ?>

					<div class="app__block-features">

						<ul class="app__block-features-items">

							<?php foreach ( $features as $feature ) : ?>

								<li class="app__block-features-item" data-aos="fade-up">

									<?php if ( ! empty( $feature['icon'] ) ) : ?>

										<div class="app__block-features-item-icon">

											<?php echo wp_get_attachment_image( $feature['icon'] ); ?>

										</div><!-- /.app__block-features-item-icon -->

									<?php endif ?>



									<div class="app__block-features-item-entry">

										<p>

											<?php echo esc_html( $feature['title'] ); ?>

										</p>

									</div><!-- /.app__block-features-item-entry -->

								</li>

							<?php endforeach ?>

						</ul>

					</div><!-- /.app__block-features -->

				<?php endif; ?>


				<?php if ( ! empty( $features ) || ! empty( $quote ) ): ?>
					<div class="app__block-separator" data-aos="fade-up"></div><!-- /.app__block-separator -->
				<?php endif ?>



				<?php if ( ! empty( $columns ) ) : ?>

					<div class="app__block-cols">

						<?php foreach ( $columns as $index => $col ) : ?>

							<div class="app__block-col" data-aos="fade-<?php echo ( $index == 0 ) ? 'left' : 'right'; ?>">

								<?php echo app_content( $col['text'] ); ?>

							</div><!-- /.app__block-col -->

						<?php endforeach; ?>

					</div><!-- /.app__block-cols -->

				<?php endif; ?>



			</div><!-- /.app__block-content -->

		</div><!-- /.app__block-inner -->

	</div><!-- /.shell -->

</section><!-- /.app-block-single-member -->



<?php get_footer(); ?>

