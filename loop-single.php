<?php

$add_hero = get_field( 'app_post_add_hero', get_the_ID() );

$socials  = get_field( 'app_post_socials', get_the_ID() );



$category 	   = get_the_category();

if ( ! empty( $category ) ) {

	$category_name = $category[0]->name;
	$category_url  = get_term_link( $category[0] );
}

$author = get_field( 'app_post_author', get_the_ID() );

if ( empty( $author ) ) {
	$author = get_field( 'app_blog_standard_author', 'options' );
}

if ( empty( $author ) ) {
	$author_id 		 = get_post_field( 'post_author', get_the_ID() );
	$author_nickname = get_the_author_meta( 'nickname', $author_id );
	$author_url 	 = get_author_posts_url( $author_id );
	$author_avatar   = get_avatar( $author_id );
}

if ( ! empty( $add_hero ) ) {
	$hero_title = get_field( 'app_post_hero_title', get_the_ID() );

	$hero_text  = get_field( 'app_post_hero_text', get_the_ID() );
}


if ( empty( $add_hero ) ): ?>
	<style type="text/css" media="screen">
	.article-single{
		border-top: none;
	}
</style>
<?php endif;

if ( ! empty( $add_hero ) ) : ?>

	<section class="app-block-heading app-block-heading--smaller">

		<div class="shell">

			<div class="app__block-inner">

				<div class="app__block-content" data-aos="fade-up">

					<?php if ( ! empty( $hero_title ) ) : ?>

						<h1>

							<?php echo esc_html( $hero_title ); ?>

						</h1>

					<?php endif;



					echo wpautop( esc_html( $hero_text ) ); ?>

				</div><!-- /.app__block-content -->

			</div><!-- /.app__block-inner -->

		</div><!-- /.shell -->

	</section><!-- /.app-block-heading -->

<?php endif; ?>


<section class="app-block-single-article">
	<div class="shell">
		<article class="article-single">
			<div class="article__head">
				<div class="article__head-actions">
					<a href="<?php echo get_the_permalink( get_option( 'page_for_posts' ) ); ?>" class="btn-back-to js-sticky-el">
						<svg xmlns="http://www.w3.org/2000/svg" width="69.75" height="69.75" viewBox="0 0 69.75 69.75"><g id="Group_154" data-name="Group 154" transform="translate(-287.25 -135.904)"><circle id="Ellipse_7" data-name="Ellipse 7" cx="34.375" cy="34.375" r="34.375" transform="translate(287.75 136.404)" fill="transparent" stroke="#676d71" stroke-miterlimit="10" stroke-width="1"/><path id="Path_267" data-name="Path 267" d="M324.448,179.343l-8.564-8.564,8.564-8.564" fill="transparent" stroke="#676d71" stroke-miterlimit="10" stroke-width="1"/></g></svg>
					</a>
				</div><!-- /.article__head-actions -->

				<div class="article__head-content" data-aos="fade-up">
					<div class="article__categories">
						<a href="<?php echo esc_url( $category_url ); ?>">
							<?php echo esc_html( $category_name ); ?>
						</a>
					</div><!-- /.article__categories -->

					<h1 class="article__title">
						<?php the_title(); ?>
					</h1><!-- /.article__title -->
				</div><!-- /.article__head-content -->
			</div><!-- /.article__head -->

			<div class="article__meta">
				<?php if ( ! empty( $author ) ) : ?>
					<div class="article__author">
						<?php if ( has_post_thumbnail( $author[0] ) ) : ?>
							<div class="article__avatar image-fit" data-aos="fade-up">
								<?php echo get_the_post_thumbnail( $author[0] );; ?>
							</div><!-- /.article__avatar -->
						<?php endif ?>

						<div class="article__author-content" data-aos="fade-up">
							<p>

								<?php _e( 'Erstellt von', 'app' ); ?>

								<strong>
									<a href="<?php echo get_the_permalink( $author[0] ); ?>">
										<?php echo get_the_title( $author[0] ); ?>
									</a>
								</strong>
							</p>

							<p>am <?php echo get_the_date( 'd.m.Y' ); ?></p>
						</div><!-- /.article__author-content -->
					</div><!-- /.article__author -->
				<?php else: ?>
					<div class="article__author">
						<div class="article__avatar image-fit" data-aos="fade-up">
							<?php echo $author_avatar; ?>
						</div><!-- /.article__avatar -->

						<div class="article__author-content" data-aos="fade-up">
							<p>
								<?php _e( 'Erstellt von', 'app' ); ?>
								<strong>
									<a href="<?php echo esc_url( $author_url ); ?>">
										<?php echo esc_html( $author_nickname ); ?>
									</a>
								</strong>
							</p>

							<p>am <?php echo get_the_date( 'd.m.Y' ); ?></p>
						</div><!-- /.article__author-content -->
					</div><!-- /.article__author -->
				<?php endif ?>


				<div class="article__socials" data-aos="fade-up">
					<div class="socials">
						<ul>
							<li>
								<a href="<?php echo esc_url( 'https://www.facebook.com/sharer/sharer.php?u=' . urlencode( get_permalink() ) ); ?>" target="_blank">
									<i class="fab fa-facebook-f"></i>
								</a>
							</li>

							<li>
								<a href="<?php echo esc_url( 'https://www.linkedin.com/shareArticle?mini=true&url=' . urlencode( get_permalink() ) . '&title=' . urlencode( get_the_title() ) ); ?>" target="_blank">
									<i class="fab fa-linkedin"></i>
								</a>
							</li>

							<li>
								<a href="<?php echo esc_url( 'mailto:?subject=' . get_the_title() . '&body=' . urlencode( get_permalink() ) ); ?>" target="_blank">
									<i class="fas fa-link"></i>
								</a>
							</li>
						</ul>
					</div><!-- /.socials -->
				</div><!-- /.article__socials -->
			</div><!-- /.article__meta -->

			<div class="article__entry richtext-entry" data-aos="fade-up">
				<?php the_content(); ?>
			</div><!-- /.article__entry -->

			<div class="article__foot" data-aos="fade-up">
				<div class="socials">
					<ul>
						<li>
							<a href="<?php echo esc_url( 'https://www.facebook.com/sharer/sharer.php?u=' . urlencode( get_permalink() ) ); ?>" target="_blank">
								<i class="fab fa-facebook-f"></i>
							</a>
						</li>

						<li>
							<a href="<?php echo esc_url( 'https://www.linkedin.com/shareArticle?mini=true&url=' . urlencode( get_permalink() ) . '&title=' . urlencode( get_the_title() ) ); ?>" target="_blank">
								<i class="fab fa-linkedin"></i>
							</a>
						</li>

						<li>
							<a href="<?php echo esc_url( 'mailto:?subject=' . get_the_title() . '&body=' . urlencode( get_permalink() ) ); ?>" target="_blank">
								<i class="fas fa-link"></i>
							</a>
						</li>
					</ul>
				</div><!-- /.socials -->
			</div><!-- /.article__foot -->
		</article><!-- /.article-single -->
	</div><!-- /.shell -->
</section><!-- /.app-block-single-article -->

