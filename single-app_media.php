<?php
$new_dropdown 	 = get_field( 'app_header_use_new_dropdown_styles', 'option' );
$add_hero 		 = get_field( 'add_hero' );

$back_button_url = get_field( 'app_global_media_back_button_url', 'option' );

$socials 		 = get_field( 'socials' );
$magazine_name   = get_field( 'app_media_magazine_name' );

if ( ! empty( $add_hero ) ) {
	$hero_title = get_field( 'hero_title' );
	$hero_text 	= get_field( 'hero_text' );
}

if ( ! $new_dropdown ) {
	get_header();
} else {
	get_header( 'nav-dropdown' );
}

if ( ! empty( $add_hero ) ) : ?>
	<section class="app-block-heading app-block-heading--smaller">
		<div class="shell">
			<div class="app__block-inner">
				<div class="app__block-content" data-aos="fade-up">

					<?php if ( ! empty( $hero_title ) ) : ?>

						<h1>

							<?php echo esc_html( $hero_title ); ?>

						</h1>

					<?php endif;

					echo app_content( $hero_text ); ?>
				</div><!-- /.app__block-content -->
			</div><!-- /.app__block-inner -->
		</div><!-- /.shell -->
	</section><!-- /.app-block-heading -->
<?php endif; ?>

<section class="app-block-single-article">
	<div class="shell">
		<article class="article-single article-single--media">
			<div class="article__head">
				<div class="article__head-actions">
					<div class="article__head-actions-inner js-sticky-el">

						<?php if ( ! empty( $back_button_url ) ) : ?>

							<a href="<?php echo esc_url( $back_button_url ); ?>" class="btn-back-to">

								<svg xmlns="http://www.w3.org/2000/svg" width="69.75" height="69.75" viewBox="0 0 69.75 69.75"><g id="Group_154" data-name="Group 154" transform="translate(-287.25 -135.904)"><circle id="Ellipse_7" data-name="Ellipse 7" cx="34.375" cy="34.375" r="34.375" transform="translate(287.75 136.404)" fill="transparent" stroke="#676d71" stroke-miterlimit="10" stroke-width="1"/><path id="Path_267" data-name="Path 267" d="M324.448,179.343l-8.564-8.564,8.564-8.564" fill="transparent" stroke="#676d71" stroke-miterlimit="10" stroke-width="1"/></g></svg>

							</a>

						<?php endif;
						//the_post_thumbnail();
						?>
					</div><!-- /.article__head-actions-inner -->
				</div><!-- /.article__head-actions -->

				<div class="article__head-content" data-aos="fade-up">
					<div class="article__magazine">

							<?php

							echo esc_html( $magazine_name );

							_e( ' vom ', 'app');

							echo get_the_date( 'd.m.Y' );

							?>
					</div><!-- /.article__magazine -->

					<h1 class="article__title">
						<?php the_title(); ?>
					</h1><!-- /.article__title -->
				</div><!-- /.article__head-content -->
			</div><!-- /.article__head -->

			<div class="article__entry richtext-entry" data-aos="fade-up">
				<?php the_content(); ?>
			</div><!-- /.article__entry -->

			<?php if ( ! empty( $socials ) ) : ?>
				<div class="article__foot" data-aos="fade-up">
					<div class="socials">
						<ul>
							<li>
								<a href="<?php echo esc_url( 'https://www.facebook.com/sharer/sharer.php?u=' . urlencode( get_permalink() ) ); ?>" target="_blank">
									<i class="fab fa-facebook-f"></i>
								</a>
							</li>

							<li>
								<a href="<?php echo esc_url( 'https://www.linkedin.com/shareArticle?mini=true&url=' . urlencode( get_permalink() ) . '&title=' . urlencode( get_the_title() ) ); ?>" target="_blank">
									<i class="fab fa-linkedin"></i>
								</a>
							</li>

							<li>
								<a href="<?php echo esc_url( 'mailto:?subject=' . get_the_title() . '&body=' . urlencode( get_permalink() ) ); ?>" target="_blank">
									<i class="fas fa-link"></i>
								</a>
							</li>
					</div><!-- /.socials -->
				</div><!-- /.article__foot -->
			<?php endif ?>
		</article><!-- /.article-single -->
	</div><!-- /.shell -->
</section><!-- /.app-block-single-article -->



<?php get_footer(); ?>

